<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysAlertMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('alert_messages', function(Blueprint $table){
            $table->foreign('alert_message_type_id')->references('id')->on('alert_message_types')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
         Schema::table('alert_messages', function(Blueprint $table){
            $table->dropForeign('alert_messages_alert_message_type_id_foreign');
        });
    }
}
