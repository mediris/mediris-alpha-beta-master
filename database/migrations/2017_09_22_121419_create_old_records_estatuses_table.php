<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordsestatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_estatuses', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->index();
            $table->string('descripcion',225);
            $table->bigInteger('id_institucion')->unsigned()->index();
            $table->timestamps();
            $table->primary(['id','id_institucion']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_estatuses');
    }
}
