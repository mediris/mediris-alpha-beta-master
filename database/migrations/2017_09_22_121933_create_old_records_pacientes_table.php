<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordspacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_pacientes', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->index();
            $table->string('nombres',255)->nullable();
            $table->string('apellidos', 255)->nullable();
            $table->string('cedula', 20)->index()->nullable();
            $table->string('sexo', 1)->nullable();
            $table->dateTime('fecha_nacimiento')->nullable();
            $table->double('peso',2)->nullable();
            $table->double('altura', 2)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('numero_historia', 10)->nullable();
            $table->string('cedulaRepresentanteLegal')->nullable();
            $table->string('nombreRepresentanteLegal', 255)->nullable();
            $table->string('telefonoFijo', 20)->nullable();
            $table->string('telefonoMovil', 20)->nullable();
            $table->bigInteger('id_institucion')->unsigned()->index();
            $table->timestamps();
            $table->primary(['id','id_institucion']);
            $table->integer('id_patient_mediris')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_pacientes');
    }
}
