<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordsarchivosescaneados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_archivos_escaneados', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->index();
            $table->string('type',45);
            $table->string('name',250);
            $table->string('filename',150);
            $table->string('description',45);
            $table->string('location',255);
            $table->bigInteger('id_solicitud')->unsigned()->index();
            $table->bigInteger('id_institucion')->unsigned()->index();
            $table->timestamps();
            $table->index(['id','id_institucion']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_archivos_escaneados');
    }
}
