<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeingsKeysOldrecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('old_records_estatuses', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');

        });
        Schema::table('old_records_modalidades', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');
        });
        Schema::table('old_records_procedimientos', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');
            $table->foreign('id_modalidad')->references('id')->on('old_records_modalidades');
        });
        Schema::table('old_records_usuarios', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');

        });
        Schema::table('old_records_pacientes', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');

        });
        Schema::table('old_records_solicitudes', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');
            $table->foreign('id_paciente')->references('id')->on('old_records_pacientes');
        });
        Schema::table('old_records_ordenes', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');
            $table->foreign('id_estatus')->references('id')->on('old_records_estatuses');
            $table->foreign('id_procedimiento')->references('id')->on('old_records_procedimientos');
            $table->foreign('id_solicitud')->references('id')->on('old_records_solicitudes');
            $table->foreign('id_usr')->references('id')->on('old_records_usuarios');
        });
        Schema::table('old_records_archivos_escaneados', function(Blueprint $table){

            $table->foreign('id_institucion')->references('id')->on('old_records_instituciones');
            $table->foreign('id_solicitud')->references('id')->on('old_records_solicitudes');

        });

/*
               Schema::table('orordens', function(Blueprint $table){

                        $table->foreign('id_procedimiento')->references('id')->on('orprocedimientos');

                       $table->foreign('id_estatus')->references('id')->on('orestatuses');

                   $table->foreign('id_solicitud')->references('id')->on('orsolicituds');

                        $table->foreign('id_usr')->references('id')->on('orusuarios');

                    });



                Schema::table('orsolicituds', function(Blueprint $table){

                        $table->foreign('id_paciente')->references('id')->on('orpacientes');

                       // $table->foreign('id_usuario')->references('id')->on('orusuarios');

                    });

                Schema::table('orarchivosescaneados', function(Blueprint $table){

                        $table->foreign('id_solicitud')->references('id')->on('orsolicituds');

                   });


*/
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('old_records_archivos_escaneados', function(Blueprint $table){
            $table->dropForeign('old_records_archivos_escaneados_id_institucion_foreign');
            $table->dropForeign('old_records_archivos_escaneados_id_solicitud_foreign');
        });
        Schema::table('old_records_estatuses', function(Blueprint $table){
            $table->dropForeign('old_records_estatuses_id_institucion_foreign');
        });
        Schema::table('old_records_modalidades', function(Blueprint $table){
            $table->dropForeign('old_records_modalidades_id_institucion_foreign');
        });

        Schema::table('old_records_solicitudes', function(Blueprint $table){
            $table->dropForeign('old_records_solicitudes_id_institucion_foreign');
        });
        Schema::table('old_records_pacientes', function(Blueprint $table){
            $table->dropForeign('old_records_pacientes_id_institucion_foreign');
        });
        Schema::table('old_records_usuarios', function(Blueprint $table){
            $table->dropForeign('old_records_usuarios_id_institucion_foreign');
        });
        Schema::table('old_records_procedimientos', function(Blueprint $table){
            $table->dropForeign('old_records_procedimientos_id_institucion_foreign');
            $table->dropForeign('old_records_procedimientos_id_modalidad_foreign');
        });
        Schema::table('old_records_ordenes', function(Blueprint $table){
            $table->dropForeign('old_records_ordenes_id_institucion_foreign');
            $table->dropForeign('old_records_ordenes_id_estatus_foreign');
        });

    }
}
