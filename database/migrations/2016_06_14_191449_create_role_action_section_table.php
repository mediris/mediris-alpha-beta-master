<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleActionSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_action_section', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->index();
            $table->integer('action_section_id')->unsigned()->index();
            $table->primary(['role_id', 'action_section_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_action_section');
    }
}
