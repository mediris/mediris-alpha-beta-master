<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GroupsSeeder::class);
        $this->call(RoutesSeeder::class);
        //$this->call(SectionsSeeder::class);
        //$this->call(ActionsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(SuffixesSeeder::class);
        $this->call(PrefixesSeeder::class);
        $this->call(UsersSeeder::class);
        //$this->call(ActionSectionSeeder::class);
        $this->call(RoleActionSectionSeeder::class);
        $this->call(SexesSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(IconsSeeder::class);
        $this->call(AlertMessageTypesSeeder::class);
        $this->call(ReportsSeeder::class);
        $this->call(ParamsSeeder::class);
        $this->call(ReportParamSeeder::class);
    }
}
