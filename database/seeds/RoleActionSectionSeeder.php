<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Section;
use App\Action;

class RoleActionSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        // Generate Roles
        $roles     = array();
        $tmp_roles = Role::all();
        foreach ($tmp_roles as $key => $value) {
            $roles[$value->name] = $value->id;
        }

        // Generate Actions
        $actions     = array();
        $tmp_actions = Action::all();
        foreach ($tmp_actions as $key => $value) {
            $actions[$value->name] = $value->id;
        }

        // Generate Sections
        $sections     = array();
        $tmp_sections = Section::all();
        foreach ($tmp_sections as $key => $value) {
            $sections[$value->name] = $value->id;
        }

        //Se asignan las acciones para cada Rol
       /*
        *  1 system                
        *  2 institutions          
        *  3 modalities            
        *  4 users                 
        *  5 roles                 
        *  6 configurations        
        *  7 divisions             
        *  8 printers              
        *  9 rooms                 
        * 10 consumables           
        * 11 units                 
        * 12 patientTypes          
        * 13 migrations            
        * 14 administration        
        * 15 sources               
        * 16 templates             
        * 17 notification_templates
        * 18 suspend_reasons       
        * 19 equipment             
        * 20 alertMessages         
        * 21 steps                 
        * 22 procedures            
        * 23 orders                
        * 24 referrings            
        * 25 patientStates         
        * 26 docent-file           
        * 27 categories            
        * 28 subcategories         
        * 29 patients              
        * 30 legacypatients        
        * 31 reception             
        * 32 appointments          
        * 33 technician            
        * 34 radiologist           
        * 35 finalreport           
        * 36 preadmission          
        * 37 search                
        * 38 reports               
        * 39 transcriber           
        * 40 results     
        * 41 polls
        */

        $role_section = array();

        // 1- root - todos 
        $role_section[$roles['root']] = [
            $sections["system"],
            $sections["institutions"],
            $sections["modalities"],
            $sections["users"],
            $sections["roles"],
            $sections["configurations"],
            $sections["divisions"],
            $sections["printers"],
            $sections["rooms"],
            $sections["consumables"],
            $sections["units"],
            $sections["patientTypes"],
            $sections["administration"],
            $sections["migrations"],
            $sections["sources"],
            $sections["templates"],
            $sections["notification_templates"],
            $sections["suspend_reasons"],
            $sections["equipment"],
            $sections["alertMessages"],
            $sections["steps"],
            $sections["procedures"],
            $sections["orders"],
            $sections["referrings"],
            $sections["patientStates"],
            $sections["categories"],
            $sections["subcategories"],
            $sections["patients"],
            $sections["reception"],
            $sections["appointments"],
            $sections["technician"],
            $sections["radiologist"],
            $sections["preadmission"],
            $sections["search"],
            $sections["reports"],
            $sections["transcriber"],
            $sections["results"],
            $sections["legacypatients"],
            $sections["docent-file"],
            $sections["finalreport"],
            $sections["polls"],
        ];

        // 2 - asistente 
        /*
         * administativo 
         * recepcionista 
         * preingreso
         * paciente
         */
        $role_section[$roles['asistente administativo']] = [
            $sections["administration"],
            $sections["reception"],
            $sections["preadmission"],
            $sections["patients"]
        ];

        // 3 - auxiliar de entrega de resultados
        /*
         * resultados 
         * referente 
         * paciente
         */
        $role_section[$roles['auxiliar de entrega de resultados']] = [
            $sections["results"],
            $sections["referrings"],
            $sections["patients"]
        ];

        // 4- coordinador 
        /* cita 
         * recepcionista 
         * preingreso 
         * tecnico 
         * radiologo 
         * transcriptor 
         * resultados 
         * referente 
         * administrador 
         * reportes 
         * paciente 
         * entrevistador
         */
        $role_section[$roles['coordinador']] = [
            $sections["appointments"],
            $sections["reception"],
            $sections["preadmission"],
            $sections["technician"],
            $sections["radiologist"],
            $sections["referrings"],
            $sections["administration"],
            $sections["reports"],
            $sections["patients"]
            // Interview doesn't exist
        ];

        // 5 - radiologo 
        /* tecnico 
         * radiologo 
         * resultados 
         * paciente
         */
        $role_section[$roles['radiólogo']] = [
            $sections["technician"],
            $sections["radiologist"],
            $sections["results"],
            $sections["patients"]
        ];

        // 6 - recepcionista 
        /* cita 
         * recepcionista 
         * resultados 
         * paciente
         */
        $role_section[$roles['recepcionista']] = [
            $sections["appointments"],
            $sections["reception"],
            $sections["results"],
            $sections["patients"]
        ];

        // 7 - sistemas - administracion.
        $role_section[$roles['sistemas']] = [
            $sections["patients"],
            // Interview doesn't exist
            $sections["categories"],
            $sections["subcategories"],
            $sections["orders"],
            $sections["referrings"],
            $sections["sources"],
            $sections["templates"]
        ];

        // 8 - tecnico 
        /* entrevista 
         * tecnico 
         * paciente
         */
        $role_section[$roles['técnico']] = [
            // Interview doesn't exist
            $sections["technician"],
            $sections["patients"]
        ];

        // 9 - telefonista 
        /* cita 
         * resultados 
         * paciente
         */
        $role_section[$roles['telefonista']] = [
            $sections["appointments"],
            $sections["results"],
            $sections["patients"]
        ];

        // 10 - transcriptor 
        /*
         * transcripcion
         */
        $role_section[$roles['transcriptor']] = [
            $sections["transcriber"]
        ];

        foreach ($role_section as $role => $sections)
        {
            foreach ($sections as $section) {
                $actions_sections = Section::find($section)->actions;
                foreach ( $actions_sections as $action_section)
                {
                    Role::find($role)->roleForSection()->attach(['action_section_id' => $action_section->pivot->id]);
                }
            }
        }
 
        /* OLD WAY
        foreach ($role_section as $role => $sections)
        {
            for($i = 0; $i < count($sections); $i++)
            {
                $actions_sections = Section::find($sections[$i])->actions;
                //dd($actions_sections);
                foreach ( $actions_sections as $action_section)
                {
                    //dump($action_section);
                    Role::find($role)->roleForSection()->attach(['action_section_id' => $action_section->pivot->id]);
                }
            }
        }
        */
    }
}
