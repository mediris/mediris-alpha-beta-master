<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Evaluar la creación del seeder segun el idioma seleccionado o la traducción de los datos
        //$roles = ['root', 'administrator', 'radiologist','technician',  'transcriber'];

        // $roles = ['root','administrative assistant', 'delivery assistant', 'coordinator', 'radiologist', 'receptionist', 'systems', 'technician','telephonist', 'transcriber'];
       $roles = ['root','asistente administativo','auxiliar de entrega de resultados','coordinador','radiólogo','recepcionista','sistemas','técnico','telefonista','transcriptor'];

        foreach($roles as $role)
        {
            Role::create([
                'name' => $role,
                'active' => 1
            ]);
        }
    }
}
