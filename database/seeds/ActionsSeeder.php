<?php

use Illuminate\Database\Seeder;
use App\Action;

class ActionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($actions = null)
    {
        if(is_null($actions)){
            $actions = ['index', 'add', 'edit', 'show', 'delete', 'active', 'printer', 'rewrite', 'suspend', 'lock', 'approve', 'cancel', 'addendum', 'revert'];
        }

        foreach($actions as $action){
            Action::create([
                'name'      => $action['value'],
                'display'   => $action['display'],
                'active'    => 1
            ]);
        }
    }
}
