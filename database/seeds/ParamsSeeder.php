<?php

use Illuminate\Database\Seeder;
use App\Parameter;

class ParamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Parameter::create([
            'description' => 'startDate',
            'type' => 'date',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'endDate',
            'type' => 'date',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'modalities',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'procedure',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'patientType',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'users',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'requestedProcedureStatus',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'appointmentStatus',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'rooms',
            'type' => 'class',
            'location' => '0'
        ]);

        Parameter::create([
            'description' => 'referring',
            'type' => 'class',
            'location' => '0'
        ]);
    }
}
