<?php

use Illuminate\Database\Seeder;
use App\Institution;
use App\Charge;

class ChargeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        $institutions = count(Institution::all());

        for($i = 0; $i<$institutions; $i++){

            Charge::create(['user_id' => 1, 'role_id' => 1, 'institution_id' => $i+1]);
            //Charge::create(['user_id' => 2, 'role_id' => 1, 'institution_id' => $i+1]);
            Charge::create(['user_id' => 3, 'role_id' => 1, 'institution_id' => $i+1]);
            Charge::create(['user_id' => 4, 'role_id' => 5, 'institution_id' => $i+1]);
            Charge::create(['user_id' => 5, 'role_id' => 8, 'institution_id' => $i+1]);
            Charge::create(['user_id' => 6, 'role_id' => 10, 'institution_id' => $i+1]);
        }

    }
}
