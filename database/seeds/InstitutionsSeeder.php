<?php

use Illuminate\Database\Seeder;
use App\Institution;

class InstitutionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $env_url = env('APP_URL', '');
        if ( $env_url == 'http://mediris.dev' ) {
            //ambiente de desarrollo
            $urls = ['http://api-meditron1.dev/', 'http://api-meditron2.dev/', 'http://api-meditron3.dev/'];
        } else if ( $env_url == 'http://mediris.local' ) {
            //ambiente de desarrollo
            $urls = ['http://api-meditron1.local/', 'http://api-meditron2.local/', 'http://api-meditron3.local/'];
        } else if ( $env_url == 'http://beta.mediris.logoscorp.com' ) {
            //ambiente de pruebas 
            $urls = ['http://betaapi.mediris.logoscorp.com/'];
        } else if($env_url=='http://mediris.logoscorp.com') {
            //ambiente de producción
            $urls = ['https://api.meditron.logoscorp.com/', 'https://api2.meditron.logoscorp.com/'];    
        }else if ($env_url == 'http://mediris.test'){
            $urls = ['http://api-meditron1.test/'];
        }
        
        for($i = 1; $i <= count($urls); $i++)
        {
            Institution::create([
                'active' =>1,
                'url' => $urls[$i - 1],
                'institution_id' => 'INST_'.$i,
                'telephone_number' => '2128676829',
                'email' => 'meditron'.$i.'@logoscorp.com',
                'name' => 'Meditron '.$i,
                'address' => 'LogosCorp, Torre Arbicenter PB-3, Calle Orinoco, Caracas 1060, Distrito Capital, Venezuela',
                'logo' => '',
                'logo_email' => '',
                //'administrative_ID' => 'MEDI_LOGOS_14_06_16_'.$i,
                'administrative_ID' => '0'.$i,
                'group_id' => 1,
                'url_pacs' => 'http://pacsmeditronoasis.dnsalias.com',
                'base_structure' => '1.2.826.0.1.3680043.9.67.1.2'.$i,
            ]);
        }

    }
}
