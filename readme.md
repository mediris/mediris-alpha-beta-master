## INSTALACION DE PROYECTO MEDIRIS EN UBUNTU SERVER 16.04 Y CENTOS 7

## Instalación del Servidor

LAMP (Apache2, MySQL y PHP)

	sudo apt-get install python-software-properties
	sudo add-apt-repository ppa:ondrej/php
	sudo apt-get update
	sudo apt-get install -y php5.6
	sudo apt-get -y install php5.6 php5.6-mcrypt php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip
	sudo apt-get install php5.6-xml
	sudo apt-get -y install tar unzip zip

Intercambiar PHP7 por PHP 5.6

	sudo a2dismod php7.1   (Aplicar a la version correcta)
	sudo a2enmod php5.6
	sudo service apache2 restart
	sudo update-alternatives --set php /usr/bin/php5.6 (para cambiar manualmente entre las versiones de php disponibles, puedes usar el siguiente comando: update-alternatives --config php
	php -v (Debería mostrar que la versión actual es PHP 5.6)

Composer

	sudo apt-get install composer

NPM

	sudo apt-get install npm

NodeJS

	curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
	sudo apt-get install -y nodejs
	sudo apt-get install -y build-essential
	En caso de Problemas con las versiones de NPM y NodeJS revisar el siguiente documento: 

Resolver Problemas con NPM y NodeJS

## Configuración del Servidor

Clonar los proyectos mediris-alpha-beta-master y api-meditron usando git.

	git clone https://<NOMBRE_USUARIO>@bitbucket.org/logoscorp/mediris-alpha-beta-master.git
	git clone https://<NOMBRE_USUARIO>@bitbucket.org/logoscorp/api-meditron.git

Crear las bases de datos de mediris y apimeditron

	mysql -u root -p (solicitara el password del usuario root de mysql, debemos ingresarlo)
	create database mediris;
	create database apimeditron;
	
Configurar los Virtualhosts

	Ir al directorio /etc/apache2/sites-available
	Crear los archivos: mediris.conf y apimeditron.conf
	El archivo debe quedar con los datos correspondientes:
		<VirtualHost *:80>
    		ServerName <HOST>  #Ejemplo mediris.dev
    		ServerAlias <HOST>  #Ejemplo mediris.dev
    		ServerAdmin webmaster@localhost
    		DocumentRoot <RUTA_PROYECTO>/public  #Ejemplo /home/developer/mediris/public
    		<Directory <RUTA_PROYECTO>/public>
        		Options Indexes FollowSymLinks MultiViews
        		AllowOverride All
    		</Directory>
   			ErrorLog <RUTA_PROYECTO>/error.log
   			CustomLog <RUTA_PROYECTO>/access.log combined
		</VirtualHost>
		
Habilitar los sitios previamente configurados

	sudo a2ensite <NOMBRE_DE_ARCHIVO_DE_CONFIGURACION>

Editar el archivo /etc/apache2/apache2.conf

	<Directory <RUTA_PROYECTO> > #Ejemplo /home/developer
    	Options Indexes FollowSymLinks
    	AllowOverride None
    	Require all granted
	</Directory>

Agregar en el archivo /etc/hosts el nombre de los Host que se colocaron en los Virtualhost

	127.0.0.1 mediris.dev
	127.0.0.1 api-meditron1.dev
	127.0.0.1 api-meditron2.dev
	127.0.0.1 api-meditron3.dev

Cambiar los permisos del apache para que funcione local

	Editar el archivo /etc/apache2/envars
	Cambiar el usuario local en lo siguiente:
		export APACHE_RUN_USER=<USUARIO>
		export APACHE_RUN_GROUP=<USUARIO>

Habilitamos modo de escritura de apache

	sudo a2enmod rewrite

Reiniciar Apache

	sudo service apache2 restart


## Configuración del Proyecto

Copiar y Configurar Archivo de Ambiente

	Tanto en mediris como en api-meditron copiar el archivo de ambiente

		sudo cp .env.example .env

	Dentro del archivo .env cambiar la configuración correspondiente de la Base de Datos
	También el APP_URL colocar el host que habían configurado en pasos anteriores

Configuración, Inicialización y Actualización de los Proyectos

	Ejecutar primero en API-Meditron

		composer install
		php artisan key:generate
		php artisan migrate:refresh --seed
		php artisan config:cache
		composer dump-autoload

	Ejecutar en Mediris

	Editar el archivo .env ubicado en la raíz del proyecto api-meditron y copiar el key que se generó en el paso anterior que está identificado como APP_KEY, luego editar el archivo .env del proyecto mediris y pegarlo en la misma ubicación identificada como APP_KEY, guardar los cambios y salir del editor.
	Ubicarse dentro de la raíz del proyecto mediris y ejecutar los siguientes comandos:

		mkdir -p storage/framework
		mkdir -p storage/framework/cache
		mkdir -p storage/framework/sessions
		mkdir -p storage/framework/views
		mkdir -p public/images/temp
		composer install
		php artisan key:generate
		php artisan migrate:refresh --seed
		php artisan cache:clear
		php artisan config:clear
		php artisan view:clear
		composer dump-autoload

Instalación de Gulp (Ambos Proyectos)

	sudo npm install --global gulp-cli
	Comprobar usando gulp -v

	Ir a la carpeta de cada uno de los proyectos y ejecutar:

		npm install
		gulp

Ingreso al Mediris

	Ir a la página principal del mediris (login), debería colocarse http://<HOST>, por ejemplo: http://mediris.dev
	Ingresar al sistema
		Usuario: desarrollo@logoscorp.com
		Password: l0g0sc0rp
	Luego de ingresar se debe ir a la sección de instituciones: http://<HOST>/institutions
	Editar cada institución y colocar la URL que se habia configurado localmente
	Luego cerrar la sesión, ingresa nuevamente, selecciona una institución y debería funcionar.

## Configuracion de JavaBridge para los Reportes

	Archivo requerido: Instalacion-Jasper.tar.gz
	
	Ejecutar los siguientes pasos:
	
		wget http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.54/bin/apache-tomcat-7.0.54.tar.gz
		tar -xvf apache-tomcat-7.0.54.tar.gz
		sudo mv apache-tomcat-7.0.54 /usr/local/
		
	Colocar en el archivo: /usr/local/apache-tomcat-7.0.54/conf/tomcat-users.xml
	
		<role rolename="manager-gui"/>
		<role rolename="admin-gui"/>
		<user username="admin" password="admin" roles="manager-gui,admin-gui"/>

	Crear un archivo llamado "tomcat754" en /etc/init.d/ y pegar el contenido a continuación:

		#!/bin/bash
		export CATALINA_HOME=/usr/local/apache-tomcat-7.0.54
		PATH=/sbin:/bin:/usr/sbin:/usr/bin
		start() {
		    sh $CATALINA_HOME/bin/startup.sh
		}
		stop() {
		    sh $CATALINA_HOME/bin/shutdown.sh
		}
		case $1 in
		  start|stop) $1;;
		  restart) stop; start;;
		  *) echo "Run as $0 <start|stop|restart>"; exit 1;;
		esac

	Cambiar permisos:
	
		sudo chmod 755 /etc/init.d/tomcat754
		Iniciar el servicio:
		sudo /etc/init.d/tomcat754 start

	Iniciar automáticamente el servicio:
	
		sudo update-rc.d tomcat754 defaults
		Modificar el "max-file-size" y "max-request-size" del archivo web.xml y colocar el tamaño como 55428800:
		sudo nano /usr/local/apache-tomcat-7.0.54/webapps/manager/WEB-INF/web.xml

	Ir al browser a la direccion del tomcat:
	
		http://localhost:8080

	Seleccionar la opción de "Manager App" y luego te pedirá la autorización que previamente se había colocado en tomcat-users.xml
	En la sección de Deploy se encuentra la opción de elegir un archivo .WAR, selecciona el JavaBridge.war (Dentro del empaquetado) y luego seleccionar el botón Deploy

	Editar el archivo Java.inc dentro de tomcat:

		sudo nano webapps/JavaBridge/java/Java.inc

	Ir a la clase java_Client y agregar:  public $cancelProxyCreationTag;
	Habilitar el modo de agregar URL en los includes:

		sudo nano /etc/php/5.6/apache2/php.ini

	Aproximadamente en la linea 835 se encuentra la opcion: allow_url_include = On

	Reiniciar el Apache.
	
		sudo service apache2 restart

	Verificar el funcionamiento colocando en algun controlador la siguiente línea:

		require_once("http://127.0.0.1:8080/JavaBridge/java/Java.inc");

	Copiar las librerías .JAR en las siguientes ubicaciones:

		/usr/local/apache-tomcat-7.0.54/lib/
		/usr/lib/jvm/java-1.7.0-openjdk-amd64/jre/lib/ext/

## Configuración del idioma

	Editar el archivo app.php ubicado en la carpeta “config” del proyecto mediris.

		nano config/app.php

	Ubicar la variable “locale” la cual por defecto estara en español (es) y colocarla en inglés (en).

		'locale' => 'es'

	por default estará en español

	para inglés se cambia a: 'locale' => 'en'

Nota: Para el funcionamiento de las cámaras que se utilizaran para el escaneo de documentos, el servidor debe contar con un certificado SSL emitido por una autoridad de confianza.


## INSTALACION DE PROYECTO MEDIRIS EN CENTOS 7

## Instalación del Servidor

LAMP (Apache2, MySQL y PHP)

	sudo yum -y install python python-tools tar unzip zip nano wget git libnotify policycoreutils-python
	rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
	sudo yum -y install php56w php56w-opcache
	sudo yum -y install php56w-fpm php56w-opcache (Alternativo al comando anterior)
	sudo yum -y install php56w-mcrypt php56w-mbstring php56w-curl php56w-cli php56w-mysql php56w-gd php56w-intl php56w-xsl php56w-zip
	wget https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
	md5sum mysql57-community-release-el7-11.noarch.rpm
	sudo rpm -ivh mysql57-community-release-el7-11.noarch.rpm
	sudo yum -y install mysql-server
	sudo grep 'temporary password' /var/log/mysqld.log (Se mostrara Password temporal del usuario root de MySQL, toma nota del mismo)
	sudo mysql_secure_installation (Se le solicitará la clave del paso anterior y deberá cambiarla por una que cumpla con los criterios mínimos de seguridad)
	usermod -aG apache root

Iniciar Apache, MySQL y Verificar Versión de PHP

	sudo systemctl start httpd
	sudo systemctl start mysqld
	php -v (Debería mostrar que la versión actual es PHP 5.6)

Composer

	sudo yum -y install composer

NodeJS & NPM

	yum -y install gcc-c++ make
	curl -sL https://rpm.nodesource.com/setup_8.x | sudo -E bash -
	yum install -y nodejs
	node -v (Para confirmar la versión)
	npm -v (Para confirmar la versión)

En caso de Problemas con las versiones de NPM y NodeJS revisar el siguiente documento: 

Resolver Problemas con NPM y NodeJS

## Configuración del Servidor

Crear las bases de datos de mediris, apimeditron y worklist

	mysql -u root -p (solicitara el password del usuario root de mysql, debemos ingresarlo)
	create database mediris;
	create database apimeditron;
	create database worklist;
	quit (Para salir de la interfaz de MySQL)

Crear Nuevo Usuario de Base de Datos

	mysql -u root -p (solicitara el password del usuario root de mysql, debemos ingresarlo)
	CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
	GRANT ALL PRIVILEGES ON worklist.* TO 'newuser'@'localhost';
	FLUSH PRIVILEGES;
	quit (Para salir de la interfaz de MySQL)

Configurar los Virtualhosts

	sudo mkdir -p /var/www/html/mediris (Esta el la ruta del proyecto mediris)
	sudo mkdir -p /var/www/html/apimeditron (Esta es la ruta de la API)
	sudo mkdir -p /etc/httpd/sites-available
	sudo mkdir -p /etc/httpd/sites-enabled
	sudo nano /etc/httpd/conf/httpd.conf

	Agregar la siguiente línea al final del archivo:

		IncludeOptional sites-enabled/*.conf
		
	Guardar y cerrar el archivo (Ctrl + o, Ctrl + x).
	
	Ir al directorio /etc/httpd/sites-available
	
	Crear los archivos: mediris.conf y apimeditron.conf
	
		sudo nano /etc/httpd/sites-available/mediris.conf
		sudo nano /etc/httpd/sites-available/apimeditron.conf
		
	El archivo debe quedar con los datos correspondientes:
	
		<VirtualHost *:80>
		    ServerName <HOST>  #Ejemplo mediris.dev
		    ServerAlias <HOST>  #Ejemplo mediris.dev
		    ServerAdmin webmaster@localhost
		    DocumentRoot <RUTA_PROYECTO>/public  #Ejemplo /home/developer/mediris/public
		    <Directory <RUTA_PROYECTO>/public>
		        Options Indexes FollowSymLinks MultiViews
		        AllowOverride All
		    </Directory>
		   ErrorLog /var/log/httpd/NOMBRE_PROYECTO_error.log
		   CustomLog /var/log/httpd/NOMBRE_PROYECTO_access.log combined
		</VirtualHost>
		
	Habilitar los sitios previamente configurados

		sudo ln -s /etc/httpd/sites-available/<NOMBRE_DE_ARCHIVO_DE_CONFIGURACION> /etc/httpd/sites-enabled/<NOMBRE_DE_ARCHIVO_DE_CONFIGURACION>

	Editar el archivo /etc/httpd/conf/httpd.conf

		<Directory <RUTA_PROYECTO> > #Ejemplo /home/developer
		    Options Indexes FollowSymLinks
		    AllowOverride None
		    Require all granted
		</Directory>

	Clonar los proyectos mediris-alpha-beta-master y api-meditron usando git.

		cd /var/www/html/NOMBRE_PROYECTO_MEDIRIS
		git clone https://<NOMBRE_USUARIO>@bitbucket.org/logoscorp/mediris-alpha-beta-master.git
		cd /var/www/html/NOMBRE_PROYECTO_API
		git clone https://<NOMBRE_USUARIO>@bitbucket.org/logoscorp/api-meditron.git

	Ejecutar los siguientes comandos para cada uno de los proyectos:

		semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/NOMBRE_PROYECTO/storage(/.*)?"
		semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/NOMBRE_PROYECTO/bootstrap/cache(/.*)?"
		semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/NOMBRE_PROYECTO/public(/.*)?"
		restorecon -Rv "/var/www/html/NOMBRE_PROYECTO/storage"
		restorecon -Rv "/var/www/html/NOMBRE_PROYECTO/bootstrap/cache"
		restorecon -Rv "/var/www/html/NOMBRE_PROYECTO/public"

	Reiniciar Apache

		sudo systemctl restart httpd



## Configuración del Proyecto

Copiar y Configurar Archivo de Ambiente

	Tanto en mediris como en api-meditron copiar el archivo de ambiente

		sudo cp .env.example .env

	Editar el archivo .env y cambiar la configuración correspondiente de la Base de Datos

		sudo nano .env

	También el APP_URL colocar el host que habían configurado en pasos anteriores

	Agregar las siguientes líneas en el archivo .env (Este paso sólo debe realizarse en el proyecto apimeditron):

		DB_HOST_WL=127.0.0.1
		DB_PORT_WL=3306
		DB_DATABASE_WL=worklist
		DB_USERNAME_WL=newuser (Sustituir por el usuario MySQL creado)
		DB_PASSWORD_WL=password

Configuración, Inicialización y Actualización de los Proyectos

	Ejecutar primero en API-Meditron

		composer install
		php artisan key:generate
		php artisan migrate:refresh --seed
		php artisan config:cache
		composer dump-autoload

	Ejecutar en Mediris

	Editar el archivo .env ubicado en la raíz del proyecto api-meditron y copiar el key que se generó en el paso anterior que está identificado como APP_KEY, luego editar el archivo .env del proyecto mediris y pegarlo en la misma ubicación identificada como APP_KEY, guardar los cambios y salir del editor.

	Ubicarse dentro de la raíz del proyecto mediris y ejecutar los siguientes comandos:

		mkdir -p storage/framework
		mkdir -p storage/framework/cache
		mkdir -p storage/framework/sessions
		mkdir -p storage/framework/views
		mkdir -p public/images/temp
		composer install
		php artisan key:generate
		php artisan migrate:refresh --seed
		php artisan cache:clear
		php artisan config:clear
		composer dump-autoload
		php artisan view:clear

Instalación de Gulp (Ejecutar en Ambos Proyectos)

	sudo npm install --global gulp-cli
	gulp -v (Comprobar versión)
	npm install
	gulp

Ingreso al Mediris

	Ir a la página principal del mediris (login), debería colocarse http://<HOST>, por ejemplo: http://mediris.dev

	Ingresar al sistema

		Usuario: desarrollo@logoscorp.com
		Password: l0g0sc0rp

	Luego de ingresar se debe ir a la sección de instituciones: http://<HOST>/institutions

	Editar cada institución y colocar la URL que se habia configurado localmente

	Luego cerrar la sesión, ingresa nuevamente, selecciona una institución y debería funcionar.

# Configuracion de JavaBridge para los Reportes

	Archivos requeridos: Instalacion-Jasper.zip

Ejecutar los siguientes pasos:

	sudo yum -y install tomcat tomcat-webapps tomcat-admin-webapps
	su -c "yum install java-1.7.0-openjdk"

	Editar el archivo y agregar la siguiente línea:

		sudo nano /usr/share/tomcat/conf/tomcat.conf

		JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true -Xmx512m -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC"
				
	Editar el archivo tomcat-users.xml y agregar en la penúltima línea, las líneas que se indican a continuación:

		sudo nano /usr/share/tomcat/conf/tomcat-users.xml

			<role rolename="manager-gui"/>
			<role rolename="admin-gui"/>
			<user username="admin" password="admin" roles="manager-gui,admin-gui"/>

	Guardar los cambios y cerrar el editor (Ctrl + o ,  Ctrl + x)

	Iniciar el servicio:

		sudo systemctl start tomcat

	Iniciar automáticamente el servicio:

		sudo systemctl enable tomcat

	Modificar el "max-file-size" y "max-request-size" del archivo web.xml y colocar el tamaño como 55428800:

		sudo nano /usr/share/tomcat/webapps/manager/WEB-INF/web.xml

	Ir al browser a la direccion del tomcat:

		http://server_IP_address:8080

	Seleccionar la opción de "Manager App" y luego te pedirá la autorización que previamente se había colocado en tomcat-users.xml

	En la sección de Deploy se encuentra la opción de elegir un archivo .WAR, selecciona el JavaBridge.war (Ubicado dentro del empaquetado Instalacion-Jasper.zip descargado previamente) y luego seleccionar el botón Deploy

	Editar el archivo Java.inc dentro de tomcat:

		sudo nano /usr/share/tomcat/webapps/JavaBridge/java/Java.inc

	Ir a la clase java_Client y agregar:  public $cancelProxyCreationTag;

	Habilitar el modo de agregar URL en los includes:

		sudo nano /etc/php.ini

	Buscar la opcion: allow_url_include = Off y cambiarla a allow_url_include = On

	Reiniciar el Apache.

		sudo systemctl restart httpd

	Copiar las librerías .JAR  (Ubicadas dentro del empaquetado Instalacion-Jasper.zip descargado previamente) en las siguientes ubicaciones:

		/usr/share/tomcat/lib/
		/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.141-2.6.10.1.el7_3.x86_64/jre/lib/


## Configuración del idioma

	Editar el archivo app.php ubicado en la carpeta “config” del proyecto mediris.

		nano config/app.php

	Ubicar la variable “locale” la cual por defecto estara en español (es) y colocarla en inglés (en).

		'locale' => 'es'

	por default estará en español

	para inglés se cambia a: 'locale' => 'en'



## Troubleshooting

# HTTP ERROR 500

	En servidores CentOS es común conseguirnos con errores 500 posterior a la creación y/o deploy de sitios y proyectos nuevos, como es el caso de las instituciones. Muchos de estos errores están relacionados con la seguridad de SELinux (módulo que se encarga de dar seguridad al Kernel de Linux y permite la administración de políticas de seguridad) y no a problemas de conexión con la base de datos.
	Para descartar que el error corresponda a un bloqueo de SELinux y no a problemas de conexión con la base de datos, se debe ejecutar el siguiente comando:

		sudo setenforce 0 (Este comando apaga el módulo SELinux)

	Luego de ejecutar el comando hacemos pruebas nuevamente para ingresar al sitio o aplicación web, si este accede sin problemas, estamos ante un bloque de seguridad de SELinux. Ahora procedemos a encender el módulo nuevamente con el siguiente comando:

		sudo setenforce 1 (Este comando enciende el módulo SELinux)

	Apagar este módulo es una muy mala práctica, ya que representa una vulnerabilidad para el sistema operativo, en lugar de ello, lo mantenemos encendido y agregamos políticas para que el módulo permita la escritura en algunas de las carpetas de nuestro sitio y/o aplicación web.

	Ejecutar los siguientes comandos para cada una de los proyectos:

		semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/PROYECTO/storage(/.*)?"
		semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/PROYECTO/bootstrap/cache(/.*)?"
		restorecon -Rv "/var/www/html/PROYECTO/storage"
		restorecon -Rv "/var/www/html/PROYECTO/bootstrap/cache"

	Luego de realizar este tipo de cambios, siempre debemos ejecutar el siguiente comando en la carpeta raíz de cada proyecto para que los mismos sean efectivos:

		php artisan config:clear

## Error de Conexion a Base de Datos al Hacer Login en Mediris

	Este error suele indicar que no se logró establecer la conexión con la base de datos en el servidor “127.0.0.1” con la credenciales definidas en los archivos .env de cada proyecto.
	Para corregir este error se debe cambiar la IP de lookup (127.0.0.1) definida en las conexiones a base de datos del archivo .env y colocar explícitamente “localhost”. Ejemplo:

		ANTES

		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE=dbname
		DB_USERNAME=dbuser
		DB_PASSWORD=password

		DESPUES

		DB_CONNECTION=mysql
		DB_HOST=localhost
		DB_PORT=3306
		DB_DATABASE=dbname
		DB_USERNAME=dbuser
		DB_PASSWORD=password