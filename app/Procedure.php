<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Procedure extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/procedures';
        parent::__construct();
    }

    

    public function hasStep( $id ) {
        foreach ( $this->steps as $step ) {
            if ( $id == $step->id ) {
                return true;
            }
        }

        return false;
    }

    public function hasTemplate( $id ) {
        foreach ( $this->templates as $template ) {
            if ( $id == $template->id ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para eliminar una instancia de Procedure dado un identificador. NO ESTÁ EN USO
     */
    // public function remove( $url, $id ) {
    //     $res = $this->client->request('POST', $url . $this->url . '/delete/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

    //     return $res;
    // }

    /*public function getModalities( $url, $id ) {
        $response = $this->client->request('POST', $url . $this->url . '/modalities/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);

        $modalities = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $modality = new Modality();

            foreach ( $element as $key => $value ) {
                $modality->$key = $value;

            }

            $modalities->push($modality);
        }

        return $modalities;
    }

    public function getModalitiesString( $url, $id ) {
        $modalities = $this->getModalities($url, $id);

        $string = "";

        foreach ( $modalities as $modality ) {
            $string = $string . " " . $modality->name;
        }

        return trim($string);
    }

    public function getModalitiesNumber( $url, $id ) {
        $modalities = $this->getModalities($url, $id);

        $string = "";

        foreach ( $modalities as $modality ) {
            $string = $string . " " . $modality->id;
        }

        return trim($string);
    }

    public function getRooms( $url, $id ) {
        $response = $this->client->request('POST', $url . $this->url . '/rooms/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);

        $rooms = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $room = new Room();

            foreach ( $element as $key => $value ) {
                $room->$key = $value;

            }

            $rooms->push($room);
        }

        return $rooms;
    }*/

    /*public function getRoomsString( $url, $id ) {
        $rooms = $this->getRooms($url, $id);

        $string = "";

        foreach ( $rooms as $room ) {
            $string = $string . " " . $room->administrative_ID;
        }

        return trim($string);
    }*/
}