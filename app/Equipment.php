<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Equipment extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/equipment';
        parent::__construct();
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para eliminar una instancia de Equipment dado un identificador. NO ESTÁ EN USO
     */
    public function remove( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->apibase . '/delete/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }

    /**
     * @fecha: 14-08-2017
     * @parametros:
     * @programador: Hendember Heras
     * @objetivo: Busca todas los equipos filtrados por este endpoint específico para cada institución
                    En este caso: que estén relacionados al procedure
     */
    public static function remoteFindByProcedure( $id ) {
        return parent::remoteFindByAction('/showbyprocedure/' . $id );
    }
}