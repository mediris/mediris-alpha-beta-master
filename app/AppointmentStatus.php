<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;
use Cache;

class AppointmentStatus extends RemoteModel {
	static $cacheKey = 'AppointmentStatus';
	static $cacheDuration = 24*60;

    public function __construct() {
        $this->apibase = 'api/v1/appointmentstatuses';
        parent::__construct();
    }

    static function getValue( $strStatus, $institution = null ) {
    	$cacheKey = self::$cacheKey . $strStatus;
    	$ret = Cache::get($cacheKey);

    	if ( $ret == null ) {
    		$statuses = self::remoteFindAll( [], $institution );

    		foreach( $statuses as $status ) {
    			if ( $status->description == $strStatus ) {
    				$ret = $status->id;
    				Cache::put($cacheKey, $ret, self::$cacheDuration );
    			}
    		}
    	}

    	return $ret;
    }
}