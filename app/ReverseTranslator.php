<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class ReverseTranslator extends \Illuminate\Support\Facades\Facade
{
	/**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'reverse-translator';
    }

    protected $languageGroups = [];

    /**
	 * Load Groups
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param  $locale 			string
     * @return $languageGroups	array
     */
    protected function loadGroups($locale)
    {
        // Iterate through all available groups and store
        $languagePath = resource_path("lang/{$locale}/");
        $languageGroups = array_map(function ($file) use ($languagePath) {
            return str_replace([$languagePath, '.php'], '', $file);
        }, File::glob($languagePath.'*.php'));

        $this->languageGroups[$locale] = $languageGroups;

        return $languageGroups;
    }

    /**
	 * Lower Case
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param 	$value 	string
     * @return 	void 	boolean
     */
    protected function lowercase($value)
    {
        return is_array($value) ? array_map([$this, 'lowercase'], $value) : Str::lower($value);
    }

    /**
	 * Array Search Recursive
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param 	$search 	string
     * @param 	$array 		array
     * @param 	$mode 	 	string
     * @param 	$return 	boolean
     * @return 	void 		string / boolean
     */
    protected function arraySearchRecursive($search, array $array, $mode = 'value', $return = false) {
        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($array));
        foreach ($iterator as $key => $value) {
            if ($search == $value) {
                $keys = [];
                for ($i = $iterator->getDepth()-1; $i >= 0; $i--) {
                    $keys[] = $iterator->getSubIterator($i)->key();
                }
                $keys[] = $key;
                return implode('.', $keys);
            }
        }

        return false;
    }

    /**
	 * Search
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param 	$foreign 	string
     * @param 	$group  	string
     * @param 	$locale  	string
     * @return 	void 		string / boolean
     */
    protected function search($foreign, $group, $locale = 'en')
    {
        // Load all strings for group in current language
        $groupStrings = trans($group);

        // Recursive and case-insensitive search
        return $this->arraySearchRecursive($this->lowercase($foreign), $this->lowercase($groupStrings));
    }

    /**
	 * Get
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param 	$foreign 	string
     * @param 	$group  	string
     * @param 	$locale  	string
     * @return 	$foreign	string
     */
    public function get($foreign, $group = null, $locale = 'en')
    {
        if (!$group) {
            if (!isset($this->languageGroups[$locale])) {
                $this->loadGroups($locale);
            }
            foreach ($this->languageGroups[$locale] as $group) {
                $key = $this->search($foreign, $group, $locale);
                if ($key !== false) {
                    return trans("{$group}.{$key}", [], null, $locale);
                }
            }
            // Failed to match -- return original string
            return $foreign;
        }

        $key = $this->search($foreign, $group, $locale);
        if ($key !== false) {
            return trans("{$group}.{$key}", [], null, $locale);
        }

        // Failed to match -- return original string
        return $foreign;
    }

    /**
	 * Get Key
	 * 
     * @version 04-12-2017
     * @author Alton Bell Smythe
     *
     * @param 	$foreign 	string
     * @param 	$group  	string
     * @param 	$locale  	string
     * @return 	$key 		string
     */
    public function getKey($foreign, $group = null, $locale = 'en')
    {
        if (!$group) {
            if (!isset($this->languageGroups[$locale])) {
                $this->loadGroups($locale);
            }
            foreach ($this->languageGroups[$locale] as $group) {
                $key = $this->search($foreign, $group, $locale);
                if ($key !== false) {
                    return $key;
                }
            }
            // Failed to match -- return original string
            return $foreign;
        }

        $key = $this->search($foreign, $group, $locale);
        if ($key !== false) {
            return $key;
        }

        // Failed to match -- return original string
        return $key;
    }
}