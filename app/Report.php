<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Report extends Model {

    /**
     * @fecha: 22-02-2017
     * @programador: Diego Oliveros
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name',
        'extension',
        'subreport',
        'report_id',
        'role_id'
    ];

    /**
     * @fecha: 22-02-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un Reporte tiene varios Reportes(Subreportes).
     */
    public function parent() {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function children() {
        return $this->hasMany(Report::class, 'report_id');
    }

    /**
     * @fecha: 15-03-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un Reporte tiene varios Parametros.
     */
    public function parameters() {
        return $this->belongsToMany(Parameter::class);
    }

    /**
     * @fecha: 28-03-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un Reporte tiene varios Roles.
     */
    public function roles() {
        return $this->belongsTo(Role::class);
    }

    /**
     * @fecha: 13-03-2017
     * @programador: Diego Oliveros
     * @objetivo: Llamada remota al query del reporte.
     */
    public function get( $url, $data, $name ) {
        //dump($data);

        $client = new Client();
        $base = 'api/v1/reports/';
        $headers = [
            'content-type' => 'application/x-www-form-urlencoded',
            'X-Requested-With' => 'XMLHttpRequest',
        ];
        $response = $client->request('POST', $url . $base . $name, [
            'headers' => $headers,
            'form_params' => [
                'api_token' => \Auth::user()->api_token,
                'user_id' => \Auth::user()->id,
                'data' => $data
            ],
        ]);
        //dump($url . $base . $name);
        //dd(json_decode($response->getBody(), true));
        return json_decode($response->getBody(), true);
    }
}
