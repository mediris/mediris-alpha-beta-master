<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PatientDocuments extends Model {

    public function __construct( array $attributes = array() ) {
        parent::__construct($attributes);

        $this->location = config('constants.paths.patient_document');
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un PatientDocument pertenece a un Patient.
     */
    public function patient() {

        return $this->belongsTo(Patient::class);

    }

    /**
     * @fecha: 22-08-2017
     * @programador: Hendember Heras
     * @objetivo: Move the uploaded file to the storage.
     */
    function moveToStorage( $uFile ) {
        $res = Storage::putFileAs( $this->location, $uFile, $this->filename );
        
        if ( $res === false ) {
            return false;    
        } else {
            return true;
        } 
    }
}
