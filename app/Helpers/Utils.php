<?php

namespace App\Helpers;
  
class Utils {

    /**
     */
    public static function formatCellphoneForMassivaMovil( $cellphone ) {
     	$cellphone = str_replace('(', '', $cellphone);
        $cellphone = str_replace(')', '', $cellphone);
        $cellphone = str_replace('-', '', $cellphone);
        $cellphone = str_replace(' ', '', $cellphone);
        if ( strpos($cellphone, '0') === 0 ) {
            $cellphone = substr($cellphone, 1);
        }
        $cellphone = '58' . $cellphone;

        return $cellphone;
    }

    /**
     * @fecha: 15-01-2018
     * @parametros: $date
     * @programador: Alton Bell Smythe
     * @objetivo: Devuelve un objecto DateTime
     */
    public static function formatDate( $date, $codeLanguage, $format = "%B %d, %Y" ) {

        if(isset($date)) {
            if ( $date == "0000-00-00 00:00:00" ) {
                $return = new \DateTime();
            } else {
                $return = new \DateTime( $date );
            }

            // Date
            setlocale(LC_ALL, $codeLanguage);

            return ucwords(strftime( $format, $return->getTimestamp() ));
        }

    }

    /**
     * @fecha: 15-01-2018
     * @parametros: $language
     * @programador: Alton Bell Smythe
     * @objetivo: setlocale para cierto idioma
     */
    public static function getLanguageEncode(){

        return \App::getLocale() . "_" . strtoupper(\App::getLocale()) . ".utf8";

    }
}