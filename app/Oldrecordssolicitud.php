<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oldrecordssolicitud extends Model {
    protected $table = 'old_records_solicitudes';

    public function ordenes() {
        return $this->hasMany(Oldrecordsorden::class, 'id_solicitud');
    }

    public function institucion() {
        return $this->belongsTo(Oldrecordsinstitucion::class, 'id_institucion', 'id');
    }

    public function paciente() {
        return $this->belongsTo(Oldrecordspaciente::class, 'id_paciente', 'id');
    }
}