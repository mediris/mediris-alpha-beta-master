<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class SmokingStatus extends Model {
    public function __construct() {
        $this->client = new Client();
        $this->url = 'api/v1/smokingstatuses';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener una colección de SmokingStatuses desde el api.
     */
    public function getAll( $url ) {
        $response = $this->client->request('POST', $url . $this->url, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $smokingStatuses = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $smokingStatus = new SmokingStatus();
            foreach ( $element as $key => $value ) {
                $smokingStatus->$key = $value;
            }
            $smokingStatuses->push($smokingStatus);
        }

        /************** Translation and sorting ****************/

        foreach ( $smokingStatuses as $key => $smokingStatus ) {
            $smokingStatuses[$key]->name = trans('statuses.' . $smokingStatus->name);
        }

        $smokingStatuses = $smokingStatuses->toArray();

        usort($smokingStatuses, function ( $a, $b ) {

            if ( $a['name'] == $b['name'] ) {
                return 0;
            }

            return ( $a['name'] < $b['name'] ) ? -1 : 1;
        });

        /*****************************************************/

        return $smokingStatuses;
    }
}
