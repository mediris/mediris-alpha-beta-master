<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\MassivaMovil;

class RequestedProcedureDone extends Notification implements ShouldQueue
{
    use Queueable;

    protected $requestedProcedure;
    protected $templateEmail;
    protected $templateEmailReferer;
    protected $templateSMS;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $requestedProcedure ) {
        $this->requestedProcedure = $requestedProcedure;
        
        //Search system's configuration to verify if we need to send the notification    
        $conf = \App\Configuration::remoteFind(1, [], $requestedProcedure->institution);

        //Search PatientType to verify if we can send the notification
        $patient_type_id = $requestedProcedure->service_request->patient_type_id;
        $patientType = \App\PatientType::remoteFind( $patient_type_id, [], $requestedProcedure->institution );

        $this->templateEmail = null;
        $this->templateEmailReferer = null;
        $this->templateSMS = null;
        
        if ( $conf ) {
            if ( $conf->results_email_auto && isset($conf->results_email_auto_template->id) ) {
                if ( isset($patientType) && $patientType->email_patient ) {
                    $this->templateEmail = $conf->results_email_auto_template;
                }
            }

            if ( $conf->results_email_auto && isset($conf->results_email_referer_template->id) ) {
                if ( isset($patientType) && $patientType->email_refeer ) {
                    $this->templateEmailReferer = $conf->results_email_referer_template;
                }
            }

            if ( $conf->results_sms && isset($conf->results_sms_template->id) ) {
                if ( isset($patientType) && $patientType->sms_send ) {
                    $this->templateSMS = $conf->results_sms_template;
                }
            }
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [];

        if ( $this->templateEmail )
            $channels[] = 'mail';

        //si está definida la platilla de SMS y no es un Referente (el referente no recibe SMS)
        if ( $this->templateSMS && ! $notifiable instanceof \App\Referring )
            $channels[] = MassivaMovil::class;

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail( $notifiable ) {
        if ( $notifiable instanceof \App\Referring ) {
            $template = $this->templateEmailReferer->template;
            $subject = $this->templateEmailReferer->description;
        } else {
            $template = $this->templateEmail->template;
            $subject = $this->templateEmail->description;
        }
        $template = html_entity_decode( $template );

        $procedure = $this->requestedProcedure->procedure->description;
        $template = str_replace('{{procedure}}', $procedure, $template);

        $patient_name = $this->requestedProcedure->patient->first_name . ' ' . $this->requestedProcedure->patient->last_name;
        $template = str_replace('{{patient_name}}', $patient_name, $template);
        
        $data['logo'] = $this->requestedProcedure->institution->logo_email;

        $mm = (new MailMessage)
                    ->subject( $subject )
                    ->greeting("&nbsp;")
                    ->line( $template );
        $mm->viewData = array_merge( $mm->viewData, $data );

        return $mm;
    }

    public function toMassivaMovil($notifiable) {
        $template = $this->templateSMS->template;
        $template = html_entity_decode( $template );
        $procedure = $this->requestedProcedure->procedure->description;
        $template = str_replace('{{procedure}}', $procedure, $template);

        return $template;
    }
}