<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\MassivaMovil;

class NewAppointment extends Notification implements ShouldQueue
{
    use Queueable;

    protected $appointment;
    protected $templateEmail;
    protected $templateSMS;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $appointment )
    {
        //
        $this->appointment = $appointment;
        
        $conf = \App\Configuration::remoteFind(1, [], $appointment->institution);
        
        $this->templateEmail = null;
        $this->templateSMS = null;

        if ( $conf ) {
            if ( $conf->appointment_email && isset($conf->appointment_email_template->id) ) {
                $this->templateEmail = $conf->appointment_email_template;
            }

            if ( $conf->appointment_sms && isset($conf->appointment_sms_template->id) ) {
                $this->templateSMS = $conf->appointment_sms_template;
            }
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [];

        if ( $this->templateEmail )
            $channels[] = 'mail';
        if ( $this->templateSMS )
            $channels[] = MassivaMovil::class;

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = $this->templateEmail->template;
        $template = html_entity_decode( $template );
        $template = str_replace('{{date}}', $notifiable->appointment_date_start, $template);

        $data['logo'] = $this->appointment->institution->logo_email;
        
        $mm = (new MailMessage)
                    ->subject( $this->templateEmail->description )
                    ->greeting("&nbsp;")
                    ->line( $template );
        $mm->viewData = array_merge( $mm->viewData, $data );

        return $mm;
    }

    public function toMassivaMovil($notifiable) {
        $template = $this->templateSMS->template;
        $template = html_entity_decode( $template );
        $template = str_replace('{{date}}', $notifiable->appointment_date_start, $template);

        return $template;
    }
}
