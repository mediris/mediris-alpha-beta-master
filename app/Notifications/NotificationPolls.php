<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotificationPolls extends Notification implements ShouldQueue
{
    use Queueable;

    protected $polls;
    protected $templateEmail;
    protected $templateEmailReferer;
    protected $linkPolls;
    protected $logos;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $institutionId ) {
        $inst = \App\Institution::find($institutionId)->first();
        $this->logos = $inst->logo;

        //Search system's configuration to verify if we need to send the notification    
        $conf = \App\Configuration::remoteFind(1, [], $inst);
        $this->linkPolls = $conf->url_poll;
        
        $this->templateEmail = null;
        if ( $conf ) {
            if (isset($conf->poll_email_template_id) ) {
                    $this->templateEmail = $conf->polls_email_template;
            }
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = $this->templateEmail->template;
        $template = html_entity_decode( $template );
        //$this->linkPolls = 'http://google.com';
        $template = str_replace('{{link_poll}}', $this->linkPolls, $template);

        $patient_name = $notifiable->first_name . ' ' . $notifiable->last_name;
        $template = str_replace('{{patient_name}}', $patient_name, $template);

        $data['logo'] = $this->logos;
        $mm = (new MailMessage)
                    ->subject( $this->templateEmail->description )
                    ->greeting("&nbsp;")
                    ->line( $template );
        $mm->viewData = array_merge( $mm->viewData, $data );

        return $mm;
   }
}