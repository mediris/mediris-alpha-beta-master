<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class NotificationFinalReport extends Notification implements ShouldQueue
{
    use Queueable;

    private $templateEmail;
    private $pathFile; 
    private $logoEmail;
    protected $requestedProcedure;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($pathFile, $logoEmail, $requestedProcedure)
    {
        $this->requestedProcedure = $requestedProcedure;
        $this->templateEmail = null;
        $this->pathFile = null;
        $this->logoEmail = null;

        $conf = \App\Configuration::remoteFind(1, []);        
        
        if ($pathFile != null && !empty($pathFile)) {
            $this->pathFile = $pathFile;
        }

        if ($logoEmail != null && !empty($logoEmail)) {
            $this->logoEmail = $logoEmail;
        }

        if ( ($conf) && $conf->results_email && isset($conf->results_email_template->id) ) {
            $this->templateEmail = $conf->results_email_template;
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = $this->templateEmail->template;
        $template = html_entity_decode( $template );
        
        $date = Carbon::parse( $this->requestedProcedure->technician_end_date );
        $template = str_replace('{{date}}', $date->format( 'd/m/y' ), $template);
        
        $procedure = $this->requestedProcedure->procedure->description;
        $template = str_replace('{{procedure}}', $procedure, $template);

        $patient_name = $this->requestedProcedure->patient->first_name . ' ' . $this->requestedProcedure->patient->last_name;
        $template = str_replace('{{patient_name}}', $patient_name, $template);

        $data['logo'] = $this->logoEmail;
        
        $mm = (new MailMessage)
                    ->subject( $this->templateEmail->description )
                    ->greeting("&nbsp;")
                    ->line( $template )
                    ->attach($this->pathFile);
        $mm->viewData = array_merge( $mm->viewData, $data );

        return $mm;
    }
}
