<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;
use Illuminate\Notifications\Notifiable;
use App\Notifications\NewAppointment;
use App\Helpers\Utils;

class Appointment extends RemoteModel {

    use Notifiable;

    public function __construct() {
        $this->apibase = 'api/v1/appointments';
        parent::__construct();
    }
    
    /**
     * @fecha: 21-06-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Sobreescribe la función del modelo remoto, para poder enviar la notificación si la
     *            creación de la cita ocurre exitosamente.
     */
    public static function remoteFind( $id, $data = [], $institution = null ) {
        $appointment = parent::remoteFind( $id, $data, $institution );

        if ( $appointment ) {
            $appointment->patient = Patient::find( $appointment->patient_id );
        }

        return $appointment;
    }

    /**
     * @fecha: 21-08-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Sobreescribe la función del modelo remoto, para poder cargar el objeto paciente antes de retornar la instancia.
     */
    public static function remoteCreate( array $data = [],$institution = null ) {
        $appointment = parent::remoteCreate( $data,$institution  );

        if ( $appointment ) {
            $appointment->notify( new NewAppointment( $appointment ) );
        }

        return $appointment;
    }

    /**
     * @fecha: 21-08-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Sobreescribe la función del modelo remoto, para poder traducir la descripción del status.
     */
    public static function remoteIndexData( $params, $action = null ) {
        $data = parent::remoteIndexData( $params );

        if( isset( $data->data ) ){
            foreach( $data->data as $key => $value ) {
                $value->statusDescription = ucfirst(trans('labels.' . $value->statusDescription));
            }
        }

        return $data;
    }

    /**
     * @fecha: 26-10-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Set the appointment status to no show.
     */
    public function setAsNoShow() {
        $data = $this->toArray();
        $data['appointment_status_id'] = AppointmentStatus::getValue('noshow', $this->institution);
        $data['string_start_unique'] = "NOSHOW " . rand(10, 2000000) . " " . $this->id;
        $data['string_end_unique'] = "NOSHOW " . rand(10, 2000000) . " " . $this->id;

        $model = self::remoteUpdate( $this->id, $data, $this->institution );

        return $model;
    }

    /**
     * @fecha: 23-08-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Set the appointment status to admitted.
     */
    public function setAsAdmitted() {
        $data = $this->toArray();
        $data['appointment_status_id'] = AppointmentStatus::getValue('admitted');
        $data['string_start_unique'] = "ADMITED " . rand(10, 2000000) . " " . $this->id;
        $data['string_end_unique'] = "ADMITED " . rand(10, 2000000) . " " . $this->id;

        $model = self::remoteUpdate( $this->id, $data );

        return $model;
    }

    /**
     * @fecha: 25-10-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Set the appointment status to deleted.
     */
    public function setAsDeleted( $params ) {
        $data = $this->toArray();
        $data['appointment_status_id'] = AppointmentStatus::getValue('deleted');
        
        $data['cancelation_reason'] = $params['cancelation_reason'];
        $data['cancelation_date'] = isset( $params['cancelation_date'] ) ? $params['cancelation_date'] : date('Y-m-d H:i:s');
        $data['cancelation_user_id'] = \Auth::user()->id;
        $data['string_start_unique'] = "DELETED " . rand(10, 2000000) . " " . $this->id;
        $data['string_end_unique'] = "DELETED " . rand(10, 2000000) . " " . $this->id;
        
        $model = self::remoteUpdate( $this->id, $data );

        return $model;
    }

    /**
     * @fecha: 25-10-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Set the appointment status to Rejected.
     */
    public function setAsRejected( $params ) {
        $data = $this->toArray();
        $data['appointment_status_id'] = AppointmentStatus::getValue('rejected');
        
        $data['cancelation_reason'] = $params['cancelation_reason'];
        $data['cancelation_date'] = isset( $params['cancelation_date'] ) ? $params['cancelation_date'] : date('Y-m-d H:i:s');
        $data['cancelation_user_id'] = \Auth::user()->id;
        $data['string_start_unique'] = "REJECTED " . rand(10, 2000000) . " " . $this->id;
        $data['string_end_unique'] = "REJECTED " . rand(10, 2000000) . " " . $this->id;
        
        $model = self::remoteUpdate( $this->id, $data );

        return $model;
    }

    /**
     * @fecha: 21-08-2017
     * @programador: Hendember Heras
     * @objetivo: functions required for the notifications, returns the route for the notification, in this case, the email or the phone number.
     */
    public function routeNotificationForMail() {
        return $this->patient_email;
    }
    public function routeNotificationForMassivaMovil() {
        return Utils::formatCellphoneForMassivaMovil( $this->patient_cellphone_number );
    }
}
