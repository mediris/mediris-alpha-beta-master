<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class polls extends Model{

    protected $fillable = [
        'email_to_send',
        'date_from',
        'date_to',
        'age_from',
        'age_to',
        'modalities',
        'Procedures',
        'gender'
    ];

    public function indexRemoteData( $url, $data ) {
        $client = new Client();
        $short_url = 'api/v1/polls';
        $headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];

        $finalresult=[];
        $responseTemp = $client->request('POST', $url . $short_url, [ 'headers' => $headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id, 'data' => $data ] ]);
        $response = json_decode($responseTemp->getBody());
    
        return $response;
    }

}
