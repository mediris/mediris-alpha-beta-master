<?php

namespace App\Http\Controllers;

// use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\FinalReport;
use App\Oldrecordsorden;
use App\RequestedProcedure;
// use App\ServiceRequest;
// use App\Http\Requests;
// use App\SubCategory;
// use App\Radiologist;
use App\Institution;
// use Dompdf\Dompdf;
// use App\Category;
use App\Addendum;
use App\Patient;
// use App\BiRad;
// use Activity;
// use Session;
// use Log;

use App\Notifications\NotificationFinalReport;

class FinalReportController extends Controller {

    public function __construct(){
        $this->logPath = '/logs/finalreport/finalreport.log';
    }

    public function pdf(Request $request, $id, $pdfversion=null) {
        try {
            $requestedProcedure = RequestedProcedure::remoteFind( $id );
            $file = $this->createFile($requestedProcedure, $pdfversion);

            return response($file, 200)->header('Content-Type', 'application/pdf');
        } catch(\Exception $e) {
            return $this->logError( $e, "finalreport", "pdf");
        }

    }

    public function createFile($requestedProcedure, $pdfversion, $isMail = false) {

        if ( $pdfversion == 'original' ) {
            $fr = FinalReport::createReport( $requestedProcedure->formatReportData(), false );
        } else if ( $pdfversion == 'addendum' ) {
            $fr = FinalReport::createAddendum( $requestedProcedure->formatReportData( true ), false );
        } elseif($pdfversion == 'original-draf'){
            $fr = FinalReport::createReport( $requestedProcedure->formatReportData(), true );
        }

        $fr->toStorage();

        if (!$isMail) {
            $file = $fr->getStream();
            return $file;
        } else {
            return $fr;
        }
    }

    public function legacy(Request $request, Oldrecordsorden $orden) {
        $stream = FinalReport::createReport( $orden->formatReportData() )->getStream();

        return response($stream, 200)->header('Content-Type', 'application/pdf');
    }

    public function email(Request $request, $id, $patientId) {
        //path del logo original, como está en BD (sin host)
        $logoEmail = \Session::get('institution')->getOriginal('logo_email');
        
        $patient = Patient::find($patientId);
        
        $conf = \App\Configuration::remoteFind(1, []);
        
        if (empty($logoEmail)) {
            $request->session()->flash('message', trans('alerts.no-logo-email'));
            $request->session()->flash('class', 'alert alert-danger');
        } elseif(empty($patient->email)) {
            $request->session()->flash('message', trans('alerts.no-email'));  
            $request->session()->flash('class', 'alert alert-danger');
        } elseif( !$conf->results_email ) {
            $request->session()->flash('message', trans('alerts.no-result-email'));  
            $request->session()->flash('class', 'alert alert-danger');
        } else {
            $requestedProcedure = RequestedProcedure::remoteFind( $id );

            // Se verifica si el RequestedProcecure posee addendums asociados
            $pdfversion = !empty($requestedProcedure->addendums) ? "addendum" : "original";
            $fr = $this->createFile($requestedProcedure, $pdfversion, true);

            $pathBase = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $pathFile = $pathBase . $fr->pathFilename;

            //url del logo completo, incluyendo el host y el path
            $logoEmail = \Session::get('institution')->logo_email;

            // se agrega el correo a la cola de notificaciones de Laravel
            $patient->notify( new NotificationFinalReport($pathFile, $logoEmail, $requestedProcedure) );

            $request->session()->flash('message', trans('alerts.email-sent'));  
            $request->session()->flash('class', 'alert alert-success');    
        }
            
        return redirect()->back();
    }
}