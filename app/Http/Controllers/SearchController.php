<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Session;
use Illuminate\Support\MessageBag;
use Log;
use Activity;
use Auth;
use App\Search;
use App\Patient;
use App\RequestedProcedure;
use App\PatientType;
use App\Source;
use App\Referring;
use App\Answer;
use App\Modality2;
use App\Procedure;
use App\RequestedProcedureStatus;
use App\Category;
use App\SubCategory;
use App\BiRad;
use App\ServiceRequest;
use App\SuspendReason;

class SearchController extends Controller {

    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';
    }

    public function index( Request $request, $print = NULL){
        try{
            if( $request->isMethod('POST') ){

                if( $print == 1 ){

                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                    echo $data;

                }else{

                    $params = $request->all();

                    $replaceFields  = array(
                        'orderStatus',
                        'patientSex'
                    );
                    if( isset($params) ){
                        if( isset($params['filter']) ){
                            if( isset($params['filter']['filters']) ){
                                $rtrans = new \App\ReverseTranslator();
                                foreach ($params['filter']['filters'] as $index => $filter) {
                                    if( in_array($filter['field'], $replaceFields) && $filter['operator'] == 'eq' ){
                                        $params['filter']['filters'][$index]['value'] = $rtrans->getKey($filter['value']);
                                    }
                                }
                            }
                        }
                    }

                    $data   = Search::remoteIndexData( $params );

                    echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }

            $categories     = Category::remoteFindAll(['active' => 1]);
            $subcategories  = SubCategory::remoteFindAll(['active' => 1]);
            $birads         = BiRad::remoteFindAll();

            return view('search.index', compact(
                'search',
                'categories',
                'subcategories',
                'birads'
            ));

        } catch( \Exception $e ) {
            return $this->logError( $e, "search", "index");
        }
    }


    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [ 
                
            ]);
            try{
                $data = $request->all();
                
                $req = RequestedProcedure::remoteUpdate( $id, $data['requestedProcedure'] );
                
                if ( isset( $req->error ) ) {

                    return $this->parseFormErrors( $data, $req );

                } else {
                    $patient = Patient::find($data['serviceRequest']['patient_id']);
                    $dataPatient = $patient->getAttributes();
                    unset($dataPatient['photo']);
                    $dataPatient['patientDocumentsToDelete'] = $request->patientDocumentsToDelete;
                    $dataPatient['patientDocuments']         = $request->patientDocuments;
                    //save the patient data
                    $patient = Patient::updateOrCreate( $dataPatient );
                    //save the request data
                    $data['serviceRequest']['requestDocuments'] = $request->requestDocuments;
                    $data['serviceRequest']['requestDocumentsToDelete'] = $request->requestDocumentsToDelete;
                    $res = ServiceRequest::remoteUpdate( $req->service_request_id, $data['serviceRequest'] );
                    
                    if ( isset( $res->error ) ) {

                        return $this->parseFormErrors( $data, $res );

                    } else {
                        $request->session()->flash('message', trans('alerts.success-edit'));
                        $request->session()->flash('class', 'alert alert-success');

                        return redirect()->route('search');
                    }
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "search", "edit");
            }
        }
        
        try {
            $requestedProcedure = RequestedProcedure::remoteFind( $id );
            if ( $requestedProcedure->suspension_reason_id ) {
                $requestedProcedure->suspension_reason = SuspendReason::remoteFind( $requestedProcedure->suspension_reason_id );
            }
            $patient = $requestedProcedure->patient;
            $serviceRequest = $requestedProcedure->service_request;
            $patientTypes = PatientType::remoteFindAll();
            $sources = Source::remoteFindAll(['active' => 1]);
            $referrings = Referring::remoteFindAll(['active' => 1]);
            $answers = Answer::remoteFindAll(['active' => 1]);
            $modalities = Modality2::remoteFindAll(['active' => 1]);
            $procedures = Procedure::remoteFindAll(['active' => 1]);
            $categories = Category::remoteFindAll(['active' => 1]);
            $subcategories = SubCategory::remoteFindAll(['active' => 1]);
           
            $requestedProcedureStatus = new RequestedProcedureStatus();
            $requestedProcedureStatuses = $requestedProcedureStatus->getAll($request->session()->get('institution')->url);

            return view('search.edit', compact( 'requestedProcedure',
                                                'patient',
                                                'serviceRequest',
                                                'patientTypes',
                                                'sources',
                                                'referrings',
                                                'answers',
                                                'requestedProcedureStatuses',
                                                'modalities',
                                                'procedures',
                                                'categories',
                                                'subcategories'
                                            ));
        } catch( \Exception $e ) {
            return $this->logError( $e, "search", "edit");
        }
    }
}
