<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\PatientType;
use App\Icon;

class PatientTypesController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';

        $this->url = 'api/v1/patienttypes';
        $this->patientType = new PatientType();
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Patient Types.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){

        try{
            $patientTypes = PatientType::remoteFindAll();

            return view('patientTypes.index', compact('patientTypes'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: patientTypes. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'administrative_ID' => 'required|max:2|unique_test:patient_types',
                'priority' => 'required|numeric|min:1',
                'icon' => 'required',
                'parent_id' => 'required',
            ]);
            try{
                $data = $request->all();
                
                $res = PatientType::remoteCreate($data);

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {   
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');
                
                    return redirect()->route('patientTypes');
                }
            }catch( \Exception $e ) {
                return $this->logError( $e, "patientTypes", "add");
            }
        }

        try{
            $roots = $this->patientType->getAllRoots($request->session()->get('institution')->url);
            $patientTypes = PatientType::remoteFindAll();
            $icons = Icon::all();

            return view('patientTypes.add', [ 'roots' => $roots, 'icons' => $icons, 'patientTypes' => $patientTypes ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "patientTypes", "add");
        }

    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
                'administrative_ID' => 'required|max:2|unique_test:patient_types,' . $id,
                'priority' => 'required|numeric|min:1',
                'icon' => 'required',
                'parent_id' => 'required',
            ]);
            try{
                $data = $request->all();
                
                $res = PatientType::remoteUpdate($id, $data);                

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');
                
                    return redirect()->route('patientTypes');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "patientTypes", "edit");
            }
        }

        try{
            $patientType = PatientType::remoteFind($id);
            $roots = $this->patientType->getAllRoots($request->session()->get('institution')->url);
            $patientTypes = PatientType::remoteFindAll();
            $icons = Icon::all();

            return view('patientTypes.edit', [ 'patientType' => $patientType, 'roots' => $roots, 'icons' => $icons, 'patientTypes' => $patientTypes ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "patientTypes", "edit");
        }
    }

    public function delete( Request $request, $id ){
        try{
            $res = $this->patientType->remove($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete-api', [ 'section' => 'patientTypes', 'id' => $id, 'id-institution' => $request->session()->get('institution')->id ]));

                $request->session()->flash('message', trans('alerts.success-delete'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('patientTypes');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: patientTypes. Action: delete');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = $this->patientType->active($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit-api', [ 'section' => 'patientTypes', 'id' => json_decode($res->getBody())->oldValue->id, 'id-institution' => $request->session()->get('institution')->id, 'oldValue' => json_encode(json_decode($res->getBody())->oldValue), 'newValue' => json_encode(json_decode($res->getBody())->newValue) ]));

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('patientTypes');
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: patientTypes. Action: active');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }
}
