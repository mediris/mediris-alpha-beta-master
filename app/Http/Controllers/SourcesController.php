<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Source;

class SourcesController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
    }


    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Sources.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try {
            $sources = Source::remoteFindAll();

            return view('sources.index', compact('sources'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "Sources", "index" );
        }
    }

    public function add( Request $request ) {
        if( $request->isMethod('post') ) {
            $requestObj = new \App\Http\Requests\SourceRequest();
            $this->validate($request, $requestObj->rules());

            try {
                $data = $request->all();
                
                $res = Source::remoteCreate($data);

                if( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');
            
                return redirect()->route('sources');
            } catch( \Exception $e ) {
                return $this->logError( $e, "Sources", "add" );
            }
        }

        try {
            return view('sources.add');
        } catch( \Exception $e ) {
            return $this->logError( $e, "Sources", "add" );
        }
    }

    public function edit( Request $request, $id ) {
        if( $request->isMethod('post') ) {
            $requestObj = new \App\Http\Requests\SourceRequest();
            $this->validate($request, $requestObj->rules());

            try {
                $data = $request->all();
                
                $res = Source::remoteUpdate( $id, $data );

                if( isset( $res->error ) ){
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('sources');
            } catch( \Exception $e ) {
                return $this->logError( $e, "Sources", "edit" );
            }
        }

        try{
            $source = Source::remoteFind($id);

            return view('sources.edit', compact('source'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "Sources", "edit" );
        }
    }

    public function delete( Request $request, $id ){
        try{
            $res = $this->source->remove($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete-api', [ 'section' => 'sources', 'id' => $id, 'id-institution' => $request->session()->get('institution')->id ]));

                $request->session()->flash('message', trans('alerts.success-delete'));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('sources');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: sources. Action: delete');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, $id ){
        try {
            $res = Source::remoteToggleActive($id);

            if( isset( $res->error ) ){
                return $this->parseFormErrors( $data, $res );
            }

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');
       
            return redirect()->route('sources');
        } catch( \Exception $e ) {
            return $this->logError( $e, "Sources", "active" );
        }
    }
}
