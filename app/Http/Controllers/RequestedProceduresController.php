<?php

namespace App\Http\Controllers;

use App\Modality;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\RequestedProcedure;
use App\Procedure;
use Log;

class RequestedProceduresController extends Controller {
    public function form( Request $request, $position, $procedureId, $requestedProcedureId = null ){
        //try{
            $procedure = Procedure::remoteFind($procedureId);
            
            if( $requestedProcedureId == null ){
                return view('requestedProcedures.form', [ 'position' => $position, 'procedure' => $procedure ]);
            }else{
                $requestedProcedure = $this->requestedProcedure->get($request->session()->get('institution')->url, $requestedProcedureId);

                return view('requestedProcedures.form', [ 'position' => $position, 'procedure' => $procedure, 'requestedProcedure' => $requestedProcedure ]);
            }
        // }catch( \Exception $e ){
        //     Log::useFiles(storage_path() . '/logs/requestedProcedures/requestedProcedures.log');
        //     Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: requestedProcedures. Action: form');

        //     return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        // }
    }
}
