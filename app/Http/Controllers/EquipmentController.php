<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Equipment;

class EquipmentController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
    }

    /**
     * @fecha 25-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Equipment.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try {
            $equipment = Equipment::remoteFindAll();

            return view('equipment.index', compact('equipment'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "equipment", "index");
        }
    }

    public function add( Request $request ) {
        if( $request->isMethod('post') ) {
            $this->validate($request, [
                'ae_title' => 'required|max:50|unique_test:equipment',
                'name' => 'required|max:50',
            ]);

            try {
                $data = $request->all();
                
                $res = Equipment::remoteCreate( $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('equipment');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "equipment", "add");
            }
        }

        try {
            return view('equipment.add');
        } catch( \Exception $e ) {
            return $this->logError( $e, "equipment", "add");
        }

    }

    public function edit( Request $request, $id ){
        if( $request->isMethod('post') ) {
            $this->validate($request, [
                'ae_title' => 'required|max:50|unique_test:equipment,' . $id,
                'name' => 'required|max:50',
            ]);

            try {
                $data = $request->all();
                
                $res = Equipment::remoteUpdate( $id, $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('equipment');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "equipment", "edit");
            }
        }

        try {
            $equipment  = Equipment::remoteFind( $id );

            return view('equipment.edit', compact('equipment') );
        } catch( \Exception $e ) {
            return $this->logError( $e, "equipment", "edit");
        }
    }

    public function delete( Request $request, $id ){
        try{
            $res = $this->equipment->remove($request->session()->get('institution')->url, $id);

            if( $res->getStatusCode() == 200 ){
                if( isset( json_decode($res->getBody())->error ) ){
                    $mb = new MessageBag();

                    $mb->add(json_decode($res->getBody())->error, trans('alerts.' . json_decode($res->getBody())->error));

                    return redirect()->back()->withErrors($mb)->withinput();
                }

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete-api', [ 'section' => 'equipment', 'id' => $id, 'id-institution' => $request->session()->get('institution')->id ]));

                $request->session()->flash('message', trans('alerts.success-delete', [ 'name' => trans('messages.equipment') ]));
                $request->session()->flash('class', 'alert alert-success');
            }

            return redirect()->route('equipment');

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: equipment. Action: delete');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = Equipment::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('equipment');
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "equipment", "active");
        }
    }
}
