<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\SubCategory;

class SubCategoriesController extends Controller {
    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección Sub Categories.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ){
        try{
            $subcategories = SubCategory::remoteFindAll();

            return view('subcategories.index', compact('subcategories'));
        }catch( \Exception $e ){
            return $this->logError( $e, "subcategories", "index");
        }
    }

    public function add( Request $request ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'required|max:45',
            ]);
            try{
                $data = $request->all();
                $data['language'] = \App::getLocale();

                $res = SubCategory::remoteCreate( $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('subcategories');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "subcategories", "add");
            }
        }

        try{
            return view('subcategories.add');
        }catch( \Exception $e ){
            return $this->logError( $e, "subcategories", "add");
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [
                'description' => 'required|max:45',
            ]);
            try{
                $data = $request->all();
                $data['language'] = \App::getLocale();

                $res = SubCategory::remoteUpdate( $id, $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('subcategories');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "subcategories", "edit");
            }
        }

        try {
            $subcategory = SubCategory::remoteFind( $id );

            return view('subcategories.edit', compact('subcategory'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "subcategories", "edit");
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = SubCategory::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('subcategories');
            }
        }catch( \Exception $e ){
            return $this->logError( $e, "subcategories", "active");
        }
    }
}
