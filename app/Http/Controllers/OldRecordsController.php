<?php

namespace App\Http\Controllers;

use App\orarchivoescaneado;
use App\Oldrecordsdatabase;
use Dompdf\Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DB;
use App\PatientDocuments;
use App\Patient;
use App\Oldrecordsinstitucion;
use App\Oldrecordsestatus;
use App\Oldrecordsmodalidad;
use App\Oldrecordsprocedimiento;
use App\Oldrecordsusuario;
use App\Oldrecordspaciente;
use App\Oldrecordssolicitud;
use App\Oldrecordsorden;
use App\Oldrecordsarchivosescaneados;
use Illuminate\Queue\TimeoutException;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use MongoDB\Driver\Exception\ExecutionTimeoutException;
use Monolog\Handler\ElasticSearchHandler;
use phpDocumentor\Reflection\Types\Integer;
use phpDocumentor\Reflection\Types\Resource;
use PhpParser\Node\Stmt\TryCatch;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Kendo;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Response;
use Log;
use App;
use Illuminate\Contracts\View\View;
use App\Room;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\VarDumper\Cloner\Data;
use App\Appointment;

class OldRecordsController extends Controller
{
    private $fileIn='storage/app/olddata/imports/ordenes.csv';
    private $newConnection = null;

    public function __construct(){
       // $this->middleware('auth');
        //$this->serviceRequest = new ServiceRequest();
        //$this->requestedProcedure = new RequestedProcedure();
    }

    public function import(Request $request)
    {
        $res=null;
        $institucion=null;
        $db=Oldrecordsdatabase::first();

        try{
            if($db->host!=null and $db->puerto!=null and $db->base!=null and $db->usuario!=null and $db->clave!=null)
            {
                $res=  $this->changeConnection($db)->select('select nombre from institucion');
                $institucion=Oldrecordsinstitucion::where('nombre', $res[0]->nombre)->first();
            }
        }
        catch (\Exception $e)
        {
            Log::useFiles(storage_path() . '/app/olddata/logs/log.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: patients. Action: all');
        }

        try{
            $olddata=Oldrecordsdatabase::first();
            $id_inst= ($institucion==null)? -1:$institucion->id;
            //$tabla= (Oldrecordsestatus::where('id_institucion','=',$id_inst)->first()==null) ? false:true;
            //Se verifica que la tabla anterior contenga data de la institucino actual
            $migrar=array();
            $fileLogs=array();
            $filtemp=array();
            $fileLogs=[ucfirst(trans('labels.institution'))=>$this->searchFile(ucfirst(trans('labels.institution')))
                ,ucfirst(trans('labels.status'))=>$this->searchFile(ucfirst(trans('labels.status')))
                ,ucfirst(trans('labels.modality'))=>$this->searchFile(ucfirst(trans('labels.modality')))
                ,ucfirst(trans('labels.procedure'))=>$this->searchFile(ucfirst(trans('labels.procedure')))
                ,ucfirst(trans('labels.user'))=>$this->searchFile(ucfirst(trans('labels.user')))
                ,ucfirst(trans('titles.patient'))=>$this->searchFile(ucfirst(trans('titles.patient')))
                ,ucfirst(trans('titles.requests'))=>$this->searchFile(ucfirst(trans('titles.requests')))
                ,ucfirst(trans('titles.order'))=>$this->searchFile(ucfirst(trans('titles.order')))
                ,ucfirst(trans('titles.scan-files'))=>$this->searchFile(ucfirst(trans('titles.scan-files')))
            ];




            /*$fileLogs=$this->searchLog([ucfirst(trans('labels.status')),ucfirst(trans('labels.institution')),ucfirst(trans('labels.modality'))
            ,ucfirst(trans('labels.procedure')),ucfirst(trans('labels.user')),ucfirst(trans('titles.patient')),ucfirst(trans('titles.requests'))
            ,ucfirst(trans('titles.order')),ucfirst(trans('titles.scan-files'))]);*/


            $migrar=[
                'status'=>($id_inst==-1) ? true:false
                ,'modalidades'=>(Oldrecordsestatus::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                ,'procedimientos'=>(Oldrecordsmodalidad::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                ,'usuarios'=>(Oldrecordsprocedimiento::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                ,'pacientes'=>(Oldrecordsusuario::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                ,'solicitudes'=>(Oldrecordspaciente::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                ,'ordenes'=>(Oldrecordssolicitud::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                ,'escaneados'=>(Oldrecordsorden::where('id_institucion','=',$id_inst)->first()==null) ? true:false
                //,'escaneados'=>(Oldrecordsarchivosescaneados::where('id_institucion','=',$institucion->id)->first()==null) ? false:true

            ];
            $vars=[ 'estatus'=>Oldrecordsestatus::count(),'modalidades'=>Oldrecordsmodalidad::count()
                ,'procedimientos'=>Oldrecordsprocedimiento::count(),'usuarios'=>Oldrecordsusuario::count()
                ,'pacientes'=>Oldrecordspaciente::count(),'solicitudes'=>Oldrecordssolicitud::count()
                ,'ordenes'=>Oldrecordsorden::count()
                ,'escaneados'=>Oldrecordsarchivosescaneados::count(),'institucion'=>Oldrecordsinstitucion::count()
                ,'id_centro'=>($institucion ==null) ?  null: $institucion->id
                ,'host'=>($olddata==null) ? null : $olddata->host
                ,'puerto'=>($olddata==null) ? null : $olddata->puerto
                ,'base'=>($olddata==null) ? null : $olddata->base
                ,'usuario'=>($olddata==null) ? null : $olddata->usuario
                ,'clave'=>($olddata==null) ? null : $olddata->clave
                ,'test'=>0
                ,'centro_a_migrar'=>($res==null)? null :  $res[0]->nombre
                ,'tablas'=>$migrar
                ,'logs'=>$fileLogs
            ];


            return view('oldrecords.index',compact('vars'));
        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/app/olddata/logs/log.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: patients. Action: all');
            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function importData(Request $request)
    {
        try{
            $this->setLog(ucfirst(trans('labels.database')));
            $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.data-conn')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')) );
            $inserta= Oldrecordsdatabase::first();
            if ($inserta<>null) {
                $inserta->host=$request->host;
                $inserta->puerto=$request->puerto;
                $inserta->base=$request->base;
                $inserta->usuario=$request->usuario;
                $inserta->clave=encrypt($request->clave);
                $inserta->save();
            }
            else{
                $inserta=new Oldrecordsdatabase;
                $inserta->host=$request->host;
                $inserta->puerto=$request->puerto;
                $inserta->base=$request->base;
                $inserta->usuario=$request->usuario;
                $inserta->clave=encrypt($request->clave);
                $inserta->save();
            }
            $this->AlertSuccess($request);
            $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.data-conn')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.start')) ,ucfirst(trans('alerts.success-add')));
            // $this->writeLog(ucfirst(trans('labels.database')),'Importar',null,null,false,null);
        }catch(\Exception $e){
            $this->AlertDanger($request);
            $this->writeLog(ucfirst(trans('labels.database')),'Guardar',null,null,false,$e);

        }
        catch (QueryException $e)
        {
            $request->session()->flash('message', trans('alerts.error-add'));
            $request->session()->flash('class', 'alert alert-danger');
        }
        catch (ExecutionTimeoutException $e)
        {
            $request->session()->flash('message', trans('alerts.error-add'));
            $request->session()->flash('class', 'alert alert-danger');
        }
        return back();
    }

    public function importInstitucion(Request $request)
    {
        try{
            $this->setLog(ucfirst(trans('labels.institution')));
            $valid_db= $this->verify_connection();
            if($valid_db){
                $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.institution')),ucfirst(trans('label.start')) . ' ' .ucfirst(trans('label.start')) ,'');
                $db=Oldrecordsdatabase::first();
                //DB::getPdo()->beginTransaction();
                $rows=$this->changeConnection($db)->select('select nombre from institucion');

                /*
                                 foreach($rows as $row)
                                 {
                                     $inserta= New Oldrecordsinstitucion();
                                     $inserta->nombre=$row->nombre;
                                     $inserta->save();
                                     $this->writeLog(ucfirst(trans('labels.institution')),ucfirst(trans('labels.migrate')),null,null,true,null);
                                 }
                                 DB::getPdo()->commit();

                                 $this->AlertSuccess($request);

                                 //$this->writeLog(ucfirst(trans('labels.institution')),ucfirst(trans('labels.migrate')),null,null,false,null);


                            */
                $response = new StreamedResponse();
                $response->setCallback(function () use($rows,$request) {
                    $view =view('oldrecords.progress');
                    echo $view;
                    $count=0;
                    try{
                        DB::getPdo()->beginTransaction();
                        foreach ($rows as $row)
                        {
                            $inserta= New Oldrecordsinstitucion();
                            $inserta->nombre=$row->nombre;
                            $inserta->save();
                            $count=$count+1;
                            $this->writeLog(ucfirst(trans('labels.institution')),ucfirst(trans('labels.migrate')),null,null,true,null);
                            echo  '<script> pb.value('. intval($count * 100 /count($rows)).') </script>'."\n";
                            ob_flush();
                            flush();
                        }
                        DB::getPdo()->commit();
                    }
                    catch(\Exception $e){
                        DB::getPdo()->rollBack();
                        $this->AlertDanger($request);
                        $this->writeLog(ucfirst(trans('labels.institution')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                        echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                    }
                });
                $this->AlertSuccess($request);
                $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.institution')),ucfirst(trans('label.end')) . ' ' .ucfirst(trans('label.end')) ,ucfirst(trans('alerts.success-add')));
                return $response;
            }
            else{
                $this->AlertInvalidConn($request);
                return back();
            }
        }catch(\Exception $e){
            //DB::getPdo()->rollBack();
            $this->AlertDanger($request);
            $this->writeLog(ucfirst(trans('labels.institution')),ucfirst(trans('labels.migrate')),null,null,false,$e);
            //$this->writeLog('Institucion','Migrar',null,null,false,$e);
        }
        return back();
    }

    public function importEstatus(Request $request)
    {
        $ct=0;
        try{
            $this->setLog(ucfirst(trans('labels.status')));
            $valid_db= $this->verify_connection();
            if($valid_db){

                $db=Oldrecordsdatabase::first();
                $c1=$rows=$this->changeConnection($db)->select('select count(*) as count from estatus');
                $estatus=$this->changeConnection($db)->select('select id,descripcion from estatus');
                //DB::getPdo()->beginTransaction();
                /*
                foreach($estatus as $esta)
                {
                    $EstatusVar= New Oldrecordsestatus;
                    $EstatusVar->id=$esta->id;
                    $EstatusVar->descripcion=$esta->descripcion;
                    $EstatusVar->id_institucion= $request->route('centro');
                    $EstatusVar->save();
                    $ct=$ct+1;
                    //$this->writeLog(null,'Estatus','Migrations',DB::getPdo()->lastInsertId ([ 'id' ] ) ,'Transaccion '.$EstatusVar,'Migrations');
                }
                DB::getPdo()->commit();
                $this->AlertSuccess($request);
                */

                $response = new StreamedResponse();
                $response->setCallback(function () use($estatus,$request) {
                    $view =view('oldrecords.progress');
                    echo $view;
                    $count=0;
                    try{
                        DB::getPdo()->beginTransaction();
                        $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.status')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')) );
                        foreach ($estatus as $esta)
                        {
                            $EstatusVar= New Oldrecordsestatus;
                            $EstatusVar->id=$esta->id;
                            $EstatusVar->descripcion=$esta->descripcion;
                            $EstatusVar->id_institucion= $request->route('centro');
                            $EstatusVar->save();
                            $this->writeLog(ucfirst(trans('labels.status')),ucfirst(trans('labels.migrate')),null,null,true,null);
                            $count=$count+1;
                            echo  '<script> pb.value('. intval($count * 100 /count($estatus)).') </script>'."\n";
                            ob_flush();
                            flush();
                        }
                        DB::getPdo()->commit();
                    }
                    catch(\Exception $e){
                        DB::getPdo()->rollBack();
                        $this->AlertDanger($request);
                        echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                    }
                });
                $this->AlertSuccess($request);
                return $response;
            }
            else{
                $this->AlertInvalidConn($request);
                return back();
            }
        }catch(\Exception $e){
            //DB::getPdo()->rollBack();
            $this->writeLog(ucfirst(trans('labels.status')),ucfirst(trans('labels.migrate')),null,null,false,$e);
            $this->AlertDanger($request);
        }
        return back();
    }

    public function importModalidad(Request $request)
    {
        try{
            $this->setLog(ucfirst(trans('labels.modality')));
            //DB::getPdo()->beginTransaction();
            $db=Oldrecordsdatabase::first();
            $modalidades=$this->changeConnection($db)->select('select id,descripcion,etiqueta from modalidad');
            $c1= count($modalidades);
            $ct=0;

            /*
            foreach ($modalidades as $modalidad)
            {
                $model=new Oldrecordsmodalidad;
                $model->id= $modalidad->id;
                $model->descripcion=$modalidad->descripcion;
                $model->etiqueta=$modalidad->etiqueta;
                $model->id_institucion=$request->route('centro') ;
                $model->save();
                $ct=$ct+1;
            }
            DB::getPdo()->commit();
            $this->AlertSuccess($request);
            */

            $response = new StreamedResponse();
            $response->setCallback(function () use($modalidades,$request) {
                $view =view('oldrecords.progress');
                echo $view;
                $count=0;

                try{
                    DB::getPdo()->beginTransaction();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.modality')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')) );
                    foreach ($modalidades as $modalidad)
                    {
                        $model=new Oldrecordsmodalidad;
                        $model->id= $modalidad->id;
                        $model->descripcion=$modalidad->descripcion;
                        $model->etiqueta=$modalidad->etiqueta;
                        $model->id_institucion=$request->route('centro') ;
                        $model->save();;
                        $count=$count+1;
                        $this->writeLog(ucfirst(trans('labels.modality')),ucfirst(trans('labels.migrate')),null,null,true,null);
                        echo  '<script> pb.value('. intval($count * 100 /count($modalidades)).') </script>'."\n";
                        ob_flush();
                        flush();
                    }
                    DB::getPdo()->commit();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.modality')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    DB::getPdo()->rollBack();
                    $this->AlertDanger($request);
                    $this->writeLog(ucfirst(trans('labels.modality')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                }
            });
            $this->AlertSuccess($request);
            return $response;

        }catch(\Exception $e){
            //DB::getPdo()->rollBack();
            $this->AlertDanger($request);
        }
        return back();
    }

    public function importProcedimiento(Request $request)
    {
        try{
            $this->setLog(ucfirst(trans('labels.procedure')));
            $response = new StreamedResponse();
            //$this->setLog('Procedimiento');
            $db=Oldrecordsdatabase::first();
            //$c1= count($result);
            $ct=0;
            $traArray=array();
            $result=$this->changeConnection($db)->select('select id,descripcion,idadministrativo,id_modalidad from procedimiento');
            $response->setCallback(function () use($result,$request) {
                $view =view('oldrecords.progress');
                echo $view;
                $count=0;
                try{

                    DB::getPdo()->beginTransaction();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.procedure')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')) );
                    foreach ($result as $insert)
                    {
                        $model=new Oldrecordsprocedimiento;
                        $model->id= $insert->id;
                        $model->descripcion=$insert->descripcion;
                        $model->idadministrativo=$insert->idadministrativo;
                        $model->id_modalidad= $insert->id_modalidad;
                        $model->id_institucion=$request->route('centro') ;
                        $model->save();
                        $count=$count+1;
                        $this->writeLog(ucfirst(trans('labels.procedure')),'Migrar',null,null,true,null);
                        echo  '<script> pb.value('. intval($count * 100 /count($result)).') </script>'."\n";
                        ob_flush();
                        flush();
                    }
                    DB::getPdo()->commit();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.procedure')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    DB::getPdo()->rollBack();
                    $this->AlertDanger($request);
                    $this->writeLog(ucfirst(trans('labels.procedure')),'Migrar',null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                }
            });
            $this->AlertSuccess($request);
            return $response;
            /*

            if($result!=null) {
                foreach ($result as $item) {
                    $log = Oldrecordsprocedimiento::where('id', '=', $item->id)->where('id_institucion', '=', $request->route('centro'))->get();
                    //$text= ($log!=null)? 'Tabla: '. 1 .' /// ID: ' . $log[0]->id .' [Creado: '. $log[0]->created_at->toDateTimeString(). ']' :'NO' ;
                    //Log::useFiles(storage_path() . '/app/olddata/logs/'. 'log' .'.log');
                    $this->writeLog('Procedimiento', 'Migrar', $log[0]->id, $log[0]->created_at->toDateTimeString(), false, null);
                }
            }
            */

        }catch(\Exception $e){
            $this->AlertDanger($request);
            $this->writeLog(ucfirst(trans('labels.procedure')),'Migrar',null,null,false,$e);
        }
       return back();
        /*
                $response = new StreamedResponse(function() use ($request){

                    $result=DB::connection('mysql_or')->select('select id,descripcion,idadministrativo,id_modalidad from procedimiento');


                    $c1= count($result);
                    $ct=0;


        echo ' <div id="outer" style="width: 500px; background-color: #761ba0"><div id="inner" style="width: 250px; background-color:#f4ff4c">t</div></div>
        <script>
        function updateProgress( val ) {
             inner = document.getElementById("inner");
             inner.style.width = val*100 + "%" ;
        }
        </script>';

                    foreach ($result as $insert)
                    {
                        $model=new orprocedimiento;
                        $model->id=$request->route('centro') . $insert->id;
                        $model->descripcion=$insert->descripcion;
                        $model->idadministrativo=$insert->idadministrativo;
                        $model->id_modalidad=$request->route('centro') . $insert->id_modalidad;
                        $model->id_institucion=$request->route('centro') ;
                        $model->save();
                        $ct=$ct+1;
                        echo '<script>updateProgress('. $ct/$c1 .')</script>';
                        ob_flush();
                        flush();
                    }
                });


                return back();

                */
    }

    public function importUsuario(Request $request)
    {

        try{
            $db = Oldrecordsdatabase::first();
            
            $result = $this->changeConnection($db)
                           ->select(
                'select id, nombre, apellido, email, cedula, telefono, celular, idadministrativo, firma
                from usuario'
            );

            $c1= count($result);
            $ct=0;
            
            $response = new StreamedResponse();
            $response->setCallback( function () use( $result, $request ) {
                
                $view =view('oldrecords.progress');
                echo $view;
                $count=0;
                $signature_path = config('constants.paths.user_signature');
                $centro = $request->route()->getParameter('centro');
                

                try {
                    $this->setLog(ucfirst(trans('labels.user')));
                    
                    DB::getPdo()->beginTransaction();
                    
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.user')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')));
                    
                    foreach ($result as $insert) {
                        $model = new Oldrecordsusuario;
                        $model->id =                $insert->id;
                        $model->nombre =            $insert->nombre;
                        $model->apellido =          $insert->apellido;
                        $model->email =             $insert->email;
                        $model->cedula =            $insert->cedula;
                        $model->telefono =          $insert->telefono;
                        $model->celular =           $insert->celular;
                        $model->idadministrativo =  $insert->idadministrativo;
                        $model->id_institucion =    $request->route('centro');

                        if ( $insert->firma ) {
                            $filename =  'oldrecords_signature_' . $insert->id . '_' . $centro . '.jpg';

                            $imagen = imagecreatefromstring($insert->firma);
                            ob_start();
                                imagejpeg($imagen, null, 100);
                                $imagenJpg = ob_get_contents();
                            ob_end_clean();
                            imagedestroy($imagen);

                            Storage::put( $signature_path . '/' . $filename, $imagenJpg);

                            $model->firma = $signature_path . '/' . $filename;
                        }

                        $model->save();
                        
                        $count++;
                        $this->writeLog(ucfirst(trans('labels.user')),ucfirst(trans('labels.migrate')),$insert->id,null,true,null);
                        echo  '<script> pb.value('. intval($count * 100 /count($insert)).') </script>'."\n";
                        ob_flush();
                        flush();
                    }
                    DB::getPdo()->commit();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.user')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    DB::getPdo()->rollBack();
                    $this->AlertDanger($request);
                    $this->writeLog(ucfirst(trans('labels.user')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                }
            });
            $this->AlertSuccess($request);
            return $response;

        }catch(\Exception $e){
            //DB::getPdo()->rollBack();
            $this->AlertDanger($request);
        }
      return back();

    }

    public function importPatients(Request $request)
    {
        //try {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 0);

            $this->setLog(ucfirst(trans('titles.patient')));

            $db=Oldrecordsdatabase::first();
            $id = $this->readcsv($this->fileIn);
            $count = $this->changeConnection($db)->Select('select count(*) as count 
                     from paciente 
                     where id in(
			                select distinct(id_paciente) 
			                from solicitud where id in(
							                			select distinct(id_solicitud) 
                                                        from orden 
                                                        where id in (
													                  ' . implode(',', $id) . '
                                                                    )
									                  ) 
                            ) ');
            $repeat = (Integer)($count[0]->count / 200);
            $arr = array();
            $registros = 0;
            $pacientes = $this->changeConnection($db)->Select(
                'select * 
                     from paciente 
                     where id in(
			                select distinct(id_paciente) 
			                from solicitud where id in(
							                			select distinct(id_solicitud) 
                                                        from orden 
                                                        where id in (
													                  ' . implode(',', $id) . '
                                                                    )
									                  )
                            )'
            );

            $response = new StreamedResponse();
            $response->setCallback(function () use($repeat,$id,$request,$pacientes) {
                $view =view('oldrecords.progress');
                echo $view;
                $count=0;


                $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.user')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')));
                DB::getPdo()->beginTransaction();
                try{

                        foreach ($pacientes as $paciente) {
                            $PacienteVar = New Oldrecordspaciente();
                            $PacienteVar->id = $paciente->id;
                            $PacienteVar->nombres = $paciente->nombres;
                            $PacienteVar->apellidos = $paciente->apellidos;
                            $PacienteVar->cedula = $paciente->cedula;
                            $PacienteVar->sexo = $paciente->sexo;
                            $PacienteVar->fecha_nacimiento = $paciente->fecha_nacimiento;
                            $PacienteVar->peso = $paciente->peso;
                            $PacienteVar->altura = $paciente->altura;
                            $PacienteVar->email = $paciente->email;
                            $PacienteVar->numero_historia = $paciente->numero_historia;
                            $PacienteVar->cedulaRepresentanteLegal = $paciente->cedulaRepresentanteLegal;
                            $PacienteVar->nombreRepresentanteLegal = $paciente->nombreRepresentanteLegal;
                            $PacienteVar->telefonoFijo = $paciente->telefonoFijo;
                            $PacienteVar->telefonoMovil = $paciente->telefonoMovil;
                            $PacienteVar->id_institucion = $request->route('centro');
                            $PacienteVar->save();
                            $count=$count+1;
                            $this->writeLog(ucfirst(trans('titles.patient')),ucfirst(trans('labels.migrate')),null,null,true,null);
                            echo  '<script> pb.value('. intval($count * 100 /count($pacientes)).') </script>'."\n";
                            ob_flush();
                            flush();
                        }
                    DB::getPdo()->commit();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('labels.patient')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    DB::getPdo()->rollBack();
                    $this->AlertDanger($request);
                    $this->writeLog(ucfirst(trans('titles.patient')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                }
            });
            $this->AlertSuccess($request);
            return $response;







    }

    public function importSolicitud(Request $request) {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 0);
        try {
            $this->setLog(ucfirst(trans('titles.requests')));
            $ct=0;
            $db=Oldrecordsdatabase::first();
            $id = $this->readcsv($this->fileIn);
            $result = $this->changeConnection($db)->Select(

                'select id,fecha_solicitud,idadministrativo,id_paciente,id_usuario,
                                                        fechaaprobacionadministrativa,referenteexterno
                                                        from solicitud                                                        
                                                        where id 
                                                        in (select distinct(id_solicitud) from orden where id in ('. implode(',', $id) .')
			                
                            )'
            );

            $response = new StreamedResponse();
            $response->setCallback(function () use($result,$request) {
                $view =view('oldrecords.progress');
                echo $view;
                $count=0;

                try{
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('titels.requests')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')));

                    DB::getPdo()->beginTransaction();
                    foreach ($result as $insert)
                    {
                        $model=new Oldrecordssolicitud;
                        $model->id= $insert->id;
                        $model->fecha_solicitud=$insert->fecha_solicitud;
                        $model->idadministrativo=$insert->idadministrativo;
                        $model->id_paciente=$insert->id_paciente;
                        $model->id_usuario= $insert->id_usuario;
                        $model->fechaaprobacionadministrativa=$insert->fechaaprobacionadministrativa;
                        $model->referenteexterno=$insert->referenteexterno;
                        $model->id_institucion=$request->route('centro');
                        $model->save();
                        $this->writeLog(ucfirst(trans('titles.requests')),ucfirst(trans('labels.migrate')),null,null,true,null);
                        $count=$count+1;
                        echo  '<script> pb.value('. intval($count * 100 /count($result)).') </script>'."\n";
                        ob_flush();
                        flush();
                    }
                    DB::getPdo()->commit();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.requests')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    DB::getPdo()->rollBack();
                    $this->AlertDanger($request);
                    $this->writeLog(ucfirst(trans('titles.requests')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                }
            });
            $this->AlertSuccess($request);
            return $response;

            $this->AlertSuccess($request);
            return back();
        }
        catch (\Exception $e){
            //DB::getPdo()->rollBack();
            $this->AlertDanger($request);
            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function importOrden(Request $request)
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 0);

        $this->setLog(ucfirst(trans('titles.order')));


        try{
            $db=Oldrecordsdatabase::first();

            $lista= $this->readcsv($this->fileIn);

            $ids=array();

            foreach ($lista as $id) {
                if (!in_array($id, $ids )) {
                    array_push($ids, $id);
                }
            }
            unset($lista);


            $response = new StreamedResponse();
            $response->setCallback(function () use($ids,$db,$request) {
                $view =view('oldrecords.progress');
                echo $view;
                $count=0;

                $resultado= array();
                $resultados= array();
                $arraIn=array();
                $select =null;

                $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.orders')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')) );
                DB::getPdo()->beginTransaction();
                try{

                    foreach ($ids as $idorden){
                        $ordenes = $this->changeConnection($db)
                            ->select('select
                ord.id,ord.accession_number,ord.study_instance_id,ord.fecharealizacion,ord.id_procedimiento,ord.id_estatus,ord.id_solicitud,
                ord.id_informe_trascrito,ord.id_addemdum, ord.observaciones,ord.campolibrearchivodocente,ord.birad ,
                inf.id as id_informe, inf.fecha_dictado, inf.fecha_transcripcion,inf.id_dictador,inf.id_transcriptora,inf.texto as texto_informe,
                adde.fecha as fecha_addemdum, adde.texto as texto_addemdum, adde.id_usuario as id_usr_adde, proc.id_modalidad
                from orden as ord
                left outer join informe as inf on ord.id_informe_trascrito = inf.id 
                left outer join addendum as adde on ord.id_addemdum = adde.id
                left outer join solicitud as sol on ord.id_solicitud = sol.id  
                left outer join procedimiento as proc on proc.id = ord.id_procedimiento
                left outer join modalidad as moda on moda.id=proc.id_modalidad        
                                                                                                               
                 where ord.id in (' . $idorden . ')');
                        $count=$count+1;
                        foreach ($ordenes as $orden)
                        {
                            $id='';
                            $fecha='';
                            $tipo='';
                            $resultados = [
                                'id'=>$orden->id,
                                'texto'=>$orden->texto_informe,
                                'id_usr'=>$orden->id_dictador,
                                'fecha'=>$orden->fecha_dictado,
                                'tipo'=>'otro',
                                'fecharealizacion'=>$orden->fecharealizacion,
                                'id_procedimiento'=>$orden->id_procedimiento,
                                'id_estatus'=>$orden->id_estatus,
                                'id_solicitud'=>$orden->id_solicitud,
                                'observaciones'=>$orden->observaciones,
                                'campolibrearchivodocente'=>$orden->campolibrearchivodocente,
                                'birad'=>$orden->birad,
                                'accession_number'=>$orden->accession_number,
                                'study_instance_id'=>$orden->study_instance_id
                            ];

                            if($orden->id_addemdum <> null) {
                                $resultados['texto']=$orden->texto_addemdum;
                                $resultados['id_usr']=  $orden->id_usr_adde;
                                $resultados['fecha']=$orden->fecha_addemdum;
                                $resultados['tipo']='Addemdum';
                            } else if ($orden->id_informe_trascrito) {
                                $resultados['tipo']='Dictado';
                                /*if($orden->fecha_dictado <> null and $orden->fecha_transcripcion <> null) {
                                    if($orden->fecha_dictado > $orden->fecha_transcripcion ) {
                                        $fecha=$orden->fecha_dictado;
                                        $id=$orden->id_dictador;
                                        $tipo='Dictado';
                                    } else {
                                        $fecha=$orden->fecha_transcripcion;
                                        $id=$orden->id_transcriptora;
                                        $tipo='Transcripcion';
                                    }
                                } else if($orden->fecha_dictado <> null and $orden->fecha_transcripcion == null) {
                                    $fecha=$orden->fecha_dictado;
                                    $id=$orden->id_dictador;
                                    $tipo='Dictado';
                                } else if($orden->fecha_dictado == null and $orden->fecha_transcripcion <> null ) {
                                    $fecha=$orden->fecha_transcripcion;
                                    $id=$orden->id_transcriptora;
                                    $tipo='Transcripcion';
                                } else {
                                    $fecha=$orden->fecha_dictado;
                                    $id=$orden->id_dictador;
                                    $tipo='Transcripcion';
                                }

                                $resultados['id_usr']=$id;
                                $resultados['fecha']=$fecha;
                                $resultados['tipo']=$tipo;*/
                            }

                            $ororden= new Oldrecordsorden;
                            $ororden->id=$resultados['id'];
                            $ororden->texto=$resultados['texto'];
                            $ororden->id_usr=$resultados['id_usr'];
                            $ororden->fecha=$resultados['fecha'];
                            $ororden->tipo=$resultados['tipo'];
                            $ororden->fecharealizacion=$resultados['fecharealizacion'];
                            $ororden->id_procedimiento=$resultados['id_procedimiento'];
                            $ororden->id_estatus=$resultados['id_estatus'];
                            $ororden->id_solicitud=$resultados['id_solicitud'];
                            $ororden->observaciones=$resultados['observaciones'];
                            $ororden->campolibrearchivodocente=$resultados['campolibrearchivodocente'];
                            $ororden->birad=$resultados['birad'];
                            $ororden->accession_number=$resultados['accession_number'];
                            $ororden->study_instance_id=$resultados['study_instance_id'];
                            $ororden->id_institucion=$request->route()->getParameter('centro');
                            $ororden->save();
                            //$this->writeLog(ucfirst(trans('titles.order')),ucfirst(trans('prueba')),$ororden->id,null,true,null);
                            ob_flush();
                            flush();
                        }
                        echo  '<script> pb.value('. intval($count * 100 /count($ids)) .') </script>'."\n";
                    }
                    DB::getPdo()->commit();
                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.order')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    DB::getPdo()->rollBack();
                    $this->writeLog(ucfirst(trans('titles.order')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                    return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
                }
            });
            $this->AlertSuccess($request);
            return $response;
        }
        catch (\Exception $e)
        {
            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
            $this->AlertDanger($request);
            return back();
        }
    }

    //MIGRA DATA DE ARCHIVOS ESCANEADO DE SOLICITUD
    public function import_documents_scan(Request $request)
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 0);
        $this->setLog(ucfirst(trans('titles.scan-files')));
        try
        {
            $result="";
            $id=$this->readcsv($this->fileIn);
            $db=Oldrecordsdatabase::first();
            $count = $this->changeConnection($db)->Select('select count(arc.id) as count  from archivo_escaneado as arc
                                left outer join orden as ord on arc.id_solicitud=ord.id_solicitud
                                left outer join solicitud as sol on ord.id_solicitud=sol.id
                                where ord.id in (' . implode(',', $id)  . ' )                                
                                  ');

            $idsDocs = $this->changeConnection($db)->Select('select arc.id_solicitud  from archivo_escaneado as arc
                                left outer join orden as ord on arc.id_solicitud=ord.id_solicitud
                                left outer join solicitud as sol on ord.id_solicitud=sol.id
                                where ord.id in (' . implode(',', $id)  . ' )                                
                                  ');

            $idSolicituds=array();
            foreach ($idsDocs as $row){
                if(!in_array($row->id_solicitud,$idSolicituds)){
                    array_push($idSolicituds,$row->id_solicitud);
                }
            }

            $count=null;
            $idsDocs=null;

            $arr= array();
            $registros=0;
            $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.scan-files')),ucfirst(trans('labels.start')) . ' ' .ucfirst(trans('labels.migration')));


            $response = new StreamedResponse();
            $response->setCallback(function () use($registros,$id,$db,$request,$count,$idSolicituds) {

                $view =view('oldrecords.progress');
                echo $view;
                try{
                    $regIni=0;
                    $centro = $request->route()->getParameter('centro');
                    $file_ext = 'jpeg';

                    $count=count($idSolicituds);

                    foreach ($idSolicituds as $SolicitudID){

                        $archivos = $this->changeConnection($db)->Select('
                                select arc.id, arc.archivo, arc.id_solicitud
                                from archivo_escaneado as arc
                                where arc.id_solicitud = (' . $SolicitudID  . ' )
                                  ' );
                        $countSolicitud=1;
                        foreach($archivos as $imgall) {

                            $file_base =  'doc_' . $imgall->id_solicitud . '_' . $countSolicitud . '_' . $centro . '.' . $file_ext;
                            $docExists= Oldrecordsarchivosescaneados::where('filename','=',$file_base )
                                ->where('id_institucion','=',$centro)
                                ->count();

                                try{
                                    $docs = new Oldrecordsarchivosescaneados;
                                    if ( !file_exists($docs->location . $file_base) ) {
                                        $imagen = imagecreatefromstring($imgall->archivo);
                                        imagejpeg($imagen, $docs->location  . $file_base, 100);
                                        imagedestroy($imagen);
                                    }
                                    if($docExists<=0){


                                    $docs->id=$imgall->id;
                                    $docs->type=$file_ext;
                                    $docs->name=$file_base;
                                    $docs->filename=$file_base;
                                    $docs->description= 'No description available';
                                    $docs->id_solicitud= $imgall->id_solicitud;
                                    $docs->id_institucion=$centro;
                                    $docs->save();
                                    $countSolicitud++;

                                    }

                                    $this->writeLog(ucfirst(trans('titles.scan-files')),ucfirst(trans('labels.migrate')),$imgall->id_solicitud,null,true,null);

                                }catch (\Exception $e){

                                    $this->AlertDanger($request);
                                    $this->writeLog(ucfirst(trans('titles.scan-files')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                                }
                            $registros++;

                        }
                        $regAct=intval($registros * 100 /$count);

                        if($regAct>$regIni){
                            $regIni=$regAct;
                            echo  '<script> pb.value('. $regAct .') </script>'."\n";
                            ob_flush();
                            flush();

                        }
                    }



                    $this->writeLogDB( Auth::user()->id,ucfirst(trans('titles.scan-files')),ucfirst(trans('labels.end')) . ' ' .ucfirst(trans('labels.migration')) ,ucfirst(trans('alerts.success-add')));
                }
                catch(\Exception $e){
                    $this->AlertDanger($request);
                    $this->writeLog(ucfirst(trans('titles.scan-files')),ucfirst(trans('labels.migrate')),null,null,false,$e);
                    echo  '<script> top.location.href="'. route('migrations') .'" </script>';
                }
            });
            $this->AlertSuccess($request);
            return $response;




            //DB::getPdo()->commit();
            $this->AlertSuccess($request);
            return back();
        }
        catch (\Exception $e)
        {
            //DB::getPdo()->rollBack();
            $this->AlertDanger($request);
            //  return back();
            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }

    public function test_connection(Request $request,$redirect=false)
    {
        try{
            Config::set('database.connections.mysql3',array('driver' => 'mysql',
                'host' =>$request->get('host'),
                'port' =>$request->get('puerto'),
                'database'=>$request->get('base'),
                'username' =>$request->get('usuario'),
                'password' =>$request->get('clave'),
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => false,
                'engine' => null));
            DB::connection('mysql3')->getPdo();

            if($redirect){
                return true;
            }
            else{
                $request->session()->flash('message', trans('alerts.success-test-conn'));
                $request->session()->flash('class', 'alert alert-success');
                return back();
            }


        }
        catch (\Exception $e)
        {
            if($redirect){
                return false;
            }
            else{
                $request->session()->flash('message', trans('alerts.error-test-conn'));
                $request->session()->flash('class', 'alert alert-danger');
                return back();
            }

        }
        return back();

    }

    public function verify_connection()
    {
        try{
            $datos=Oldrecordsdatabase::first();
            Config::set('database.connections.mysql3',array('driver' => 'mysql',
                'host' =>$datos->host,
                'port' =>$datos->puerto,
                'database'=>$datos->base,
                'username' =>$datos->usuario,
                'password' =>($datos->clave!='' and $datos->clave!=null) ? decrypt($datos->clave): '',
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => false,
                'engine' => null));
            DB::connection('mysql3')->getPdo();
            return true;
        }
        catch (\Exception $e)
        {
            return false;

        }
    }

    #region Utilidades
    //Cambia configuracion de conexion
    private function changeConnection($params) {
        if ( $this->newConnection == null ) {
            Config::set('database.connections.mysql2',array('driver' => 'mysql',
                'host' =>$params->host,
                'port' =>$params->puerto,
                'database'=>$params->base,
                'username' =>$params->usuario,
                'password' =>decrypt($params->clave),
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => false,
                'engine' => null));
            $this->newConnection = DB::connection('mysql2');
        }

        return $this->newConnection;
    }

    //Indica cuantos ciclos se haran segun la cantidad de data
    private function forRepeat($count,$size)
    {
        return  ($count<=$size) ? 1: (Integer)($count / $size);
    }

    //Lee archivos csv o excel
    private function readcsv($file){
        $myfile = fopen(base_path().'/'.$file, "r") or die("Unable to open file!");
        while(!feof($myfile)) {
            $ret[] = (Integer)fgets($myfile);
        }
        fclose($myfile);
        return $ret;
    }


    private function readcsv_docs($file,$skip=0,$take=2500){
        $items= Excel::load($file, function($reader) use ($skip,$take) {
            return $reader->skipRows($skip)->takeRows($take)->get();
        });
    }
    #region Funciones para mostrar mensajes de alerta
    private function AlertSuccess(Request $request)
    {
        $request->session()->flash('message', trans('alerts.success-add'));
        $request->session()->flash('class', 'alert alert-success');
        return $request;
    }

    private function AlertDanger(Request $request)
    {
        $request->session()->flash('message', trans('alerts.error-add'));
        $request->session()->flash('class', 'alert alert-danger');
        return $request;
    }

    private function AlertInvalidConn(Request $request){
        $request->session()->flash('message', trans('alerts.invalid-test-conn'));
        $request->session()->flash('class', 'alert alert-danger');
    }
    #endregion
    private function GetLimitSelect( $rows,$size =1000)

    {
        $repeat=1;
        if($rows >$size){

            $repeat=intval($rows/$size);

        }
        else
        {
            $repeat=0;

        }
        $result=null;
        for($c=0;$c<=$repeat;$c++)
        {
            $result[$c]=' limit ' . $c *$size . ',  '. $size ;
        }
        return $result;
    }

    private function setLog($file){
        $date=date('Y-m-d h:m:s');
        $name='';
        $route=Oldrecordsdatabase::url_logs();
        $sfile= ($this->searchFile( $file)=='') ? $file .'_' . $date.'.log':  $this->searchFile( $file);
        $name=$route . $sfile;
        Log::useFiles($name);
        // Log::useFiles(Oldrecordsdatabase::url_logs(). $file . date('Y-m-d h:m:s')  .'.log');
    }

    private function writeLog($section,$action=null,$id=null,$date=null, $log=false,\Exception $e=null){
        $date=date('Y-m-d h:m:s');
        $name='';
        /*$route=Oldrecordsdatabase::url_logs();
        $sfile= ($this->searchFile( $section)=='') ? $section .'_' . $date.'.log':  $this->searchFile( $section);
      $name=$route . $sfile;
       Log::useFiles($name);*/
        if(!$log){
            $this->deleteLogs($section);
        }
        if($e==null){
            $date=($date==null)? date('Y-m-d h:m:s'): $date;

            Log::info(' Section: '. $section . '. Action: '. $action . ' Id: '. $id . ' Date: ' . $date . ' User: ' .Auth::user()->email);
        }else{
            Log::error(' Section: '. $section . '. Action: '. $action . ' Id: ' . $id . 'Error code: ' . $e->getCode() . '. Error message: ' . $e->getMessage());
        }
    }


    private function writeLogDB($user_id,$section,$action,$result=null,\Exception $e=null){

        $log=new App\OldRecordsLogs();
        $log->id_user=$user_id;
        $log->action=$action;
        $log->section=$section;

        if($e==null){
            $log->result=$result;// trans('alerts.success-add');
            $log->description=$action;
        }else{
            $log->result=$result;
            $log->description='Code: '. $e->getCode() . 'Message: ' . $e->getMessage();
        }

        $log->save();

    }

    private function searchFile($file){
        if (file_exists( Oldrecordsdatabase::url_logs() )) {
            $dirs= scandir( Oldrecordsdatabase::url_logs());
            foreach ($dirs as $item){
                if(str_contains($item,$file)){
                    //unlink(Oldrecordsdatabase::url_logs() . $item);
                    return $item;
                    break;
                }
            }
        }
        return '';
    }

    private function deleteLogs($table_name){
        $name=-1;
        $result=null;

        if (file_exists( Oldrecordsdatabase::url_logs() )) {
            $dirs= scandir( Oldrecordsdatabase::url_logs());
            foreach ($dirs as $item){

                if(str_contains($item,$table_name)){
                    unlink(Oldrecordsdatabase::url_logs() . $item);
                    break;
                }
            }
        }
    }
    #endregion
    public function consulta(Request $request)
    {
    }

    private function calcularProgreso($contador,$total){
        return intval($contador * 100 /count($total));
    }

    public function searchLog($array){
        $result=array();
        //Oldrecordsdatabase::url_logs()

        foreach ($array as $item){

            $file= $this->searchFile($item);
            if($file!=''){
                array_push($result,[Oldrecordsdatabase::url_logs() . $file]);
            }
        }
        return $result;
    }

    public function showLog($log){
        $text=Oldrecordsdatabase::url_logs().  $this->searchFile($log);
        $content="
        <h2></h2>
            <code>
                <pre>".htmlspecialchars(file_get_contents($text))."</pre>
            </code>";
        //display
        return $content;
        //return "/ Log: <a type='text/local' href=". $filessss[0]['Estado'] ." >asd</a>" ;
    }

    public function fileOrders(Request $ufile){
        $res = Storage::putFileAs( 'olddata/imports/', $ufile->migrateOrders, 'ordenes.csv' );

        if ( $res === false ) {
            $this->AlertDanger($ufile);
            return back();
        } else {
            $this->AlertSuccess($ufile);
            return back();
        }


    }
}
