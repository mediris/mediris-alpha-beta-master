<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Http\Requests;
use App\Transcriber;
use Activity;
use File;
use Log;

class TranscriberController extends Controller {

    public function __construct(){
        $this->logPath = '/logs/transcriber/transcriber.log';
    }

    public function index( Request $request, $print = null ){
        try{
            if( $request->isMethod('post') ){
                if( $print == 1 ){

                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                    echo $data;

                } else {
                    $data = Transcriber::remoteIndexData($request->all());

                    echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }

            return view('transcriber.index');

        } catch( \Exception $e ) {
            return $this->logError( $e, "transcriber", "index");
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            
            $this->validate($request, [
                'text' => 'required',
            ]);
            
            try{
                $data = [];
                $data['text'] = $request->input('text');

                $res = RequestedProcedure::setAsApprove( $id, $data );
                            
                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    session()->flash('message', trans('alerts.success-transcribe-order'));
                    session()->flash('class', 'alert alert-success');

                    return redirect()->route('transcriber');
                }
            }catch( \Exception $e ){
                return $this->logError( $e, "transcriber", "edit");
            }
        }

        try{
            $requestedProcedure = RequestedProcedure::remoteFind($id);
            $requestedProcedure->patient->age = $requestedProcedure->patient->getAgeAndMonth();
            $audio = $requestedProcedure->getDictationFile();
            
            return view('transcriber.edit', [
                                                'requestedProcedure' => $requestedProcedure,
                                                'audio' => $audio,
                                                'allowImpersonate'   => true
                                            ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "transcriber", "edit");
        }
    }
}
