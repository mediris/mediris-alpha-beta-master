<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReferringRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.,]+$/|required|max:50',
            'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.,]+$/|required|max:50',
            'administrative_ID' => 'required|max:10|unique_test:referrings',
            'email' => 'required|email|max:255|unique_test:referrings',
            'telephone_number' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number' => 'min:7|phone:US,VE,MOBILE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
        ];


        if( ! empty( $this->id ) ){
            $rules['administrative_ID'] = $rules['administrative_ID'] . ',' . $this->id;
            $rules['email'] = $rules['email'] . ',' . $this->id;
        }

        return $rules;
    }
}
