<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsumableEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $id = $this->id;

        return [
            'description' => 'regex:/^[A-Za-z\s]+$/|required|max:25|unique_test:consumables,' . $id,
            'unit_id' => 'required',
        ];
    }
}
