<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceRequestAddRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){

        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){

        if( empty( $this->id ) ){
            $uniqueEmail = 'required|unique:patients|email';
            $uniquePatientId = 'required|unique:patients|max:11';
        }else{
            $uniqueEmail = 'required|unique:patients,email,' . $this->id . '|email';
            $uniquePatientId = 'required|unique:patients,patient_ID,' . $this->id . '|max:11';
        }

        return [
            'patient_ID' => 'required|unique:patients|max:11',
            'patient_ID' => $uniquePatientId,
            'patient.first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'patient.last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'patient.birth_date' => 'required|date',
            'patient.address' => 'required|max:250',
            'patient.country_id' => 'required',
            'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
            'email' => $uniqueEmail,
            'patient.citizenship' => 'required|max:25',
            'patient.sex_id' => 'required',
            'patient_type_id' => 'required',
            'source_id' => 'required',
            'answer_id' => 'required',
            'weight' => 'required|regex:/^\d*(\.\d{1}\d?)?$/',
            'height' => 'required|regex:/^\d*(\.\d{1}\d?)?$/',
            'smoking_status_id' => 'required',
            'issue_date' => 'required',
            'patient_state_id' => 'required',
            'requestedProcedures.*.procedure_id' => 'required',

        ];
    }
}
