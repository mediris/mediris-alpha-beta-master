<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Validator;

class LoginRequest extends Request {

    public function authorize(){
        return true;
    }


    public function rules(){
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

}
