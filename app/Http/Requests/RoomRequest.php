<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use GuzzleHttp\Client;
use App\Room;

class RoomRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){

        \Validator::extend('unique_test', function ( $attribute, $value, $parameters, $validator ){
            $table = array_shift($parameters);
            $room = new Room();

            return !$room->unique($this->session()->get('institution')->url, $table, $attribute, $value);

        }, trans('validation.unique'));


        if( $this->isMethod('post') ){
            return [
                'name' => 'required|max:50',
                'description' => 'required|max:250',
                //'administrative_ID' => 'required|unique_test:rooms|max:45',
                'administrative_ID' => 'required|max:45',
                'division_id' => 'required',
            ];
        }else{
            return [

            ];
        }
    }
}
