<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class OrderSuspendRequest extends Request {

    public function authorize(){
        return true;
    }


    public function rules(){
        return [
            'suspensionReason' => 'required'
        ];
    }

}
