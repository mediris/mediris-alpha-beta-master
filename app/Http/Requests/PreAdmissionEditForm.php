<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PreAdmissionEditForm extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
//            'patient_ID' => 'required|unique:patients,patient_ID,' . $this->patient->id . '|max:9',
            //'patient_ID' => 'required',
            'patient.first_name' => 'required|max:25',
            'patient.last_name' => 'required|max:25',
            'patient.birth_date' => 'required|date',
            'patient.address' => 'required|max:250',
            'patient.country_id' => 'required',
            'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
            //'email' => 'required|email',
            'patient.citizenship' => 'required|max:25',
            'patient.sex_id' => 'required',
            'patient_type_id' => 'required',
            'source_id' => 'required',
            'referring_id' => 'required',
            'answer_id' => 'required',
            'serviceRequest.weight' => 'required|numeric|min:0',
            'serviceRequest.height' => 'required|numeric|min:0',
            'pregnancy_status_id' => 'required',
            'smoking_status_id' => 'required',
            'issue_date' => 'required',
            'requestedProcedures.*.procedure_id' => 'required',
        ];

    }
}
