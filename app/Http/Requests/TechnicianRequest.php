<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TechnicianRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'requestedProcedure.number_of_plates' => 'integer|min:0',
            'requestedProcedure.study_number' => 'string',
            'requestedProcedure.plates_size_id' => 'string'
        ];
    }
}
