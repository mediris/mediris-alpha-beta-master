<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConfigurationEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'institution_id' => 'required',
            'appointment_sms_notification' => 'required|numeric|min:1',
            'birthday_expression' => 'required|max:45',
            'birad_0' => 'required|numeric|min:1',
            'birad_1' => 'required|numeric|min:1',
            'birad_2' => 'required|numeric|min:1',
            'birad_3' => 'required|numeric|min:1',
            'birad_4' => 'required|numeric|min:1',
            'birad_5' => 'required|numeric|min:1',
            'birad_6' => 'required|numeric|min:1',
            'birad_7' => 'required|numeric|min:1',
            'birad_8' => 'required|numeric|min:1',
            'mammography_expression' => 'required|max:45',
            'mammography_reminder' => 'required|numeric|min:1',
            'appointment_sms_expression' => 'required|max:45',
        ];
    }
}
