<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AppointmentAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'applicant'=>'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/',
            'room_id' => 'required',
            'procedure_id' => 'required',
            'patient_identification_id' => 'required',
            'patient_email' => 'required|email',
            'patient_first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required',
            'patient_last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required',
            'patient_telephone_number' => 'required|phone:US,VE,FIXED_LINE',
            'patient_cellphone_number' => 'required|phone:US,VE,MOBILE',
            'appointments.*.room_id' => 'required',
            'appointments.*.procedure_id' => 'required',
            'appointments.*.appointment_date_start' => 'required',
            'appointments.*.appointment_date_end' => 'required',
            'appointments.*.procedure_duration' => 'required',
            'appointments.*.patient_identification_id' => 'required',
            'appointments.*.patient_first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required',
            'appointments.*.patient_last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required',
            'appointments.*.patient_email' => 'required|email',
            'appointments.*.patient_telephone_number' => 'required',
            'appointments.*.patient_cellphone_number' => 'required',
            'appointments.*.appointment_status_id' => 'required',
        ];
    }
}
