<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use Closure;
use Illuminate\Session\FileSessionHandler;
use Illuminate\Filesystem\Filesystem;
//use Illuminate\Contracts\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\Storage;

class Authenticate {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */


    public function handle( $request, Closure $next, $guard = null ) {
        //Si se definen los campos user-change y user-change-password, serán usados para
        //loguear temporalmene (solo durante la duración del request actual) a este usuario.
        if ( isset($request['user-change']) && !empty($request['user-change-password']) ) {
            $credentials['id']          = $request['user-change'];
            $credentials['password']    = $request['user-change-password'];

            if ( Auth::once( $credentials ) ) {
                Auth::user()->impersonating = true;

            } else {
                if( $request->ajax() || $request->wantsJson() ){
                    return response('Unauthorized', 401);
                } else {
                    $mb = new MessageBag();
                    $mb->add( 'error', trans('auth.failedOnce') );
                    
                    return redirect()->back()->withErrors($mb)->withinput();
                }
            }
        } 

        if( Auth::guard($guard)->guest() ){
            if( $request->ajax() || $request->wantsJson() ){
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        //return $next($request);
        $response = $next($request);

        if ( $request->input('kill-session') && !$request->ajax() ){
            $request->session()->getHandler()->destroy($request->session()->getId());
            $request->session()->flush();

            $fileSession = new FileSessionHandler( new Filesystem, storage_path('framework/sessions'), 120);
            $fileSession->destroy($request->session()->getId());
            $fileSession->gc(0);
        }
        return $response;
    }
}
