<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use GuzzleHttp\Client;
use Activity;

class MassivaMovil
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toMassivaMovil($notifiable);
        $message = html_entity_decode( $message );
        $message = strip_tags( $message );

        $phone = $notifiable->routeNotificationForMassivaMovil();

        $url = config('massivamovil.webservice');

        $data['usuario'] = config('massivamovil.usuario');
        $data['clave'] = config('massivamovil.clave');
        $data['texto'] = $message;
        $data['telefonos'] = $phone;
        
        $client = new Client();
        $response = $client->request('POST',
                            $url,
                            [ 'form_params' => $data ]
                    );

        $body = $this->remove_utf8_bom( (string )$response->getBody() );
        $body = trim( $body );
        
        Activity::Log( $body );
        //TO DO: evaluar respuesta de status del envío
        //http://sistema.massivamovil.com/files/docs/documentacion-implementacion-API-MassivaMovil.pdf
    }

    //Remove UTF8 Bom
    public function remove_utf8_bom($text) {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }
}