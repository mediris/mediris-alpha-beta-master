<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Configuration extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/configurations';
        parent::__construct();
    }
}