@extends('layouts.app')

@section('title', ucfirst(trans('titles.forbidden')))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1 class="main-title">{{ ucfirst(trans('titles.forbidden')) }} - 403</h1>
            </div>
        </div>
        <div class="row m-t-20">
            <div class="col-md-12">
                <p>{{ ucfirst(trans('errors.403')) }}</p>
            </div>
        </div>
    </div>

@endsection