@extends('layouts.app')

@section('title', 'Error')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-8 col-md-offset-2 col-lg-offset-2">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title">Error {{ $code or '' }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="alert alert-danger m-t-20">
                                    {!! $message or '' !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p><strong>Archivo: </strong>{{ $file or '' }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p><strong>Linea: </strong>{{ $line or '' }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <button data-toggle="collapse" class='btn btn-primary btn-left' data-target="#trace">Traza</button>

                                <div style="clear:both;"></div>
                                <div id="trace" class="collapse">
                                    <pre>{{ $trace or '' }}</pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection