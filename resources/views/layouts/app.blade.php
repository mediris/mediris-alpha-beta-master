<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<html lang="en">
    <head>
        <title>@yield('title','') | Meditron Ris</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        @include('partials.links')
        @stack('styles')

    </head>
    <body id="app-layout" data-page="panels" class="mediris">
        <span id="notification" style="display:none;"></span>
        @include('partials.header')

        <div id="wrapper" >
            @include('partials.nav')

            <div id="main-content" class="meditron" >
                @yield('content')
            </div>

        </div>

        <div id="nprogress">
            <div class="bar" role="bar" style="transform: translate3d(-0.6%, 0px, 0px); transition: all 200ms ease;">
                <div class="peg"></div>
            </div>
        </div>
        

        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        @include('partials.jscripts')
        @include('partials.errors-functions')

        <!-- END PAGE LEVEL SCRIPTS -->

        <script type="text/javascript" src="{{url('/handlebars/handlebars.js')}}"></script>

        <!-- Hotjar Tracking Code for mediris.meditron.logoscorp.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:384538,hjsv:5};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
        </script>


        <!-- TEMPLATE HANDLEBARS -->
        <script id="alert-message-template" type="text/x-handlebars-template">
            <div class="row content-message-header">
                <div class="col-xs-1">
                    <div class="alert-content @{{cssClass}}">
                    </div>

                </div>

                <div class="col-xs-11 @{{newClass}}">
                    <div class="message-text">@{{alertMessage}}</div>
                </div>
            </div>
        </script>
        <script id="ordersTemplate" type="text/x-kendo-template">
            <div class="approve-orders">
                <i class="fa fa-exclamation-circle fa-4x" aria-hidden="true"></i>
                <h3>#= title #</h3>
                <p>#= message #</p>
            </div>
        </script>
            
        
        <script>
            $(document).ready(function () {

                var hasInstitution = "<?= Session::has('institution') ?>";

                if(hasInstitution){


                    setInterval(function() {
                        if($('.k-animation-container').length < 1){
                            $.ajax({
                                    type: 'POST',
                                    url: "<?= route('preadmission') ?>",
                                    data: {data: ''},
                                    dataType: 'json',
                                    success: function (data) {
                                        if(data.total >= 1){
                                            setOrders(data.total);
                                        } 
                                    }
                            });
                            function setOrders(number){
                                var notification = $("#notification").kendoNotification({
                                    position: {
                                        pinned: true,
                                        bottom: 30,
                                        right: 30
                                    },
                                    autoHideAfter: 0,
                                    stacking: "up",
                                    templates: [{
                                        type: "warning",
                                        template: $("#ordersTemplate").html()
                                    }]
                                }).data("kendoNotification");
                                
                                notification.show({
                                    title: TitleOrderToApprove(),
                                    message: NumberOrderToApprove(number)
                                }, "warning");
                                $(document).one("kendo:pageUnload", function(){ if (notification) { notification.hide(); } });
                            }
                        }    
                    }, 30 * 1000); //CADA 30 SEGUNDOS, REVISA SI TIENE ORDNES POR APROBAR Y CORRE EL CODIGO PARA MOSTRAR EL MENSAJE

                    var getMessagesUrl = "<?= route('alertMessages.show') ?>";

                    function checkMessages() { //Trae más mensajes
                        var currentTime = new Date().getTime();
                        localStorage.setItem("previousTime", currentTime);
                        worker.postMessage(getMessagesUrl);
                    }


                    //WORKER ---------------------------------------------------------
                    var worker = new Worker("<?= url('/js/checkMessages.min.js') ?>");

                    worker.addEventListener('message', function(e) {
                        var data = e.data;
                        var messages = data.messages;
                        var noReadMessagesNumber = data.noReadMessages;
                        paintNumberNoReadMessages(noReadMessagesNumber);
                        paintMessages(messages);
                    }, false);
                    //-----------------------------------------------------------------


                    showAllMessages(getMessagesUrl); //Carga inicial de mensajes


                    //INTERVAL ---------------------------------------------------------
                    var interval = "<?= env('ALERT_INTERVAL') ?>";

                    if(!interval){
                        interval = 2;
                    }

                    interval = parseInt(interval)*60000;

                    var previousTime = localStorage.getItem("previousTime"); //Tiempo de la última consulta a los mensajes

                    if(previousTime){
                        var currentTime = new Date().getTime();
                        var diff = currentTime - previousTime; //Calculo diferencia entre el último chequeo y el tiempo actual

                        if(diff >= interval){
                            diff = 0;
                        }

                        //Si excedió el tiempo chequeo de inmediato, sino se llamará cuando se cumpla la diferencia de tiempo
                        setTimeout(function(){
                            checkMessages();
                            setInterval(checkMessages, interval);
                        }, diff);

                    }else{
                        setInterval(checkMessages, interval);
                    }
                    //------------------------------------------------------------------

                    $("#notification-header").click(function(e) { //Guardar mensajes leidos al hacer click
                        var messages = getNoReadMessages();

                        if(JSON.parse(messages).length > 0){
                            $.ajax({
                                type: 'POST',
                                url: "<?= route('alertMessages.lock') ?>",
                                data: messages,
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if(parseInt(data) == 1){
                                        $("#numberMessages").hide();
                                        setNoReadMessages([]);
                                    }
                                }
                            });
                        }

                    });
                    
                    

                }

            });
        </script>
        @stack('scripts')
    </body>
</html>
