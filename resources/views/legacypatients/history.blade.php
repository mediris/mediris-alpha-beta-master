@extends('layouts.app')

@section('title', ucfirst(trans('labels.patient-history')))

@section('content')
    @include('partials.actionbar',[ 'title' => ucfirst(trans('labels.patient-history-migration')),
    'routeBack' => route('legacypatients')
    ])

    <div class="container-fluid">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel sombra x6 elastic">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ trans('titles.patient-info') }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="patient-img">
                                    <img src="{{ $patient->photo != '' ? $patient->photo : '/images/patients/default_avatar.jpeg' }}" alt="Patient-Image" class="img-responsive" id='avatar'/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row patient-info">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $patient->nombres . ' ' . $patient->apellidos }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.id')) }}:</strong> {{ $patient->cedula }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $patient->sexo == 'M' ? trans('labels.male') : trans('labels.female') }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ date('Y') - date('Y', strtotime($patient->fecha_nacimiento)) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong> {{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($patient->fecha_nacimiento)) : date('Y-m-d', strtotime($patient->fecha_nacimiento)) }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p><strong>{{ ucfirst(trans('labels.email')) }}:</strong> {{ $patient->email }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p><strong>{{ trans('labels.telephone-number') }}:</strong> {{ $patient->telefonoFijo }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p><strong>{{ trans('labels.cellphone-number') }}:</strong> {{ $patient->telefonoMovil }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="patient-history" class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

            </div>

        </div>
    </div>

    <script>
        $.get('/servicerequests/legacyhistory/' + {{ $patient->id }} , function (response) {
            $('#patient-history').hide().append(response).fadeIn(400);
        });
    </script>

@endsection
