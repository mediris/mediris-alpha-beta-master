
    <div class="container-fluid">
            <form method="post" action="{{ route('modalities.edit', [$modality]) }}" id="ModalityEditForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
                    <div class="row">
                        <div class="modal-header">
                            <h4 class="modal-title">{{ ucfirst(trans('titles.edit')).' '.trans('titles.modality') }}</h4>
                        </div>
                    </div>
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                <div class="row">
                    <div class="col-md-12">
                        @include('includes.general-checkbox', [
                            'id'        =>'active-chk',
                            'name'      =>'active',
                            'label'     =>'labels.is-active',
                            'condition' => $modality->active
                        ])
                        @if ($errors->has('active'))
                            <span class="help-block">
                                {{ $errors->first('active') }}
                             </span>
                        @endif
                    </div>
                </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
                            </div>
                            <div>
                                <input type="text" class="form-control" name="name" id="name" value="{{ $modality->name }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <label for='parent_id'>{{ ucfirst(trans('labels.parent-modality')) }} *</label>
                            </div>
                            <div>
                                <select class="form-control" name="parent_id" id="parent_id">
                                    <option value="0">{{ ucfirst(trans('labels.none')) }}</option>
                                    @foreach($modalities as $parent)
                                        <option value="{{ $parent->id }}" {{ $parent->id == $modality->parent_id ? 'selected' : '' }}>{{ $parent->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('parent_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('parent_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <label for='description'>{{ ucfirst(trans('labels.description')) }}</label>
                            </div>
                            <div>
                                <textarea class="form-control" name="description" id="description">{{ $modality->description }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                @include('includes.general-checkbox', [
                                        'id'        =>'requires_additional_fields-chk',
                                        'name'      =>'requires_additional_fields',
                                        'label'     =>'labels.requires_additional_fields',
                                        'condition' => $modality->requires_additional_fields
                                    ])
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="modal-footer">
                            <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                                <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                            </button>
                        </div>
                    </div>
            </form>


            <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
            {!! JsValidator::formRequest('App\Http\Requests\ModalityAddRequest', '#ModalityEditForm'); !!}
        
    </div>
    <script>
        //ColorBoxSelectsInit();
        ColorBoxmultiSelectsInit();
    </script>

