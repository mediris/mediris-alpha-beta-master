@extends('layouts.app')

@section('title', ucfirst(trans('titles.modalities')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.modalities')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('modalities.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.modalities')) }}</h1>
				</div>
				
				<?php
				
				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');		
				$NameField = new \Kendo\Data\DataSourceSchemaModelField('name');
				$NameField->type('string');

				$kendo->addFields(array(
					$IdField,
					$NameField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $modalities = $modalities->toArray();
		
                $canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'modalities', 'show');
                $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'modalities', 'edit');
                $canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'modalities', 'active');

				foreach ($modalities as $key => $value) {
					$title = $modalities[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
                    $modalities[$key]['actions'] = '<a href=' . route('modalities.edit', [$modalities[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement" ><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>';
					$active = $modalities[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
                    $editAction = $canEdit ? '<a href=' . route('modalities.edit', [$modalities[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement"><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
                    $activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('modalities.active', [$modalities[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';

					$modalities[$key]['actions'] = $editAction . $activeAction;
				}

				// Set  Modalities
				$kendo->setDataSource($modalities);

				// Create Grid
                $kendo->createGrid('modalities');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Name = new \Kendo\UI\GridColumn();
				$Name->field('name')
						->title(ucfirst(trans('labels.name')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
	                //$Id,
					$Name,
					$Actions,
                ));

				
                // Defines & Generate
                $kendo->generate();

                // Redefines Data Bound
                $kendo->dataBound('onDataBound');

				?>
				
				{!! $kendo->render() !!}
			
			
			</div>
		</div>
	</div>

@endsection
