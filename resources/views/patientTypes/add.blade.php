@extends('layouts.app')

@section('title',ucfirst(trans('titles.add')).' '.trans('titles.patient-types'))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.patient-types')),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.save')),
                                    'form_id' => '#PatientTypeAddForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('patientTypes')
                                ])

    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>

        <form method="post" action="{{ route('patientTypes.add') }}" id="PatientTypeAddForm">

            {!! csrf_field() !!}

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="panel sombra x4 x4-less elastic" id="main-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.main-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'active-chk',
                                        'name'      =>'active',
                                        'label'     =>'labels.active',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('active'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('active') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div>
                                        <label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="administrative_ID"
                                               id="administrative_ID"
                                               value="{{ old('administrative_ID') }}">

                                        @if ($errors->has('administrative_ID'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('administrative_ID') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div>
                                        <label for='icon'>{{ ucfirst(trans('labels.icon')) }} *</label>
                                    </div>
                                    <div>
                                        <select class="form-control" name="icon" id="icon">
                                            <option value="" disabled selected>{{ ucfirst(trans('labels.select')) }}</option>
                                            @foreach($icons as $icon)
                                                <option value="{{ $icon->description }}">{{ $icon->description }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('icon'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('icon') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div>
                                        <label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="description" id="description"
                                               value="{{ old('description') }}">
                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div>
                                        <label for="parent_id">{{ ucfirst(trans('labels.parent-patient-type')) }} *</label>
                                    </div>
                                    <div>
                                        <select class="form-control" name="parent_id" id="parent_id">
                                            <option value="0" selected>{{ ucfirst(trans('labels.none')) }}</option>
                                            @foreach($patientTypes as $patientType)
                                                <option value="{{ $patientType->id }}">{{  $patientType->description }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('parent_id'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('parent_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div>
                                        <label class="control-label"
                                               for="priority">{{ ucfirst(trans('labels.priority')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="number" class="form-control" name="priority" id="priority" min="0"
                                               value="{{ old('priority') }}">
                                        @if ($errors->has('priority'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('priority') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="panel sombra x4 x4-less" id="main-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.general-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'admin_aprob',
                                        'name'      =>'admin_aprob',
                                        'label'     =>'labels.admin-aprobation',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('text-danger'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('admin_aprob') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'sms_send',
                                        'name'      =>'sms_send',
                                        'label'     =>'labels.sms-send',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('sms_send'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('sms_send') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'email_patient',
                                        'name'      =>'email_patient',
                                        'label'     =>'labels.email-patient',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('email_patient'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('email_patient') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'email_refeer',
                                        'name'      =>'email_refeer',
                                        'label'     =>'labels.email-refeer',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('email_refeer'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('email_refeer') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\PatientTypeAddRequest', '#PatientTypeAddForm'); !!}
    </div>
@endsection
