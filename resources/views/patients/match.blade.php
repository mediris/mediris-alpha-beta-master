
@extends('layouts.app')

@section('title',ucfirst(trans('titles.edit')).' '.ucfirst(trans('titles.patient')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.patient')),
    'elem_type' => 'button',
    'elem_name' => ucfirst(trans('labels.save')),
    'form_id' => '#PatientEditForm',
    'route' => '',
    'fancybox' => '',
    'routeBack' => route('patients')
])

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if(Session::has('message'))
                <div class="{{ Session::get('class') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>        
      
    <form id="PatientEditForm" method="post" action="{{ route('patients.match', [$patient]) }}" enctype="multipart/form-data">

        {!! csrf_field() !!}

        <div class="row patients edit">
            <div class="col-xs-12 col-md-4">
                <div class="panel sombra elastic x5">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('labels.main-info') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                                <div class="patient-img ">
                                    @if( isset($patient->id) && !empty($patient->photo))
                                        <img src="{{ asset( 'storage/'. $patient->photo ) }}"
                                        alt="Patient-Image" class="img-responsive" id='avatar'>
                                    @else
                                        <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                        class="img-responsive" id='avatar'>
                                    @endif
                                </div>    
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p><strong>{{ ucfirst(trans('labels.id')) }}:</strong>
                                            {{ $patient->patient_ID }}
                                        </p>
                                    </div>

                                    <div class="col-xs-12">
                                        <div>
                                            <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong>
                                            {{ $patient->first_name . ' ' . $patient->last_name }}</p>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong>
                                            {{ substr($patient->birth_date, 0, 10) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="panel sombra elastic">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('labels.oldpatient-info') }}</h3>
                    </div>
                    <div id="grid">  
@if( $patients && $patients->count() > 0 )
    <?php
    $dataSource = new \Kendo\Data\DataSource();
    
    foreach ($patients as $key => $value) {
        if ( $patients[$key]->id_patient_mediris ) {
            if ( $patients[$key]->id_patient_mediris == $patient->id ) {
                $patients[$key]->actions = 
                "<button type='button' onclick='javascript:toogleLinked(this)' class='btn btn-form-selected choose-patient selected' data-id='" . $patients[$key]->id . "'>".
                    "<i class='fa fa-unlink' aria-hidden='true'></i>" . ucfirst(trans('labels.unlink')) .
                "</button>";
                $patients[$key]->actions .=
                "<input type='checkbox' name='old_patients[]' value='".$patients[$key]->id."' checked>";
            } else {
                $patients[$key]->actions = ucfirst(trans('labels.disabled'));
            } 
        } else {
            $patients[$key]->actions = 
                "<button type='button' onclick='javascript:toogleLinked(this)' class='btn btn-form choose-patient' data-id='" . $patients[$key]->id . "'>".
                    "<i class='fa fa-link' aria-hidden='true'></i>" . ucfirst(trans('labels.link')) .
                "</button>";
            $patients[$key]->actions .=
                "<input type='checkbox' name='old_patients[]' value='".$patients[$key]->id."'>";
        }

        $patients[$key]->name =
            '<div class="customer-name">' . $patients[$key]->nombres . ' ' . $patients[$key]->apellidos . '</div>';
        $patients[$key]->name = html_entity_decode($patients[$key]->name);

        $patients[$key]->birth_date = substr( $patients[$key]->fecha_nacimiento, 0, 10);
    }

    $dataSource->data($patients)
            ->pageSize(7)
            //->schema($schema)
            ->serverFiltering(true)
            ->serverGrouping(true)
            ->serverSorting(true)
            ->serverPaging(true);

    $PatientId = new \Kendo\UI\GridColumn();
    $PatientId->field('cedula')
            ->title(ucfirst(trans('labels.patient-id')));

    $Name = new \Kendo\UI\GridColumn();
    $Name->field('name')
            ->encoded(false)
            ->title(ucfirst(trans('labels.name')));

    $BirthDate = new \Kendo\UI\GridColumn();
    $BirthDate->field('birth_date')
            ->encoded(false)
            ->title(ucfirst(trans('labels.birth-date')));

    $Actions = new \Kendo\UI\GridColumn();
    $Actions->field('actions')
            ->sortable(false)
            ->filterable(false)
            ->encoded(false)
            ->title(ucfirst(trans('labels.actions')));

    $pageable = new \Kendo\UI\GridPageable();
    $pageable->input(true)
            ->numeric(false);

    $grid = new \Kendo\UI\Grid('grid');
    $grid->addColumn($PatientId)
            ->addColumn($Name)
            ->addColumn($BirthDate)
            ->addColumn($Actions)
            ->dataSource($dataSource)
            ->sortable(true)
            ->scrollable(true)
            ->height(500)
            ->filterable(true)
            ->pageable($pageable);
    ?>
    {!! $grid->render() !!}
@else
    <div class="alert alert-warning">
        <strong>Atenci&oacute;n!</strong> No hay resultados para el criterio de b&uacute;squeda seleccionado.
    </div>
@endif

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    function toogleLinked( btn ) {
        this_id = $(btn).attr('data-id');
        alreadyLinked = false;
        $("input[name='old_patients[]']").each( function () {
            if ( $(this).val() == this_id ) {
                if ( $(this).is(':checked') ) {
                    alreadyLinked = true;
                }
                checkbox = $(this);
                return false;
            }
        });

        if ( alreadyLinked ) {
            buttonShowAsUnlinked( $(btn) );
            checkbox.prop('checked', false);
        } else {
            buttonShowAsLinked( $(btn) );
            checkbox.prop('checked', true);
        }
    }

    function buttonShowAsLinked( btn ) {
        $('#oldpatient_id').val( btn.attr('data-id') )
        btn.removeClass('btn-form');
        btn.addClass('btn-form-selected');
        btn.html("<i class='fa fa-unlink' aria-hidden='true'></i> {{ ucfirst(trans('labels.unlink')) }}");
    }

    function buttonShowAsUnlinked( btn ) {
        btn.removeClass('btn-form-selected');
        btn.addClass('btn-form');
        btn.html("<i class='fa fa-link' aria-hidden='true'></i> {{ ucfirst(trans('labels.link')) }}");
    }
</script>


@endsection