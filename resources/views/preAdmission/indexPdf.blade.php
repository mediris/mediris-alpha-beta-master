<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ trans('labels.addendum') }}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="{{ '/css/pdfs.css' }}">
<link rel="stylesheet" href="/resources/assets/css/bootstrap.min.css">
</head>
<body id="app-layout">
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div style="text-align: center;"><img src="{{ url('/images/logo_idaca.svg') }}" alt=""></div>
					<div><h1 class="panel-title">{{ trans('printer.report') }}</h1></div>
					<div><h2 class="panel-title">{{ trans('printer.order-no') . ': ' . $rp->id }}</h2></div>
				</div>
				<div class="panel-body">
					<br>
					<br>
					<p>{!! $addendum !!}</p>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>