@extends('layouts.app')

@section('title', ucfirst(trans('titles.configuration')))

@section('content')

@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.configurations')),
    'elem_type' => 'button',
    'elem_name' => ucfirst(trans('labels.save')),
    'form_id' => '#form-1',
    'route' => '',
    'routeBack' => '',
    'fancybox' => ''
    ])

    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                <div class="{{ Session::get('class') }}">
                    <p>{{ Session::get('message') }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form id="form-1" method="post" action="{{ route('configurations.edit', [$configuration]) }}">

                    {!! csrf_field() !!}

                    <input type="hidden" name="id" value="{{ $configuration->id }}">
                    
                    <div class="row">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!-- CITAS -->
                            <div class="panel x4 x4-less-5 elastic sombra" id="dates-notification-panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ trans('labels.dates-notifications') }}</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row well well-sm">
                                        <div class="col-xs-12 col-md-6">
                                            @include('includes.general-checkbox', [
                                                'id'        =>'appointment_sms',
                                                'name'      =>'appointment_sms',
                                                'label'     =>'labels.appointment',
                                                'via'       => 'SMS',
                                                'condition' => $configuration->appointment_sms
                                            ])

                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div>
                                                <label for="appointment_sms_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'SMS'])) }}</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'appointment_sms_template_id',
                                                    'value' => $configuration->appointment_sms_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row well well-sm">
                                        <div class="col-xs-12 col-md-6">

                                            @include('includes.general-checkbox', [
                                                'id'        =>'appointment_email',
                                                'name'      =>'appointment_email',
                                                'label'     =>'labels.appointment',
                                                'via'       => 'email',
                                                'condition' =>$configuration->appointment_email
                                            ])


                                            {{-- <div id="check-awesome" class="check-center">
                                                <div class="ckeckbox-left">    
                                                    <input type="checkbox" name="appointment_email" id="appointment_email" class="form-control" value="1" {{ $configuration->appointment_email == 1 ? 'checked' : '' }}>  
                                                    <label for="appointment_email">
                                                        <div class="check-container">
                                                            <span class="check"></span>
                                                        </div>
                                                        <span class="box"></span>
                                                    </label> 
                                                </div>
                                                <div class="ckeckbox-right">
                                                    <span class="message-checkbox">
                                                    {{ trans('labels.appointment', ['via' => 'email']) }}
                                                    </span>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div>
                                                <label for="appointment_email_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'Email'])) }}</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'appointment_email_template_id',
                                                    'value' => $configuration->appointment_email_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-4">
                                            <div>
                                                <label for='appointment_sms_notification'>{{ trans('labels.appointment-notification', ['via' => 'sms']) }} *</label>
                                            </div>
                                            <div>
                                                <input type="number" name="appointment_sms_notification"
                                                id="appointment_sms_notification"
                                                class="input-field form-control user btn-style"
                                                value="{{ $configuration->appointment_sms_notification }}"/>
                                            </div>
                                            @if ($errors->has('appointment_sms_notification'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('appointment_sms_notification') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    
                                        <div class="col-xs-12 col-lg-8">
                                            <div>
                                                <label>{{ trans('labels.appointment-expression') }} *</label>
                                            </div>
                                            <div>
                                                <input type="text" name="appointment_sms_expression" id="appointment-expression"
                                                class="input-field form-control user btn-style text-center"
                                                value="{{ $configuration->appointment_sms_expression }}"/>
                                                @if ($errors->has('appointment_sms_expression'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('appointment_sms_expression') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!-- CUMPLEAÑOS -->
                            <!--
                            <div class="panel x2 x2-less-2 sombra" id="dates-notification-panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ trans('labels.birthday-notification') }}</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="kendo-label">
                                                <span class="k-checkbox-wrapper">
                                                    <input type="checkbox" name="birthday_email" id="birthday_email"
                                                    class="k-checkbox"
                                                    value="1" {{ $configuration->birthday_email == 1 ? 'checked' : '' }}/>
                                                    <label for="birthday_email" class="k-checkbox-label"></label>
                                                </span>
                                                <span class="ka-in label">{{ trans('labels.birthday-appointment', ['via' => 'email']) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div>
                                                <label>{{ trans('labels.birthday-expression') }} *</label>
                                            </div>
                                            <div>
                                                <input type="text" name="birthday_expression" id="birthday-expression"
                                                class="input-field form-control user btn-style text-center"
                                                value="{{ $configuration->birthday_expression }}"/>
                                                @if ($errors->has('birthday_expression'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('birthday_expression') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            -->
                            <!-- RESULTADOS  -->
                            <div class="panel x4 sombra" id="results-notification-panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ trans('labels.results-notification') }}</h3>
                                </div>
                                <div class="panel-body">
                                    
                                    <div class="row well well-sm">
                                        <div class="col-xs-12 col-md-6">

                                            @include('includes.general-checkbox', [
                                                'id'        =>'results_sms',
                                                'name'      =>'results_sms',
                                                'label'     =>'labels.results-appointment',
                                                'via'       => 'SMS',
                                                'condition' =>$configuration->results_sms 
                                            ])
                                            
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div>
                                                <label for="results_sms_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'SMS'])) }}</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'results_sms_template_id',
                                                    'value' => $configuration->results_sms_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row well well-sm">
                                        <div class="col-xs-12 col-md-6">

                                            @include('includes.general-checkbox', [
                                                'id'        =>'results_email_auto',
                                                'name'      =>'results_email_auto',
                                                'label'     =>'labels.results-appointment',
                                                'via'       => 'email',
                                                'condition' => $configuration->results_email_auto 
                                            ])
                                            
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div>
                                                <label for="results_email_auto_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'email'])) }} Paciente</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'results_email_auto_template_id',
                                                    'value' => $configuration->results_email_auto_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>

                                            <div>
                                                <label for="results_email_auto_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'email'])) }} Referente</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'results_email_referer_template_id',
                                                    'value' => $configuration->results_email_referer_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>
                                        </div>
                                    </div>

                                



                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="panel x3 sombra" id="results-send-polls">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ trans('labels.send-polls') }}</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row well well-sm">
                                        <div class="col-xs-12 col-md-6">
                                            @include('includes.general-checkbox', [
                                                'id'        =>'active_poll',
                                                'name'      =>'active_poll',
                                                'label'     =>'labels.activePoll',
                                                'via'       => 'SMS',
                                                'condition' =>$configuration->active_poll
                                            ])
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div>
                                                <label for="poll_email_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'email'])) }} Encuesta</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'poll_email_template_id',
                                                    'value' => $configuration->poll_email_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>
                                            <input type="url" class="mt-15px input-field form-control user btn-style valid" name="url_poll" id="url_poll" placeholder="https://example.com" pattern="https://.*" size="90" value="{{ $configuration->url_poll }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="panel x2 sombra" id="results-send-polls">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ trans('labels.report-notification') }}</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row well well-sm">
                                        <div class="col-xs-12 col-md-6">
                                            @include('includes.general-checkbox', [
                                                'id'        =>'results_email',
                                                'name'      =>'results_email',
                                                'label'     =>'labels.results-appointment',
                                                'via'       => 'email',
                                                'condition' =>$configuration->results_email 
                                            ])
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div>
                                                <label for="results_sms_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'email'])) }}</label>
                                            </div>
                                            <div>
                                                @include('includes.select', [
                                                    'idname' => 'results_email_template_id',
                                                    'value' => $configuration->results_email_template_id,
                                                    'data' => $notificationTemplates,
                                                    'keys' => ['id', 'description'] 
                                                ])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <!-- ESTUDIOS BIRAD -->
                        <div class="panel x4 sombra">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ trans('labels.birad-notifications') }}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="row well well-sm">
                                            <div class="col-xs-12 col-md-6">
                                                
                                                @include('includes.general-checkbox', [

                                                    'id'        =>'mammography_sms',
                                                    'name'      =>'mammography_sms',
                                                    'label'     =>'labels.mammography-appointment',
                                                    'via'       => 'SMS',
                                                    'condition' => $configuration->mammography_sms 
                                                ])
                                                
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <div>
                                                    <label for="mammography_sms_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'SMS'])) }}</label>
                                                </div>
                                                <div>
                                                    @include('includes.select', [
                                                        'idname' => 'mammography_sms_template_id',
                                                        'value' => $configuration->mammography_sms_template_id,
                                                        'data' => $notificationTemplates,
                                                        'keys' => ['id', 'description'] 
                                                    ])
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row well well-sm">
                                            <div class="col-xs-12 col-md-6">

                                                @include('includes.general-checkbox', [
                                                    'id'        =>'mammography_email',
                                                    'name'      =>'mammography_email',
                                                    'label'     =>'labels.results-appointment',
                                                    'via'       => 'email',
                                                    'condition' => $configuration->mammography_email 
                                                ])
                                                
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <div>
                                                    <label for="mammography_email_template_id">{{ ucfirst(trans('labels.select-template', ['via' => 'email'])) }}</label>
                                                </div>
                                                <div>
                                                    @include('includes.select', [
                                                        'idname' => 'mammography_email_template_id',
                                                        'value' => $configuration->mammography_email_template_id,
                                                        'data' => $notificationTemplates,
                                                        'keys' => ['id', 'description'] 
                                                    ])
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div>
                                                    <label for='mammography_reminder'>{{ trans('labels.mammography-reminder') }} *</label>
                                                </div>
                                                <div>
                                                    <input type="number" name="mammography_reminder" id="mammography_reminder"
                                                    class="input-field form-control user btn-style"
                                                    value="{{ $configuration->mammography_reminder }}"/>
                                                    @if ($errors->has('mammography_reminder'))
                                                    <span class="text-danger">
                                                        <strong>{{ $errors->first('mammography_reminder') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div>
                                                    <label>{{ trans('labels.mammography-expression') }} *</label>
                                                </div>
                                                <input type="text" name="mammography_expression" id="mammography-expression"
                                                class="input-field form-control user btn-style text-center"
                                                value="{{ $configuration->mammography_expression }}"/>
                                                @if ($errors->has('mammography-expression'))
                                                <span class="text-danger">
                                                    <strong>{{ $errors->first('mammography-expression') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="table-responsive normal-table" id="birad-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        @foreach ( $birads as $birad )
                                                        <th>{{$birad->description}} *</th>
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th>Frecuencia (meses)</th>
                                                        @foreach ( $birads as $birad )
                                                        <td>
                                                            <input type="number" name="birad_{{$birad->id}}" id="birad_{{$birad->id}}"
                                                                class="input-field form-control user btn-style"
                                                                value="{{ $birad->frecuency }}"/>
                                                            @if ($errors->has('birad_'.$birad->id))
                                                            <span class="text-danger">
                                                                <strong>{{ $errors->first('birad_'.$birad->id) }}</strong>
                                                            </span>
                                                            @endif
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\ConfigurationEditRequest', '#form-1'); !!}

        @include('configurations.cron-modal')

    </div>
    <style>
        .mt-15px {
            margin-top: 10px;
        }
    </style>

    @endsection
