<link rel="shortcut icon" href="{{ '/images/mediris_favicon.png' }}">
{{--<link href="{{ '/css/icons/icons.min.css' }}" rel="stylesheet">--}}
{{--<link href="{{ '/css/plugins.min.css' }}" rel="stylesheet">--}}
{{--<link href="{{ '/plugins/bootstrap-loading/lada.min.css' }}" rel="stylesheet">--}}
{{-- <link href="{{ '/css/style.min.css' }}" rel="stylesheet"> --}}
{{--<link href="{{ '/plugins/charts-d3/nv.d3.css' }}"  rel="stylesheet">--}}
{{-- <link href="{{ '/css/style.css' }}" rel="stylesheet"> --}}
{{--<link href="{{ '/plugins/revolution/revolution.css' }}" rel="stylesheet">--}}
<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'> -->
{{-- <link href="{{ '/bower_components/bootstrap/dist/css/bootstrap.min.css' }}" rel="stylesheet">
<link href="{{ '/bower_components/font-awesome/css/font-awesome.min.css' }}" rel="stylesheet">
<link href="{{ '/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css' }}" rel="stylesheet">
<link href="{{ '/bower_components/jquery-colorbox/example1/colorbox.css' }}" rel="stylesheet"> --}}
<link rel="stylesheet" media="print" href="{{ elixir('css/print.min.css') }}">
<link rel="stylesheet" href="{{ '/css/app1.min.css' }}">
<link rel="stylesheet" href="{{ '/telerik/styles/kendo.common.min.css' }}">
<link rel="stylesheet" href="{{ '/telerik/styles/kendo.bootstrap.min.css' }}">
<link rel="stylesheet" href="{{ '/telerik/styles/kendo.default.mobile.min.css' }}">
{{-- <script src="{{ '/bower_components/jquery/dist/jquery.min.js' }}"></script>
<script src="{{ '/bower_components/bootstrap/dist/js/bootstrap.min.js' }}"></script>
<script src="{{ '/bower_components/jquery-colorbox/jquery.colorbox-min.js' }}"></script> --}}
<script src="{{ '/js/app1.min.js' }}"></script>
<script src="{{ '/telerik/js/kendo.all.min.js' }}"></script>


@if ( App::getLocale() == 'en'  )

    <script src="{{ '/telerik/js/messages/kendo.messages.en-US.min.js' }}"></script>
    <script src="{{ '/telerik/js/cultures/kendo.culture.en-US.min.js' }}"></script>
    <script>
        kendo.culture('en-US');
    </script>

@else ( App::getLocale() == 'es' )

    <script src="{{ '/telerik/js/messages/kendo.messages.es-VE.min.js' }}"></script>
    <script src="{{ '/telerik/js/cultures/kendo.culture.es-VE.min.js' }}"></script>
    <script>
        kendo.culture('es-VE');
    </script>

@endif

{{-- <link href="{{ '/plugins/croppic/assets/css/croppic.css' }}" rel="stylesheet"> --}}
{{-- <script src="{{ '/plugins/croppic/croppic.js' }}"></script> --}}
{{-- <link href="{{ '/css/logoscorp.css' }}" rel="stylesheet">
<link href="{{ '/css/pace.css' }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="{{ elixir('css/app2.min.css') }}">