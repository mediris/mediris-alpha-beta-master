<li>
    <a href="{{ route($key) }}">
        <span class="sidebar-text">{{ ucfirst(trans('labels.'.$key)) }}</span>
    </a>
</li>