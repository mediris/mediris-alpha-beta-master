<li><a href="{{ route('radiologist') }}" target="_self">
        <i class="ico icon-radiologo"></i>
        <span class="sidebar-text ">{{ ucfirst(trans('labels.radiologist')) }}</span>
        <span class="arrow glyphicon glyphicon-triangle-right" id="arrow-rad"></span>
    </a>
    <ul class="submenu collapse">
        <li><a href="{{ route('radiologist') }}"><span class="sidebar-text">{{ trans('labels.orders-to-dictate') }}</span></a></li>
        <li><a href="{{ route('radiologist.status', [2]) }}"><span class="sidebar-text">{{ trans('labels.orders-to-approve') }}</span></a></li>
        <li><a href="{{ route('radiologist.status', [3]) }}"><span class="sidebar-text">{{ ucfirst(trans('labels.addendum')) }}</span></a></li>
        <li><a href="{{ route('radiologist.status', [4]) }}"><span class="sidebar-text">{{ trans('labels.orders-dictated') }}</span></a></li>
    </ul>
</li>