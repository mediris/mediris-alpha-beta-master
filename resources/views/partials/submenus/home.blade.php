<li class="{{ (Request::segment(1) == null)? 'current active' : '' }}">
    <a href="/">
        <i class="ico icon-principal"></i>
        <span class="sidebar-text">{{ ucfirst(trans('labels.main')) }}</span>
    </a>
</li>