<?php 
	$subcurrent = false;
	$parent 	= $menu->section;
	$son 		= $submenu->section;
    if(Route::has($submenu->section)){ 
        $route = route($submenu->section);
		if(route($submenu->section) == $path){
			$subcurrent = true;
		} 
    } else {
        $route = '#';
    }
    // Fix Route
    if(isset($parent) && isset($son) && $parent == $son){
    	$here = $submenu->section . '.' . $submenu->action;
    	if(Route::has($here)){
    		$route = route($here);
    	}
    }
    if(isset($submenu->sub)){
    	if(count($submenu->sub) > 0){
    		$subcategories = $submenu->sub;
    	} else {
    		$subcategories = null;
    	}
    } else {
		$subcategories = null;
	}
?>
@if($subcurrent)
	<li class="current">
@else
	<li>
@endif
	    <a href="{{ $route}}">
	    	@if(isset($parent) && isset($son) && $parent != $son)
	        	<span class="sidebar-text">{{ ucfirst(trans('labels.'.$submenu->section)) }}</span>	
	        	@if(isset($subcategories) && count($subcategories) > 0)
    				<span class="arrow glyphicon glyphicon-triangle-right" id="arrow-docent-file" style="transform: rotate(90deg);"></span>
	        	@endif
	        @else 
	        	<span class="sidebar-text">{{ ucfirst(trans('labels.'.$submenu->action)) }}</span>
	        @endif
	    </a>
	    @if(isset($parent) && isset($son) && $parent != $son)
		    @if(isset($subcategories) && count($subcategories) > 0)
	    		<ul class="submenu collapse docent-file-menu">
					@foreach($subcategories as $key => $subcategory)
						@include('partials.submenus.subcategory')
					@endforeach
				</ul>
	    	@endif
	    @endif
	</li>