@if (!Auth::guest())
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="menu-medium" class="sidebar-toggle tooltips">
                <img src="/images/menu.png" width="25px" height="25px">
            </a>

            <a class="navbar-brand" href="{{ url('/') }}"></a>
        </div>
        <!--<div class="navbar-center">AVISOS PERMANENTES!! </div>-->
        <div class="navbar-collapse collapse">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <ul class="nav navbar-nav pull-right header-menu">
                <!-- BEGIN CALENDARIO -->
                <li  id="fecha-header" >
                    <!--<img src="/images/fecha.png" alt="Fecha" width="25" class="p-r-5">-->
                    <span class="username c-text">
                        <i class="dashboard-icon icon-calendar"></i>
                        <script>

                           var mydate=new Date();
                           var year=mydate.getYear();
                           if (year < 1000)
                               year+=1900;
                           var day=mydate.getDay();
                           var month=mydate.getMonth()+1;
                           if (month<10)
                               month="0"+month;
                           var daym=mydate.getDate();
                           if (daym<10)
                               daym="0"+daym;
                           document.write("<font color='1c3e4e' face='Open Sans'>"+daym+"/"+month+"/"+year+"</font>")

                       </script>
                   </span>

               </li>
               <!-- END CALENDARIO -->
               <!-- BEGIN HORA -->
               <li  id="hora-header" >
                <span class="username c-text" >
                    <i class="dashboard-icon icon-time"></i>
                    <span id="reloj"></span>
                    <script type="text/javascript">
                       function startTime(){
                           today=new Date();
                           h=today.getHours();
                           ampm = h >= 12 ? 'pm' : 'am';
                           h = h % 12;
                           h = h ? h : 12;
                           m=today.getMinutes();
                           s=today.getSeconds();
                           m=checkTime(m);
                           s=checkTime(s);
                           document.getElementById('reloj').innerHTML=h+":"+m+":"+s+" "+ampm;
                           t=setTimeout('startTime()',500);}
                           function checkTime(i)
                           {if (i<10) {i="0" + i;}return i;}
                           window.onload=function(){startTime();}
                       </script>
                   </span>

               </li>
               <!-- END HORA -->
               <!-- BEGIN USER -->
               <li  id="user-header">
                <span class="username c-text separador">
                    <i class="dashboard-icon icon-user"></i>
                    {{ ucfirst(Auth::user()->first_name) }} {{  ucfirst(Auth::user()->last_name) }}
                </span>
            </li>
            <!-- END USER  -->



            <!-- BEGIN NOTIFICACIONES -->

            <li class="dropdown config-header" id="notification-header">
                
                <a href="#" class="dropdown-toggle c-text">
                    <div class="row">
                        <div class="col-xs-7">
                            <span class="c-text separador" >
                                {{  ucfirst(trans('labels.notifications')) }}
                            </span>
                        </div>
                        <div class="col-xs-5">
                            <div id="numberMessages" class="circle"><span>0</span></div>
                        </div>
                    </div>
                </a>
                @if(App\AlertMessage::getCurrentMessages())
                <ul class="dropdown-menu">
                    <li class="col-xs-12" >
                        <div id="notifications-container" class="container-fluid notifications-container">
                        </div>
                    </li>
                </ul>
                @endif
            </li>
            <!-- END NOTIFICACIONES  -->

            <!-- BEGIN INSTITUCION -->
            <!--<li  id="messages-header" >
                <span class="username c-text separador"  >
                   {{ Session::has('institution') ?  Session::get('institution')->name : ucfirst(trans('labels.none'))}}
               </span>
           </li>-->
           <!-- END INSTITUCION  -->

           
           <!-- BEGIN CONFIGURACION -->
           <li class="dropdown config-header" >
            <!--<a href="#" class="dropdown-toggle c-text " data-toggle="dropdown" data-hover="dropdown"  data-close-others="true">-->
            <a href="#" class="dropdown-toggle c-text">
                <i class="dashboard-icon icon-engine color-blue-strong"></i>
            </a>
            <ul class="dropdown-menu">
                <li id="clave" class="col-lg-6" >

                    <form action="{{ route('users.password', [Auth::user()]) }}" method="post">

                        {!! csrf_field() !!}

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class=""><strong>{{ ucfirst(trans('titles.change')) }} {{  ucfirst(trans('titles.password')) }}</strong></label>
                                    <input class="form-control" type="password" placeholder="{{ ucfirst(trans('labels.actual-password')) }}" name="actual_password">
                                    @if ($errors->has('actual_password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('actual') }}</strong>
                                    </span>
                                    @endif
                                    <br>

                                    <input class="form-control" type="password" placeholder="{{ ucfirst(trans('labels.password')) }}" name="password">
                                    @if ($errors->has('password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <br>

                                    <input class="form-control" type="password" placeholder="{{ ucfirst(trans('labels.password-confirmation')) }}" name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif

                                    <button id="submit-form" class="btn btn-form btn-right" data-style="expand-left">
                                        <span>{{ ucfirst(trans('labels.change')) }}</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                </li>
                <li class="col-lg-6">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label for="dropdown-institutions"><strong>{{ ucfirst(trans('labels.institution')) }}</strong></label>
                                <select id='dropdown-institutions' name="">

                                    {{ Session::has('institutions') ? '' : Session::put('institutions',  \Auth::user()->getAllinstitutions()) }}

                                    @if(Session::has('institutions'))

                                        @if(Session::has('institution'))
                                            @foreach(Session::get('institutions') as $institution)
                                                <option value="{{ route('institutions.selected', [$institution->id]) }}" {{ Session::get('institution')->id == $institution->id ? 'selected' : '' }}>{{ $institution->name }}</option>
                                            @endforeach
                                        @else
                                            @foreach(Session::get('institutions') as $institution)
                                                <option value="{{ route('institutions.selected', [$institution->id]) }}">{{ $institution->name }}</option>
                                            @endforeach
                                        @endif

                                    @endif
                                    
                                </select>

                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
        <!-- END  CONFIGURACION -->

        <!-- BEGIN LOGOUT -->
        <li id="user-logout">
            <a href="#" class="toggle_fullscreen icon-config" data-toggle="tooltip" data-placement="bottom" title="{{ trans('labels.logout') }}" onclick="document.getElementById('logoutform').submit();">
              <i class="fa fa-power-off"></i>
            </a>
            <form method="POST" id="logoutform" action="{{ route('logout') }}"></form>
        </li>
        <!-- END LOGOUT  -->
    </ul>
    <!-- END TOP NAVIGATION MENU -->
</div>
</div>
</nav>
@endif