{{-- 
    By: Ricardo Martos

    @include(

        'includes.general-checkbox',[

            'id'       =>'id del checkbox', --requerido

            'name'      =>'nombre del checkbox', --requerido

            'label'     =>'mensaje que tenga el checkbox', --requerido

            'readOnly'  =>'true', si es clickeable o no --opcional

            'via'       => 'sms,email,etc, ejemplo: enviar via :via -> enviar via SMS', --opcional

            'condition' =>'condicion si debe estar checked o no ej: $configuration->appointment_email' --opcional

            By: JCH

            se agrega {{ $val or '1' }}, el cual permite indicar si llega un valor $val el check devolvera dicho valor,
            en caso contrario devolvera 1
        ]

    )
     
--}}


<div id="check-awesome">
    <div class="ckeckbox-left">    
        <input type="checkbox" name="{{ $name }}" id="{{ $id }}" class="form-control" value="{{ $val or '1' }}" {{ $condition ? $condition == 1 ? 'checked' : '' : ''   }}>
        <label for="{{ $id }}">

            @if(isset($readOnly))

                <span class="check" onclick="return false;"></span>
                <span class="box" onclick="return false;"></span>

            @else

                <span class="check"></span>
                <span class="box"></span>

            @endif
            
        </label> 
    </div>
    <div class="ckeckbox-right">
        <span class="message-checkbox">

        @if(isset($via))

            {{ ucfirst(trans($label, ['via' => $via])) }}

        @else

            {{ ucfirst(trans($label)) }}

        @endif

        </span>
    </div>
</div>