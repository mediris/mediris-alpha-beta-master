<!-- Modal -->
<div class="modal fade" id="rejectOrder" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="cboxContent">
            <div class="modal-header">
                <button type="button" id="cboxClose" data-dismiss="modal"><span>x</span></button>
                <h4 class="modal-title">{{ ucfirst(trans('labels.reject-reason')) }}</h4>
            </div>
            <div class="modal-body">
                <input type="text" name="reject-input-reason" value="" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-form" id="reject-modal-form">{{ ucfirst(trans('labels.save')) }}</button>
            </div>
        </div>
    </div>
</div>
