<div class="modal fade" id="login-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="cboxContent">
            <div class="login-logo">
                <img src="/images/mediris_icon.png" alt="Mediris Logo">
                <button type="button" id="cboxClose" class="light" data-dismiss="modal"><span>x</span></button>
            </div>
            <div class="login-form ">
                <form method="post" id="login-form">
                    <div class="form-group">
                        <label for='email' class="m-t-20">{{ ucfirst(trans('labels.user-name')) }}</label>
                        <div>
                            @include('includes.select', [
                                                    'idname' => 'user',
                                                    'value' => \Auth::user()->id,
                                                    'data' => \App\User::all(),
                                                    'keys' => ['id', 'username'] 
                                                ])
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">{{ ucfirst(trans('labels.password')) }}</label>
                        <div>
                            <input type="password" id="password" name="password" class="input-field form-control password" />
                        </div>
                    </div>

                    
                    <div class="btn-caja">
                        <button class="btn btn-login ladda-button" data-style="expand-left">
                            <span class="ladda-label">{{ ucfirst(trans('labels.login')) }}</span>
                        </button>
                    </div>
                </form>
                <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
                {!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#user-change-form'); !!}
            </div>    
        </div>
    </div>
</div>