<div class="modal fade" id="scan-file-modal" tabindex="-1" role="dialog" aria-labelledby="scan-file-Label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
          aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="scan-file-Label">{{ ucwords(trans('labels.scan-file-title')) }}</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="croppic-init" id="scan-documents-container">


                <div id="container" class="container-fluid">

                  <div class="content-video">
                    <div class="row">
                      <div id="videoContainer" class="col-md-8 col-md-offset-2" data-filter="none">
                        <video autoplay="true" id="videoElement">
                        </video>
                      </div>
                    </div>
                    <div class="row">
                      <div id="videoControls" class="col-md-8 col-md-offset-2">
                        <button type="button" class="btn btn-primary odremans-start-capture">{{ ucwords(trans('labels.start_camara')) }}</button>
                        <button type="button" class="btn btn-danger odremans-stop-capture">{{ ucwords(trans('labels.stop_camara')) }}</button>
                        <button type="button" class="btn btn-success odremans-take-snapshot">{{ ucwords(trans('labels.capture')) }}</button>
                      </div>
                    </div>
                  </div>

                  <div class="row content-canvas-to-crop">
                    <div class="col-md-8 col-md-offset-2">
                      <canvas id="canvas" width="760" height="570"></canvas>
                    </div>
                    <div class="col-md-8 col-md-offset-2">
                      <button type="button" class="btn btn-primary" id="selectImageCrop">{{ ucwords(trans('labels.crop_capture')) }} </button>
                      <button type="button" class="btn btn-primary" id="resetSelectImageCrop">{{ ucwords(trans('labels.clear')) }} </button>
                    </div>
                  </div>

                  <div class="col-md-12 content-img-crop">

                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-take-picture"
          data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
          <button type="button" class="btn btn-form"
          id="set-photo-scan-file">{{ ucfirst(trans('labels.save')) }}</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
