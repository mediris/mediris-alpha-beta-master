
<div class="container-fluid">
    <form method="post" action="{{ route('consumables.edit', [$consumable->id]) }}" id="ConsumableEditForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.edit')).' '.trans('titles.consumable') }}</h4>
            </div>
        </div>

        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <input type="hidden" value="{{ $consumable->id }}">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('includes.general-checkbox', [
                    'id'        =>'active-chk',
                    'name'      =>'active',
                    'label'     =>'labels.is-active',
                    'condition' => $consumable->active
                ])
                
                @if ($errors->has('active'))
                <span class="text-danger">
                    <strong>{{ $errors->first('active') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
                </div>
                <div>
                    <input type="text" name="description" id="description" class="input-field form-control user btn-style" value="{{ $consumable->description }}"/>
                    @if ($errors->has('description'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='unit_id'>{{ ucfirst(trans('labels.units')) }} *</label>
                </div>
                <div>
                    <select class="form-control" name="unit_id" id="unit_id">
                        @foreach($units as $unit)
                        <option value="{{ $unit->id }}" {{ $unit->id == $consumable->unit_id ? 'selected' : '' }}>{{ $unit->description }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('section_id'))
                    <span class="help-block">
                        {{ $errors->first('section_id') }}
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\ConsumableEditRequest', '#ConsumableEditForm'); !!}
</div>
<script>
        //ColorBoxSelectsInit();
        ColorBoxmultiSelectsInit();
</script>
