@extends('layouts.app')

@section('title',ucfirst(trans('titles.add')).' '.trans('titles.template'))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.add')).' '.trans('titles.template'),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.save')),
                                    'form_id' => '#TemplateAddForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('templates')
                                ])

    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>

        <form method="post" action="{{ route('templates.add') }}" id="TemplateAddForm">
            {!! csrf_field() !!}

            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2">
                    <div class="panel sombra x5">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('titles.add')).' '.trans('titles.template') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'active-chk',
                                        'name'      =>'active',
                                        'label'     =>'labels.is-active',
                                        'condition' => 1
                                    ])
                                    @if ($errors->has('active'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('active') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div>
                                        <label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" name="description" id="description"
                                               class="input-field form-control user btn-style"
                                               value="{{ old('description') }}"/>
                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div>
                                        <label for='template'>{{ ucfirst(trans('labels.template')) }}</label>
                                    </div>
                                    <div>
                                        <textarea class="" name="template"
                                                  id="template">{{ old('template') }}</textarea>
                                        @if ($errors->has('template'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('template') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\TemplateAddRequest', '#TemplateAddForm'); !!}

    </div>
@endsection


