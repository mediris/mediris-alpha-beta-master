@extends('layouts.app')

@section('title', ucfirst(trans('titles.printers')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.printers')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('printers.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.printers')) }}</h1>
				</div>
				
				<?php
				
				$model = new \Kendo\Data\DataSourceSchemaModel();
				
				$IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');
				
				$NameField = new \Kendo\Data\DataSourceSchemaModelField('name');
				$NameField->type('string');
				
				$HostField = new \Kendo\Data\DataSourceSchemaModelField('host');
				$HostField->type('string');
				
				$model
						->addField($IdField)
						->addField($NameField)
						->addField($HostField);
				
				$schema = new \Kendo\Data\DataSourceSchema();
				$schema->model($model);
				
				$dataSource = new \Kendo\Data\DataSource();
				
				$printers = $printers->toArray();
				
				$canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'printers', 'show');
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'printers', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'printers', 'active');
				
				foreach ($printers as $key => $value) {
					$title = $printers[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $printers[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$showAction = $canShow ? '<a href=' . route('printers.show', [$printers[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.show') . '><i class="fa fa-eye" aria-hidden="true"></i></a>' : '';
					$editAction = $canEdit ? '<a href=' . route('printers.edit', [$printers[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement"><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('printers.active', [$printers[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					
					$printers[$key]['actions'] = $editAction . $activeAction;
				}
				
				$dataSource->data($printers)
						->pageSize(20)
						->schema($schema);
				
				$grid = new \Kendo\UI\Grid('grid');
				$grid->attr('view-attr', 'printers');
				
				$Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				
				$Name = new \Kendo\UI\GridColumn();
				$Name->field('name')
						->title(ucfirst(trans('labels.name')));
				
				$Host = new \Kendo\UI\GridColumn();
				$Host->field('host')
						->title(ucfirst(trans('labels.host')));
				
				$Actions = new \Kendo\UI\GridColumn();
				/*$Actions->template(new \Kendo\JavaScriptFunction('function(row){
					if(!(row.id == null))
					{
						return "<a href=/printers/show/"+row.id+" data-toggle=\'tooltip\' data-placement=\'bottom\' title=\'show\'><img src=/images/icon_ver_off.png width=26 /></a>"

							+ "<span>|</span> <a href=/printers/edit/"+row.id+" data-toggle=\'tooltip\' data-placement=\'bottom\' title=\'edit\' class=\'edit-row cboxElement\'><i class =\'fa fa-pencil-square-o\' aria-hidden=true></i></a>"

							+ "<span class=\'slash\'>|</span> <a href=/printers/active/"+row.id+" data-toggle=\'tooltip\' data-placement=\'bottom\' title=\'"+(row.active == 1 ? "disable" : "enable")+"\'><i class =\'fa fa-power-off "+(row.active == 1 ? "" : "active")+"\' aria-hidden=true></i></a>";
					}
					else
					{
						return "";
					}
				}'))*/
				$Actions->field('actions')
						->filterable(false)
						->sortable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));
				
				$pageable = new \Kendo\UI\GridPageable();
				$pageable->input(true)
						->numeric(false);
				
				$grid->addColumn($Name)
						->addColumn($Host)
						->addColumn($Actions)
						->dataSource($dataSource)
						->sortable(true)
						->height(750)
						->filterable(true)
						->pageable($pageable)
						->dataBound('onDataBound');
				?>
				
				{!! $grid->render() !!}
			
			
			</div>
		</div>
	</div>

@endsection