@extends('layouts.app')

@section('title', ucfirst(trans('titles.appointments')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.appointments')),
                                    'elem_type' => '',
                                    'elem_name' => '',
                                    'search_bar' => ucfirst(trans('labels.appointments-search-by')),
                                    'search_bar_title' => ucfirst(trans('titles.search-of-appointments')),
                                    'search_action' => ucfirst(trans('labels.action-search')),
                                    'search_action_id' => 'search-appointment',
                                    'form_id' => '',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => ''
                                ])

    <div class="container-fluid appointments index">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <!--<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-search"></i> {{ ucfirst(trans('titles.search-of-appointments')) }}</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row flex">
                            <div class="col-xs-9 col-sm-10 col-md-10 col-lg-10 flex-end">
                                <input type="text" name="search_criteria" id="search_criteria" class="input-field form-control user btn-style" placeholder="{{ ucfirst(trans('labels.appointments-search-by')) }}"/>
                            </div>
                            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 flex-end">
                                <button id="search-appointment" class="btn btn-form btn-left">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    {{ ucfirst(trans('labels.action-search')) }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="panel sombra">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-calendar"></i> {{ trans('titles.availability') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div>
                                    <label for="room_id_appointment">{{ ucfirst(trans('labels.room')) }} *</label>
                                </div>
                                <div>
                                    @include('includes.select', [
                                        'idname' => 'room_id_appointment',
                                        'data' => $rooms,
                                        'keys' => ['id', 'name']
                                    ])
                                </div>
                            </div>

                            @foreach($rooms as $key => $room)
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 procedure_room hidden" id="procedure_room_{{ $room->id }}">
                                    <div>
                                        <label for="procedure_id_{{ $room->id }}">{{ ucfirst(trans('labels.procedure')) }} *</label>
                                    </div>
                                    <div>
                                        @include('includes.select', [
                                            'idname' => 'procedure_id_' . $room->id,
                                            'data' => array_filter( $room->procedures, function($e) {return $e->active == 1;} ),
                                            'keys' => ['id', 'description'],
                                            'attr' => 'data-live-search="true" data-hide-disabled="true" title="' . ucfirst(trans('labels.select')) . '" '
                                        ])
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div id="room_scheduler" class="room_scheduler col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="scheduler" class="scheduler"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.templates')

    <script>
        //Call to render the scheduler when the combobox of the room change
        $("#room_id_appointment").change(function(){
            room_id = $("#room_id_appointment").val();

            //hide all procedures selects
            $('.procedure_room').addClass('hidden');

            //reset the select
            $("#procedure_id_"+room_id)[0].selectedIndex = 0;
            $("#procedure_id_"+room_id).selectpicker('render');

            if ( validateRoom(room_id) ) {
                //show the procedure select for this room
                $('#procedure_room_'+room_id).removeClass('hidden');
                getAppointments("{{ date("Y/m/d 00:00:00") }}","{{ date("Y/m/d 23:59:59") }}", "day", false);
            } else {
                var scheduler = $("#scheduler").data("kendoScheduler");
                if (scheduler != undefined) {
                    scheduler.destroy();
                    $("#room_scheduler").html('');
                }

                kendo.alert( errorRoomNotReady() );
            }
        });

        var winInterval;
        function startInterval() {
            console.log('start interval');
            getAppointmentsInterval();
            //Refresh the scheduler every 5 seconds
            winInterval = window.setInterval( getAppointmentsInterval, 5000);
        }

        function stopInterval() {
            console.log('stop interval');
            clearInterval(winInterval);
        }
        startInterval();

        function getAppointmentsInterval() {
            var scheduler = $("#scheduler").data("kendoScheduler");
            if ( scheduler != undefined ) {
                var scheduler_start_date = scheduler.view()._startDate.toISOString();
                var scheduler_end_date = scheduler.view()._endDate.toISOString();

                var actualView = translateNameView( scheduler.view().title );
                getAppointments(scheduler_start_date,scheduler_end_date, actualView, true);
            }
        }

        //verify room default values
        function validateRoom( id ) {
            //find the the room
            var room = json_rooms.filter(function(room){
                return room.id == id;
            });
            room = room[0];

            //verify default block size, 60 minutes
            if ( room.block_size == '' || room.block_size == 0 ) {
                return false;
            }
            //verify start_hour default value
            if (room.start_hour == '' || room.start_hour == '0') {
                return false;
            }
            //verify start_hour default value
            if (room.end_hour == '' || room.end_hour == '0') {
                return false;
            }
            //verify quota default value, at least 1
            if (room.quota == '' || room.quota == '0') {
                return false;
            }

            return true;
        }

        //get appointments from api
        function getAppointments(date_start , date_end, view, refresh ) {
            console.log('getAppointments', date_start, date_end );

            var appointments = [];
               var data={
                   start_date:date_start,
                   end_date:date_end
               }

            $.ajax({
                url: '/appointments/datasource',
                type: 'GET',
                async:false,
                data:data
            })
            .done(function(response) {
                appointments = response;
            })
            .fail(function(response) {
                appointments = json_appointments;
            });

            if ( refresh ) {
                schedulerAppointmentsRefresh(
                    appointments
                );
            } else {
                schedulerAppointments(
                    json_rooms,
                    appointments,
                    json_procedures,
                    date_start,
                    view
                );    
            }
            
        }

        //array of recent used ids
        var recents_patient_id = {!! json_encode( session('recents_patient_id', []) ) !!};
        var json_appointments = {!! json_encode($appointments) !!};
        var json_rooms = {!! json_encode($rooms->toArray()) !!};
        var json_procedures = {!! json_encode($procedures->toArray()) !!};
    </script>

    <div class="modal fade" id="appointment-search-modal" tabindex="-1" role="dialog"
         aria-labelledby="patient-search-Label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="appointment-search-Label">{{ ucfirst(trans('labels.search-results')) }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div id="search-modal-content">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close-search"
                            data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection