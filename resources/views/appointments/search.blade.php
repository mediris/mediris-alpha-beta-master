@if($appointments)

    <?php

        //dd($patients);

        $model = new \Kendo\Data\DataSourceSchemaModel();

        $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
        $IdField->type('integer');

        $PatientIdField = new \Kendo\Data\DataSourceSchemaModelField('patient_identification_id');
        $PatientIdField->type('string');

        $PatientNameField = new \Kendo\Data\DataSourceSchemaModelField('patient_first_name');
        $PatientNameField->type('string');


        $PatientEmailField = new \Kendo\Data\DataSourceSchemaModelField('patient_email');
        $PatientEmailField->type('string');


        $model
                ->addField($IdField)
                ->addField($PatientIdField)
                ->addField($PatientNameField)
                ->addField($PatientEmailField);

        $schema = new \Kendo\Data\DataSourceSchema();
        $schema->model($model);

        $dataSource = new \Kendo\Data\DataSource();

    //$patients = $patients->toArray();


        foreach($appointments as $key => $value)
        {
            $appointments[$key]['patient_first_name'] = $appointments[$key]['patient_first_name'] . ' ' . $appointments[$key]['patient_last_name'];
            //$appointments[$key]['patient_first_name'] = $appointments[$key]['room'] . ' ' . $appointments[$key]['room'];
            $appointments[$key]['actions'] = "<a href=' ". route('appointments.show', $appointments[$key]['id']) ." ' class='btn btn-form' id='choose-patient'>"
                    ."<i class='fa fa-check' aria-hidden='true'></i>" . ucfirst(trans('labels.show')) . "</a>";
        }

        $dataSource->data($appointments)
                ->pageSize(7)
                ->schema($schema)
                ->serverFiltering(true)
                ->serverGrouping(true)
                ->serverSorting(true)
                ->serverPaging(true);

        $grid = new \Kendo\UI\Grid('grid');

        $Id = new \Kendo\UI\GridColumn();
        $Id->field('id')
                ->filterable(false)
                ->title('Id');

        $PatientId = new \Kendo\UI\GridColumn();
        $PatientId->field('patient_identification_id')
                ->title(ucfirst(trans('labels.patient-id')));

        $Name = new \Kendo\UI\GridColumn();
        $Name->field('patient_first_name')
                ->encoded(false)
                ->title(ucfirst(trans('labels.name')));

        $Email = new \Kendo\UI\GridColumn();
        $Email->field('patient_email')
                ->encoded(false)
                ->title(ucfirst(trans('labels.email')));

        $Room = new \Kendo\UI\GridColumn();
        $Room->field('room.name')
                ->encoded(false)
                ->title(ucfirst(trans('labels.room')));

        $Procedure = new \Kendo\UI\GridColumn();
        $Procedure->field('procedure.description')
                ->encoded(false)
                ->title(ucfirst(trans('labels.procedure')));

        $Actions = new \Kendo\UI\GridColumn();
        $Actions->field('actions')
                ->sortable(false)
                ->filterable(false)
                ->encoded(false)
                ->title(ucfirst(trans('labels.actions')));

        $pageable = new \Kendo\UI\GridPageable();
        $pageable->input(true)
                ->numeric(false);

        $grid->addColumn($PatientId)
                ->addColumn($Name)
                ->addColumn($Email)
                ->addColumn($Room)
                ->addColumn($Procedure)
                ->addColumn($Actions)
                ->dataSource($dataSource)
                ->sortable(true)
                ->scrollable(true)
                ->height(500)
                ->filterable(true)
                ->pageable($pageable);
    ?>

    {!! $grid->render() !!}

@else

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="alert alert-warning">
                <strong>Atenci&oacute;n!</strong> No hay resultados para el criterio de b&uacute;squeda seleccionado.
            </div>
        </div>
    </div>

@endif
