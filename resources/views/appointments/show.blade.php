@extends('layouts.app')

@section('title',ucfirst(trans('titles.appointment')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.appointment-record')),
                                    'elem_type' => 'button',
                                    'elem_id' => 'print-label',
                                    'elem_name' => ucfirst(trans('labels.print')),
                                    'form_id' => '',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('appointments')
                                ])

    <div class="container-fluid appointments show">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <div class="row flex">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 logo-institution">
                                <h3><img src="{{ Session::get('institution')->logo }}" alt=""></h3>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 flex-end info-institution">
                                <p class="institution_id text-left rif">Rif. {{ Session::get('institution')->institution_id }}</p>
                                <p class="institution_id text-left name"> {{ Session::get('institution')->name }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="record-heading">

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title-show">
                                    <h3 class="record-title">{{ strtoupper(trans('titles.appointment')) }}  {{ App::getLocale() == 'es' ? date('d/m/Y h:i a', strtotime($appointment->appointment_date_start)) : date('m/d/Y h:i a', strtotime($appointment->appointment_date_start)) }}</h3>
                                    {{-- <h3 class="record-title">{{ App::getLocale() == 'es' ? date('d/m/Y h:i a', strtotime($appointment->appointment_date_start)) : date('m/d/Y h:i a', strtotime($appointment->appointment_date_start)) }}</h3> --}}
                                </div>
                            </div>
                        </div>

                        <div id="record-content">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.patient-id')) }}:</strong></p>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left">
                                    <p>{{ $appointment->patient_identification_id }}</p>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.name')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ ucfirst($appointment->patient_first_name) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.last-name')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ ucfirst($appointment->patient_last_name) }}</p>
                                </div>
                            </div>

                            @if(isset($appointment->patient->birth_date))
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <p class="text-right"><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong></p>
                                    </div>

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <p>{{ App::getLocale() == 'es' ? date('d/m/Y', strtotime($appointment->patient->birth_date)) : date('m/d/Y', strtotime($appointment->patient->birth_date)) }}</p>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.appointment-date')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ App::getLocale() == 'es' ? date('d/m/Y', strtotime($appointment->appointment_date_start)) : date('m/d/Y', strtotime($appointment->appointment_date_start)) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.hour')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ date('h:i a', strtotime($appointment->appointment_date_start)) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.room')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ strtoupper($appointment->room->name) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.procedure')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ strtoupper($appointment->procedure->description) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p class="text-right"><strong>{{ ucfirst(trans('labels.indications')) }}:</strong></p>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>{{ strtoupper($appointment->observations) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <p>{{ Session::get('institution')->name }} - {{ Session::get('institution')->telephone_number }}</p>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <p class="text-right">{{ date('Y-m-d h:i a') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
