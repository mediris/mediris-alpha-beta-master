
@extends('layouts.app')

@section('title',ucfirst(trans('titles.edit-appointment')))

@section('content')

    @include('partials.actionbar',[ 'title' => trans('titles.edit-appointment'),
    'elem_type' => 'button',
    'elem_name' => ucfirst(trans('labels.save')),
    'form_id' => '#AppointmentAddForm',
    'route' => '',
    'fancybox' => '',
    'routeBack' => route('appointments')
])

    <div class="container-fluid appointmet add" id="appointmet">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <form action="{{ route('appointments.edit', [$appointment->id]) }}" method="post" id="AppointmentEditForm">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel sombra">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('titles.edit-appointment') }}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div>
                                            <label for="room_id_appointment">{{ ucfirst(trans('labels.room')) }}</label>
                                        </div>
                                        <div>
                                            <select class="form-control" name="room_id" id="room_id_appointment">
                                                <option value="" disabled selected>{{ ucfirst(trans('labels.select')) }}</option>
                                                @foreach($rooms as $room)
                                                    <option value="{{ $room->administrative_ID }}" {{ $appointment->room_id == $room->id ? 'selected' : 'disabled' }}>{{ $room->description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div>
                                            <label for="procedure_id">{{ ucfirst(trans('labels.procedure')) }}</label>
                                        </div>
                                        <div>
                                            <select class="form-control" name="procedure_id" id="procedure_id">
                                                <option value="" disabled selected>{{ ucfirst(trans('labels.select')) }}</option>
                                                @foreach($procedures as $procedure)
                                                    <option value="{{ $procedure->id }}" class="{{ $procedure->getRoomsString(Session::get('institution')->url, $procedure->id) }}" {{ $appointment->procedure_id == $procedure->id ? 'selected' : 'disabled' }}>{{ $procedure->description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='applicant'>{{ ucfirst(trans('labels.applicant')) }}</label>
                                        </div>
                                        <div>
                                            <input type="text" name="applicant" id="applicant" class="input-field form-control user btn-style" value="{{ $appointment->applicant }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel sombra">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="dashboard-icon icon-user"></i>{{ ucfirst(trans('labels.patient_assigned')) }} </h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='patient_identification_id'>{{ ucfirst(trans('labels.patient-id')) }}</label>
                                        </div>
                                        <div>
                                            <input type="text" class="form-control" name="patient_identification_id" id="patient_identification_id" value="{{ $appointment->patient_identification_id }}">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='patient_email'>{{ ucfirst(trans('labels.email')) }}</label>
                                        </div>
                                        <div>
                                            <input type="email" class="form-control" name="patient_email" id="patient_email" value="{{ $appointment->patient_email }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='patient_first_name'>{{ ucfirst(trans('labels.name')) }}</label>
                                        </div>
                                        <div>
                                            <input type="text" name="patient_first_name" id="patient_first_name" class="input-field form-control user btn-style" value="{{ $appointment->patient_first_name }}" />
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='patient_last_name'>{{ ucfirst(trans('labels.last-name')) }}</label>
                                        </div>
                                        <div>
                                            <input type="text" name="patient_last_name" id="patient_last_name" class="input-field form-control user btn-style" value="{{ $appointment->patient_last_name }}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='patient_telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }}</label>
                                        </div>
                                        <div>
                                            <input type="tel" class="form-control" name="patient_telephone_number" id="patient_telephone_number" value="{{ $appointment->patient_telephone_number }}">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div>
                                            <label for='patient_cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }}</label>
                                        </div>
                                        <div>
                                            <input type="tel" class="form-control" name="patient_cellphone_number" id="patient_cellphone_number" value="{{ $appointment->patient_cellphone_number }}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-calendar"></i> Disponibilidad</h3>
                        </div>
                        <div class="panel-body">
                            <div id="scheduler"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id="appointments">

                </div>
            </div>

            <div class="row">

            </div>
        </form>

    </div>



    <script>

        $(document).ready(function(){
            var room_id = $('#room_id_appointment').val();

            var rooms = {!! json_encode($rooms->toArray()) !!};

            var room = rooms.filter(function(room){
                return room.administrative_ID == room_id;
            });

            room = room[0];

            var appointments = {!! json_encode($appointments->toArray()) !!};

            var appointments = appointments.filter(function(appointment){
                return appointment.room_id == room.id;
            });

            var dataSource = [];

            for(var i = 0; i < appointments.length; i++)
            {
                dataSource.push({
                    'id':appointments[i].id,
                    'title':appointments[i].procedure.description + " [" + appointments[i].patient_first_name + " " + appointments[i].patient_last_name + "]",
                    'start': new Date(appointments[i].appointment_date_start),
                    'end': new Date(appointments[i].appointment_date_end),
                    'description': appointments[i].observations,
                    'equipment_id': appointments[i].equipment_id
                });
            }

            var lock_blocks = room.avoided_blocks.split(" ");

            for(var j = 0; j < lock_blocks.length; j++)
            {
                if(lock_blocks[j] != "")
                {
                    var hours = getHours(room.block_size, room.start_hour, room.end_hour, lock_blocks[j]);

                    for(var k = 1; k <= room.equipment_number; k++)
                    {
                        dataSource.push({
                            'id': 'locked-' + j,
                            'title':'lock block',
                            'start': new Date("2016-10-3 " + hours.split("-")[0]),
                            'end': new Date("2016-10-3 " + hours.split("-")[1]),
                            'recurrenceRule': 'FREQ=DAILY',
                            'equipment_id': k,
                        });
                    }
                }
            }

            var equipment = [];

            var colors = [
                "#0079c2",
                "#a94442",
                "#203d4f",
                "#00a2d9",
            ];

            for(var i = 1; i <= room.equipment_number; i++)
            {
                equipment.push({ text: "Equipment "+i, value: i, color: colors[i] });
            }


            $("#scheduler").empty();
            $("#scheduler").kendoScheduler({
                date: "{{ $appointment->appointment_date_start }}",
                majorTick: room.block_size,
                startTime: new Date("2013-6-6 " + room.start_hour),
                endTime: new Date("2013-6-6 " + room.end_hour),
                workDayStart: new Date("2016-09-23 " + room.start_hour),
                workDayEnd: new Date("2016-09-23 "  + room.end_hour),
                minorTickCount: room.quota,
                showWorkHours: true,
                views: ["day", "week", "month"],
                editable: {
                    resize: false,
                },
                dataSource: {
                    data: dataSource
                },

                group: {
                    resources: ["equipment"]
                },

                resources: [
                    {
                        field: "equipment_id",
                        name: "equipment",
                        dataSource: equipment,
                        title: "Equipment",
                    }
                ],

                add: function(e)
                {
                    console.log('add');

                    var errorMessage = "{{ trans('alerts.add-block') }}";
                    kendo.alert(errorMessage);

                    e.preventDefault();
                },

                edit: function(e)
                {
                    console.log('edit');

                    if(e.event.title == 'lock block')
                    {
                        var errorMessage = "{{ trans('alerts.edit-block') }}";
                        kendo.alert(errorMessage);
                        e.preventDefault();
                    }

                    if(e.event.id != {{ $appointment->id }})
                    {
                        var errorMessage = "{{ trans('alerts.edit-block') }}";
                        kendo.alert(errorMessage);
                        e.preventDefault();
                    }
                },

                moveStart: function(e)
                {
                    console.log('moveStart');
                    //$('#'+e.event.uid).remove();
                },

                move: function(e)
                {
                    console.log('move');
                    // $('#'+e.event.uid).remove();
                },

                moveEnd: function(e)
                {
                    console.log('moveEnd');
                    if(e.event.title == 'lock block')
                    {
                        var errorMessage = "{{ trans('alerts.move-block') }}";
                        kendo.alert(errorMessage);
                        e.preventDefault();
                    }
                    if(e.event.id != {{ $appointment->id }})
                    {
                        var errorMessage = "{{ trans('alerts.move-block') }}";
                        kendo.alert(errorMessage);
                        e.preventDefault();
                    }
                },

                remove: function(e)
                {
                    console.log('remove');
                    if(e.event.title == 'lock block')
                    {
                        var errorMessage = "{{ trans('alerts.delete-block') }}";
                        kendo.alert(errorMessage);
                        e.preventDefault();
                    }
                    else
                    {
                        if(e.event.id != {{ $appointment->id }})
                        {
                            var errorMessage = "{{ trans('alerts.delete-block') }}";
                            kendo.alert(errorMessage);
                            e.preventDefault();
                        }
                        else
                        {
                            $('#'+e.event.uid).remove();
                        }
                    }
                },

                cancel: function(e)
                {
                    console.log('cancel');

                    if(e.event.title == 'lock block')
                    {
                        e.preventDefault();
                    }

                    if(!e.event.id)
                    {
                        $('#'+e.event.uid).remove();
                    }
                },

                save: function(e)
                {
                    console.log('save');

                    var scheduler = $("#scheduler").getKendoScheduler();

                    var occurrences = scheduler.occurrencesInRange(e.event.start, e.event.end);

                    occurrences =occurrences.filter(function(occurrence){
                        return occurrence.equipment_id == e.event.equipment_id;
                    });

                    console.log(occurrences);

                    if(occurrences.length > 1)
                    {
                        var errorMessage = "{!! trans('alerts.busy-block') !!}";
                        kendo.alert(errorMessage);
                        e.preventDefault();
                    }
                    else
                    {
                        $('#'+e.event.uid).remove();

                        var procedure_id = $('#procedure_id').val();
                        var procedures = {!! json_encode($procedures->toArray()) !!};

                        var procedure = procedures.filter(function(procedure){
                            return procedure.id == procedure_id;
                        });

                        procedure = procedure[0];


                        var procedure_duration = procedure.rooms.filter(function(procedureRoom){
                            return procedureRoom.administrative_ID == room_id;
                        });

                        procedure_duration = procedure_duration[0].pivot.duration * (room.block_size / room.quota);

                        var random = Math.floor((Math.random() * 40000) + 1);;

                        $('#appointments').append(
                                '<div id='+ e.event.uid +' class="appointment">' +
                                    '<input type="text" name="appointments[' + random + '][active]" value="1">' +
                                    '<input type="text" name="appointments[' + random + '][id]" value="' + e.event.id + '">' +
                                    '<input type="text" name="appointments[' + random + '][equipment_id]" value="' + e.event.equipment_id + '">' +
                                    '<input type="text" name="appointments[' + random + '][string_start_unique]" value="' + moment(e.event.start).format("YYYY-MM-DD HH:mm:ss") + "_" + room_id + "_" + e.event.equipment_id + '">' +
                                    '<input type="text" name="appointments[' + random + '][string_end_unique]" value="' + moment(e.event.end).format("YYYY-MM-DD HH:mm:ss") + "_" + room_id + "_" + e.event.equipment_id + '">' +
                                    '<input type="text" name="appointments[' + random + '][uid]" value="' + e.event.uid + '">' +
                                    '<input type="text" name="appointments[' + random + '][procedure_id]" value="' + $('#procedure_id').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][room_id]" value="' + room.id + '">' +
                                    '<input type="text" name="appointments[' + random + '][appointment_date_start]" value="' + moment(e.event.start).format("YYYY-MM-DD HH:mm:ss") + '">' +
                                    '<input type="text" name="appointments[' + random + '][appointment_date_end]" value="' + moment(e.event.end).format("YYYY-MM-DD HH:mm:ss") + '">' +
                                    '<input type="text" name="appointments[' + random + '][procedure_duration]" value="' + procedure_duration + '">' +
                                    '<input type="text" name="appointments[' + random + '][patient_identification_id]" value="' + $('#patient_identification_id').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][patient_first_name]" value="' + $('#patient_first_name').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][patient_last_name]" value="' + $('#patient_last_name').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][patient_telephone_number]" value="' + $('#patient_telephone_number').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][patient_cellphone_number]" value="' + $('#patient_cellphone_number').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][patient_email]" value="' + $('#patient_email').val() + '">' +
                                    '<input type="text" name="appointments[' + random + '][appointment_status_id]" value="1">' +
                                    '<input type="text" name="appointments[' + random + '][applicant]" value="' + $('#applicant').val() + '">' +
                                    '<textarea name="appointments[' + random + '][observations]">' + e.event.description + '</textarea>' +
                                '</div>'
                        );
                    }
                }
            });
        });


    </script>



@endsection
