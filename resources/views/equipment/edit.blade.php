
<div class="container-fluid">
    <form method="post" action="{{ route('equipment.edit', [$equipment->id]) }}" id="EquipmentEditForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.edit')).' '.trans('titles.equipment') }}</h4>
            </div>
        </div>
        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('includes.general-checkbox', [
                    'id'        =>'active-chk',
                    'name'      =>'active',
                    'label'     =>'labels.is-active',
                    'condition' => $equipment->active
                ])
                @if ($errors->has('active'))
                <span class="help-block">
                    {{ $errors->first('active') }}
                </span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='ae_title'>{{ ucfirst(trans('labels.ae-title')) }} *</label>
                </div>
                <div>
                    <input type="text" class="form-control" name="ae_title" id="ae_title" value="{{ $equipment->ae_title }}">
                    @if ($errors->has('ae_title'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('ae_title') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
                </div>
                <div>
                    <input type="text" class="form-control" name="name" id="name" value="{{ $equipment->name }}">
                    @if ($errors->has('name'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\EquipmentEditRequest', '#EquipmentEditForm'); !!}

</div>
<script>
    //ColorBoxSelectsInit();
    ColorBoxmultiSelectsInit();
</script>

