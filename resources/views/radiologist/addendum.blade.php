@extends('layouts.app')

@section('title',ucfirst(trans('titles.show')).' '.ucfirst(trans('titles.order').' - '.ucfirst(trans('titles.radiologist'))))

@section('content')
    
    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.radiologist')).' - '.ucfirst(trans('titles.addendum').' - '.ucfirst(trans('titles.detail'))),
                                    'elem_type' => 'button',
                                    'elem_name' => ucfirst(trans('labels.save')),
                                    'form_id' => '#RadiologistAddendumForm',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => route('radiologist')
                                ])
    
    <div class="container-fluid radiologist edit">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                 @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('titles.patient-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="patient-img ">
                                        @if( isset($requestedProcedure->patient->id) && !empty($requestedProcedure->patient->photo))
                                            <img src="{{ asset( 'storage/'. $requestedProcedure->patient->photo ) }}"
                                        alt="Patient-Image" class="img-responsive" id='avatar'>
                                        @else
                                            <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                        class="img-responsive" id='avatar'>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}:</strong> {{ $requestedProcedure->patient->first_name . ' ' . $requestedProcedure->patient->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}:</strong> {{ $requestedProcedure->patient->age['years']
                                            }} {{ trans('labels.years') }}, {{ $requestedProcedure->patient->age['months'] }}
                                            {{ $requestedProcedure->patient->age['months'] == 1 ? trans('labels.month') : trans('labels.months')
                                            }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}:</strong> {{ $requestedProcedure->patient->sex_id == 1 ? trans('labels.male') : trans('labels.female') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.birth-date')) }}:</strong> {{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($requestedProcedure->patient->birth_date)) : date('Y-m-d', strtotime($requestedProcedure->patient->birth_date)) }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.history-id')) }}:</strong> {{ $requestedProcedure->patient->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.address')) }}:</strong> {{ $requestedProcedure->patient->address }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.responsable')) }}:</strong> {{ $requestedProcedure->service_request->parent or trans('labels.na') }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.admission-id')) }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.contrast-allergy')) }}:</strong> {{ $requestedProcedure->patient->contrast_allergy ? ucfirst(trans('labels.yes')) : 'No' }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.medical-alerts')) }}:</strong> {{ $requestedProcedure->patient->medical_alerts }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ ucfirst(trans('labels.allergies')) }}: </strong>
                                        {{ $requestedProcedure->patient->allergies }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $requestedProcedure->procedure->description }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.request-num') }}:</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.order-num') }}:</strong> {{ $requestedProcedure->id }}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.patient-type')) }}:</strong> {{ $requestedProcedure->service_request->patient_type->description }}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.source')) }}:</strong> {{ $requestedProcedure->service_request->source->description }}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.referring') . ': ' }}</strong>{{ $requestedProcedure->service_request->referring ? ucwords($requestedProcedure->service_request->referring->first_name . ' ' . $requestedProcedure->service_request->referring->last_name) : ucfirst(trans('labels.not-specified'))}}
                                    </p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.admitted-by') }}:</strong> {{ $requestedProcedure->service_request->user->first_name }} {{ $requestedProcedure->service_request->user->last_name }}</p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.attended-by') }}:</strong> {{ $requestedProcedure->technician_user->first_name }} {{ $requestedProcedure->technician_user->last_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.contrast-study') . ': ' }}</strong>
                                        @if($requestedProcedure->procedure_contrast_study == 1)
                                            {{ trans('labels.yes')}}
                                        @else
                                            {{ trans('labels.not')}}
                                        @endif    
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                      <strong>{{ trans('labels.patient-abdominal-circumference') . ': ' }}</strong> {{ $requestedProcedure->abdominal_circumference or trans('labels.na') }}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations') }}:</strong> {{ $requestedProcedure->comments }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.documents')) }}</h3>
                        </div>
                        
                        <div class="panel-body">
                            <div class="k-widget k-upload k-header">
                                <ul class="k-upload-files k-reset">
                                    
                                    @foreach($requestedProcedure->patient->patientDocuments as $key => $patientDocument)
                                        <a href="{{ route('patients.document', ['document' => $patientDocument->id ]) }}"
                                           target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}"
                                                attr-id="{{ $patientDocument->id }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                                <span class="k-file-state"></span>
                                            </span>
                                                <span class="k-file-name-size-wrapper">
                                                <span class="k-file-name"
                                                      filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                                <span class="k-file-size">95.56 KB</span>
                                            </span>
                                            </li>
                                        </a>
                                    @endforeach
                                    
                                    @foreach($requestedProcedure->service_request->request_documents as $key => $requestDocument)
                                        <a href="{{ route('reception.document', ['document' => $requestDocument->id ]) }}"
                                           target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                <span class="k-file-extension">{{ $requestDocument->type }}</span>
                                                <span class="k-file-state"></span>
                                            </span>
                                                <span class="k-file-name-size-wrapper">
                                                <span class="k-file-name"
                                                      title="{{ $requestDocument->name }}">{{ $requestDocument->name }}</span>
                                                <span class="k-file-size">95.56 KB</span>
                                            </span>
                                            </li>
                                        </a>
                                    @endforeach
                                
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="panel sombra">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#order-info"><h1
                                                    class="tab-title">{{ trans('labels.order-info') }}</h1></a></li>
                                </ul>
                                
                                <div class="tab-content">
                                    <div id="order-info" class="tab-pane fade in active">
                                        
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="panel">
                                                    <a href="{{ route('radiologist.pac', ['id'=>$requestedProcedure->id]) }}" class="btn btn-form btn-pac">
                                                      <span><i class="fa fa-picture-o" aria-hidden="true"></i> {{ trans('labels.open-images') }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="panel">
                                                    <div class="panel-heading bg-blue">
                                                        <h4 class="panel-title">{{ trans('titles.addendum') }}</h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row m-t-20">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 big-textarea-plantilla">
                                                                <form action="{{ route('radiologist.addendum', [$requestedProcedure->id]) }}"
                                                                      method='post' id="RadiologistAddendumForm">
                                                                    {{ csrf_field() }}
                                                                    <div>
                                                                        <label for='template'>{{ ucfirst(trans('labels.addendum-text')) }} *</label>
                                                                    </div>
                                                                    <textarea class="" name="text" id="template">{{ old('template') }}</textarea>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="">
                                                    <div class="panel-heading bg-blue">
                                                        <h4 class="panel-title">{{ trans('titles.transcription') }}</h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row m-t-20">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                @if ( isset($requestedProcedure->addendums) && !empty($requestedProcedure->addendums) )
                                                                    @foreach ( $requestedProcedure->addendums as $addendum )
                                                                        <div class="panel">
                                                                            <div class="panel-heading"> 
                                                                                <h4 class="panel-title">Addendum {{ $addendum->created_at }}</h4>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                                {!! html_entity_decode($addendum->text) !!}
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">Informe {{ $requestedProcedure->approval_date }}</h4>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        {!! html_entity_decode($requestedProcedure->text) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection