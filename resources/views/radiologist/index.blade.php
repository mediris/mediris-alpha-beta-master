@extends('layouts.app')

@section('title', ucfirst(trans('labels.radiologist')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('labels.radiologist')),
									'elem_type' => '',
									'elem_name' => '',
									'form_id' => '',
									'route' => '',
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid radiologist index">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<ul class="nav nav-tabs">
					<li class="{{ $status == 1 || $status == null ? 'active' : '' }}">
						<a data-toggle="tab" href="#orders-to-dictate">
							<h1 class="tab-title">{{ trans('labels.orders-to-dictate') }}</h1>
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#own-orders">
							<h1 class="tab-title">{{ trans('labels.my-orders-dictate') }}</h1>
						</a>
					</li>
					<li class="{{ $status == 2 ? 'active' : '' }}">
						<a data-toggle="tab" href="#orders-to-approve">
							<h1 class="tab-title">{{ trans('labels.orders-to-approve') }}</h1>
						</a>
					</li>
					<li>
						<a data-toggle="tab" href="#own-orders-to-approve">
							<h1 class="tab-title">{{ trans('labels.my-orders-to-approve') }}</h1>
						</a>
					</li>
					<li class="{{ $status == 3 ? 'active' : '' }}">
						<a data-toggle="tab" href="#addendum">
							<h1 class="tab-title">{{ ucfirst(trans('labels.addendum')) }}</h1>
						</a>
					</li>
					<li class="{{ $status == 4 ? 'active' : '' }}">
						<a data-toggle="tab" href="#orders-dictated">
							<h1 class="tab-title">{{ trans('labels.orders-dictated') }}</h1>
						</a>
					</li>
				</ul>
				
				<div class="tab-content">
					<div id="orders-to-dictate" class="tab-pane fade {{ $status == 1 || $status == null ? 'in active' : '' }}">
						<div class="title row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h1>{{ trans('labels.orders-to-dictate') }}</h1>
							</div>
						</div>
						
						<?php
						
						// Grid
		                $options = (object) array(
		                    'url' => '/radiologist/3'
		                );
		                $kendo    = new \App\CustomKendoGrid($options);

		                // Fields
		                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
						$PatientTypeIconField->type('string');
						$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
						$PatientTypeField->type('string');
						$AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						$AdmissionDateField->type('date');
						$AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						$AdmissionHourField->type('date');
						$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
						$RequestNumberField->type('string');
						$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
						$OrderNumberField->type('string');
						$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
						$ProcedureField->type('string');
						$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
						$PatientFirstNameField->type('string');
						$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
						$PatientLastNameField->type('string');
						$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
						$PatientIDField->type('string');
						$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
						$BlockedStatusField->type('integer');
						$BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
						$BlockingUserId->type('integer');
						$BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
						$BlockingUserName->type('integer');
						
		                // Add Fields
		                $kendo->addFields(array(
							$PatientTypeIconField,
							$AdmissionDateField,
							$AdmissionHourField,
							$RequestNumberField,
							$OrderNumberField,
							$ProcedureField,
							$PatientLastNameField,
							$PatientFirstNameField,
							$PatientIDField,
							$BlockedStatusField,
							$BlockingUserId,
							$BlockingUserName,
		                ));

		                // Create Schema
		                $kendo->createSchema(true, true);

		                // Create Data Source
		                $kendo->createDataSource();
		                
		                // Create Grid
		                $kendo->createGrid();

		                // Columns
		                $PatientTypeIcon = new \Kendo\UI\GridColumn();
						$PatientTypeIcon->field('patientTypeIcon')
								->attributes(['class' => 'font-awesome-td'])
								->filterable(false)
								->encoded(false)
								->title(ucfirst(trans('labels.patient-type-icon')));
						$PatientType = new \Kendo\UI\GridColumn();
						$PatientType->field('patientType')
								->title(ucfirst(trans('labels.technician-patient-type')));
						
						$Modality = new \Kendo\UI\GridColumn();
						$Modality->field('modality')
								->title(ucfirst(trans('labels.modality')));
						
						$AdmissionDate = new \Kendo\UI\GridColumn();
						$AdmissionDate->field('serviceIssueDate')
								->format('{0: dd/MM/yyyy}')
								->title(ucfirst(trans('labels.admission-date')));
						$AdmissionHour = new \Kendo\UI\GridColumn();
						$AdmissionHour->field('serviceIssueDate')
								->filterable(false)
								->format('{0: h:mm tt}')
								->title(ucfirst(trans('labels.admission-hour')));
						$RequestNumber = new \Kendo\UI\GridColumn();
						$RequestNumber->field('serviceRequestID')
								->title(ucfirst(trans('labels.service-request-id')));
						$OrderNumber = new \Kendo\UI\GridColumn();
						$OrderNumber->field('orderID')
								->title(ucfirst(trans('labels.order-id')));
						$Procedure = new \Kendo\UI\GridColumn();
						$Procedure->field('procedureDescription')
								->title(ucfirst(trans('labels.procedure')));
						$PatientFirstName = new \Kendo\UI\GridColumn();
						$PatientFirstName->field('patientFirstName')
								->title(ucfirst(trans('labels.patient.first-name')));
						$PatientLastName = new \Kendo\UI\GridColumn();
						$PatientLastName->field('patientLastName')
								->title(ucfirst(trans('labels.patient.last-name')));
						
						$actions = new \Kendo\UI\GridColumn();
						$actions->title(trans('labels.actions'));
						
						$slash = "<span class='slash'>|</span>";
						
						$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');
						
						$canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');
						
						$lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingUserName) + \"'><i class='fa \" + (row.blockedStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
						
						$addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');
						
						$editAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/edit') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');
						
						
						$actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $lockAction . $addListAction . $editAction . ";
                            }"));

		                // Excel
		                $kendo->setExcel('titles.orders-to-dictate-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'), 'page-template1');

		                // PDF
		                $kendo->setPDF('titles.orders-to-dictate-list', 'radiologist/3/1', '
		                    function (e) {
		                        e.sender.hideColumn(6);
		                        e.promise.done(function() {
		                            e.sender.showColumn(6);
		                        });
		                    }', ' ' . date('d-M-Y'), 'page-template1');

		                // Filter
		                $kendo->setFilter();

		                // Pager
		                $kendo->setPager();

		                // Column Menu
		                $kendo->setColumnMenu();

		                $kendo->addcolumns(array(
		                	$PatientTypeIcon,
							$PatientType,
							$Modality,
							$AdmissionDate,
							$AdmissionHour,
							$RequestNumber,
							$OrderNumber,
							$Procedure,
							$PatientLastName,
							$PatientFirstName,
							$actions,
		                ));

		                $kendo->generate(true, true, 'row', 550);

						?>
						
						{!! $kendo->render(); !!}
					</div>

					<div id="own-orders" class="tab-pane fade">
						<div class="title row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h1>{{ trans('labels.my-orders-dictate') }}</h1>
							</div>
						</div>
						
						<?php
						
						// Grid
		                $options = (object) array(
		                    'url' => '/radiologist/3'
		                );
		                $kendo    = new \App\CustomKendoGrid($options);

		                // Fields
		                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
						$PatientTypeIconField->type('string');
						$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
						$PatientTypeField->type('string');
						$AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						$AdmissionDateField->type('date');
						$AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						$AdmissionHourField->type('date');
						$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
						$RequestNumberField->type('string');
						$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
						$OrderNumberField->type('string');
						$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
						$ProcedureField->type('string');
						$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
						$PatientFirstNameField->type('string');
						$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
						$PatientLastNameField->type('string');
						$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
						$PatientIDField->type('string');
						$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
						$BlockedStatusField->type('integer');
						$BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
						$BlockingUserId->type('integer');
						$BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
						$BlockingUserName->type('integer');
						
		                // Add Fields
		                $kendo->addFields(array(
							$PatientTypeIconField,
							$AdmissionDateField,
							$AdmissionHourField,
							$RequestNumberField,
							$OrderNumberField,
							$ProcedureField,
							$PatientLastNameField,
							$PatientFirstNameField,
							$PatientIDField,
							$BlockedStatusField,
							$BlockingUserId,
							$BlockingUserName,
		                ));

		                // Create Schema
		                $kendo->createSchema(true, true);

		                // Create Data Source
		                $kendo->createDataSource(1, 10, array(
		                	array('field' => 'blockedStatus', 'value' => 1, 'operator' => 'eq'),
		                	array('field' => 'blockingUserId', 'value' => Auth::user()->id, 'operator' => 'eq')
		                ));
		                
		                // Create Grid
		                $kendo->createGrid(null, 'grid1');

		                // Columns
		                $PatientTypeIcon = new \Kendo\UI\GridColumn();
						$PatientTypeIcon->field('patientTypeIcon')
								->attributes(['class' => 'font-awesome-td'])
								->filterable(false)
								->encoded(false)
								->title(ucfirst(trans('labels.patient-type-icon')));
						$PatientType = new \Kendo\UI\GridColumn();
						$PatientType->field('patientType')
								->title(ucfirst(trans('labels.technician-patient-type')));
						$Modality = new \Kendo\UI\GridColumn();
						$Modality->field('modality')
								->title(ucfirst(trans('labels.modality')));
						$AdmissionDate = new \Kendo\UI\GridColumn();
						$AdmissionDate->field('serviceIssueDate')
								->format('{0: dd/MM/yyyy}')
								->title(ucfirst(trans('labels.admission-date')));
						$AdmissionHour = new \Kendo\UI\GridColumn();
						$AdmissionHour->field('serviceIssueDate')
								->filterable(false)
								->format('{0: h:mm tt}')
								->title(ucfirst(trans('labels.admission-hour')));
						$RequestNumber = new \Kendo\UI\GridColumn();
						$RequestNumber->field('serviceRequestID')
								->title(ucfirst(trans('labels.service-request-id')));
						$OrderNumber = new \Kendo\UI\GridColumn();
						$OrderNumber->field('orderID')
								->title(ucfirst(trans('labels.order-id')));
						$Procedure = new \Kendo\UI\GridColumn();
						$Procedure->field('procedureDescription')
								->title(ucfirst(trans('labels.procedure')));
						$PatientFirstName = new \Kendo\UI\GridColumn();
						$PatientFirstName->field('patientFirstName')
								->title(ucfirst(trans('labels.patient.first-name')));
						$PatientLastName = new \Kendo\UI\GridColumn();
						$PatientLastName->field('patientLastName')
								->title(ucfirst(trans('labels.patient.last-name')));

						$actions = new \Kendo\UI\GridColumn();
						$actions->title(trans('labels.actions'));
						
						$slash = "<span class='slash'>|</span>";
						
						$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');
						
						$canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');
						
						$lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingUserName) + \"'><i class='fa \" + (row.blockedStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
						
						$addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' href='javascript:;' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . "data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');
						
						$editAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/edit') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');
						
						
						$actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $lockAction . $addListAction . $editAction . ";
                            }"));

		                // Excel
		                $kendo->setExcel('titles.my-orders-to-dictate-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));
						
		                // PDF
		                $kendo->setPDF('titles.my-orders-to-dictate-list', 'radiologist/3/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template2');

		                // Filter
		                $kendo->setFilter();

		                // Pager
		                $kendo->setPager();

		                // Column Menu
		                $kendo->setColumnMenu();

		                $kendo->addcolumns(array(
							$PatientTypeIcon,
							$PatientType,
							$Modality,
							$AdmissionDate,
							$AdmissionHour,
							$RequestNumber,
							$OrderNumber,
							$Procedure,
							$PatientLastName,
							$PatientFirstName,
							$actions,
		                ));

		                $kendo->generate(true, true, 'row', 550);

						?>
						
						{!! $kendo->render() !!}
					
					</div>
					
					<div id="orders-to-approve" class="tab-pane fade {{ $status == 2 ? 'in active' : '' }}">
						<div class="title row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h1>{{ trans('labels.orders-to-approve') }}</h1>
							</div>
						</div>
						
						<?php

						// Grid
		                $options = (object) array(
		                    'url' => '/radiologist/5'
		                );
		                $kendo    = new \App\CustomKendoGrid($options);

		                // Fields
		                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
						$PatientTypeIconField->type('string');
						$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
						$PatientTypeField->type('string');
						$TranscriptionDateField = new \Kendo\Data\DataSourceSchemaModelField('transcriptionDate');
						$TranscriptionDateField->type('date');
						$TranscriptionHourField = new \Kendo\Data\DataSourceSchemaModelField('transcriptionDate');
						$TranscriptionHourField->type('date');
						$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
						$RequestNumberField->type('string');
						$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
						$OrderNumberField->type('string');
						$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
						$ProcedureField->type('string');
						$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
						$PatientFirstNameField->type('string');
						$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
						$PatientLastNameField->type('string');
						$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
						$PatientIDField->type('string');
						$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
						$BlockedStatusField->type('integer');
						$BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
						$BlockingUserId->type('integer');
						$BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
						$BlockingUserName->type('integer');
						
		                // Add Fields
		                $kendo->addFields(array(
							$PatientTypeIconField,
							$TranscriptionDateField,
							$TranscriptionHourField,
							$RequestNumberField,
							$OrderNumberField,
							$ProcedureField,
							$PatientFirstNameField,
							$PatientLastNameField,
							$PatientIDField,
							$BlockedStatusField,
							$BlockingUserId,
							$BlockingUserName,
		                ));

		                // Create Schema
		                $kendo->createSchema(true, true);

		                // Create Data Source
		                $kendo->createDataSource();
		                
		                // Create Grid
		                $kendo->createGrid(null, 'grid2');

		                // Columns
		                $PatientTypeIcon = new \Kendo\UI\GridColumn();
						$PatientTypeIcon->field('patientTypeIcon')
								->attributes(['class' => 'font-awesome-td'])
								->filterable(false)
								->encoded(false)
								->title(ucfirst(trans('labels.patient-type-icon')));
						$PatientType = new \Kendo\UI\GridColumn();
						$PatientType->field('patientType')
								->title(ucfirst(trans('labels.technician-patient-type')));
						$TranscriptionDate = new \Kendo\UI\GridColumn();
						$TranscriptionDate->field('transcriptionDate')
								->format('{0: dd/MM/yyyy}')
								->title(ucfirst(trans('labels.transcription-date')));
						$TranscriptionHour = new \Kendo\UI\GridColumn();
						$TranscriptionHour->field('transcriptionDate')
								->filterable(false)
								->format('{0: h:mm tt}')
								->title(ucfirst(trans('labels.transcription-hour')));
						$RequestNumber = new \Kendo\UI\GridColumn();
						$RequestNumber->field('serviceRequestID')
								->title(ucfirst(trans('labels.service-request-id')));
						$OrderNumber = new \Kendo\UI\GridColumn();
						$OrderNumber->field('orderID')
								->title(ucfirst(trans('labels.order-id')));
						$Procedure = new \Kendo\UI\GridColumn();
						$Procedure->field('procedureDescription')
								->title(ucfirst(trans('labels.procedure')));
						$PatientFirstName = new \Kendo\UI\GridColumn();
						$PatientFirstName->field('patientFirstName')
								->title(ucfirst(trans('labels.patient.first-name')));
						$PatientLastName = new \Kendo\UI\GridColumn();
						$PatientLastName->field('patientLastName')
								->title(ucfirst(trans('labels.patient.last-name')));
						$PatientIDField = new \Kendo\UI\GridColumn();
						$PatientIDField->field('patientID')
								->title(ucfirst(trans('labels.id-number')));
						
						$actions = new \Kendo\UI\GridColumn();
						$actions->title(trans('labels.actions'));
						
						$slash = "<span class='slash'>|</span>";
						
						$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');
						
						$canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');
						
						$canApprove = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'approve');
						
						$lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingUserName) + \"'><i class='fa \" + (row.blockedStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
						
						$addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');
						
						$approveAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/approve') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');
						
						
						$actions->template(new Kendo\JavaScriptFunction("
						   function (row)
						   {
						      return  " . $lockAction . $addListAction . $approveAction . ";
						   }"
						));

		                // Excel
		                $kendo->setExcel('titles.orders-to-approve-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

		                // PDF
		                $kendo->setPDF('titles.orders-to-approve-list', 'radiologist/5/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template3');

		                // Filter
		                $kendo->setFilter();

		                // Pager
		                $kendo->setPager();

		                // Column Menu
		                $kendo->setColumnMenu();

		                $kendo->addcolumns(array(
							$PatientTypeIcon,
							$PatientType,
							$TranscriptionDate,
							$TranscriptionHour,
							$RequestNumber,
							$OrderNumber,
							$Procedure,
							$PatientLastName,
							$PatientFirstName,
							$PatientIDField,
							$actions,
		                ));

		                $kendo->generate(true, true, 'row', 550);
								
						?>
						
						{!! $kendo->render() !!}
					
					</div>

					<div id="own-orders-to-approve" class="tab-pane fade">
						<div class="title row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h1>{{ trans('labels.my-orders-to-approve') }}</h1>
							</div>
						</div>

						<?php

						// Grid
		                $options = (object) array(
		                    'url' => '/radiologist/5'
		                );
		                $kendo    = new \App\CustomKendoGrid($options);

		                // Fields
		                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
						$PatientTypeIconField->type('string');
						$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
						$PatientTypeField->type('string');
						$AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						$AdmissionDateField->type('date');
						$AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						$AdmissionHourField->type('date');
						$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
						$RequestNumberField->type('string');
						$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
						$OrderNumberField->type('string');
						$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
						$ProcedureField->type('string');
						$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
						$PatientFirstNameField->type('string');
						$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
						$PatientLastNameField->type('string');
						$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
						$PatientIDField->type('string');
						$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
						$BlockedStatusField->type('integer');
						$BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
						$BlockingUserId->type('integer');
						$BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
						$BlockingUserName->type('integer');
						
		                // Add Fields
		                $kendo->addFields(array(
							$PatientTypeIconField,
							$AdmissionDateField,
							$AdmissionHourField,
							$RequestNumberField,
							$OrderNumberField,
							$ProcedureField,
							$PatientFirstNameField,
							$PatientLastNameField,
							$PatientIDField,
							$BlockedStatusField,
							$BlockingUserId,
							$BlockingUserName,
		                ));

		                // Create Schema
		                $kendo->createSchema(true, true);

		                // Create Data Source
		                $kendo->createDataSource(1, 10, array(
							array('field' => 'blockedStatus', 'value' => 1, 'operator' => 'eq'),
							array('field' => 'blockingUserId', 'value' => Auth::user()->id, 'operator' => 'eq'),
		                ));
		                
		                // Create Grid
		                $kendo->createGrid(null, 'grid5');

		                // Columns
		                $PatientTypeIcon = new \Kendo\UI\GridColumn();
						$PatientTypeIcon->field('patientTypeIcon')
								->attributes(['class' => 'font-awesome-td'])
								->filterable(false)
								->encoded(false)
								->title(ucfirst(trans('labels.patient-type-icon')));
						$PatientType = new \Kendo\UI\GridColumn();
						$PatientType->field('patientType')
								->title(ucfirst(trans('labels.technician-patient-type')));
						$AdmissionDate = new \Kendo\UI\GridColumn();
						$AdmissionDate->field('serviceIssueDate')
								->format('{0: dd/MM/yyyy}')
								->title(ucfirst(trans('labels.admission-date')));
						$AdmissionHour = new \Kendo\UI\GridColumn();
						$AdmissionHour->field('serviceIssueDate')
								->filterable(false)
								->format('{0: h:mm tt}')
								->title(ucfirst(trans('labels.admission-hour')));
						$RequestNumber = new \Kendo\UI\GridColumn();
						$RequestNumber->field('serviceRequestID')
								->title(ucfirst(trans('labels.service-request-id')));
						$OrderNumber = new \Kendo\UI\GridColumn();
						$OrderNumber->field('orderID')
								->title(ucfirst(trans('labels.order-id')));
						$Procedure = new \Kendo\UI\GridColumn();
						$Procedure->field('procedureDescription')
								->title(ucfirst(trans('labels.procedure')));
						$PatientFirstName = new \Kendo\UI\GridColumn();
						$PatientFirstName->field('patientFirstName')
								->title(ucfirst(trans('labels.patient.first-name')));
						$PatientLastName = new \Kendo\UI\GridColumn();
						$PatientLastName->field('patientLastName')
								->title(ucfirst(trans('labels.patient.last-name')));
						$actions = new \Kendo\UI\GridColumn();
						$actions->title(trans('labels.actions'));

						$slash = "<span class='slash'>|</span>";

						$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');

						$canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');

						$lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingUserName) + \"'><i class='fa \" + (row.blockedStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');

						$addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' href='javascript:;' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . "data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');

						$editAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/approve') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');


						$actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $lockAction . $addListAction . $editAction . ";
                            }"));

		                // Excel
		                $kendo->setExcel('titles.my-orders-to-approve-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

		                // PDF
		                $kendo->setPDF('titles.my-orders-to-approve-list', 'radiologist/5/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template4');

		                // Filter
		                $kendo->setFilter();

		                // Pager
		                $kendo->setPager();

		                // Column Menu
		                $kendo->setColumnMenu();

		                $kendo->addcolumns(array(
							$PatientTypeIcon,
							$PatientType,
							$AdmissionDate,
							$AdmissionHour,
							$RequestNumber,
							$OrderNumber,
							$Procedure,
							$PatientLastName,
							$PatientFirstName,
							$actions,
		                ));

		                $kendo->generate(true, true, 'row', 550);

						?>

						{!! $kendo->render() !!}

					</div>
					
					<div id="addendum" class="tab-pane fade {{ $status == 3 ? 'in active' : '' }}">
						<div class="title row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h1>{{ trans('labels.addendum') }}</h1>
							</div>
						</div>
						
						<?php

						// Grid
		                $options = (object) array(
		                    'url' => '/radiologist/6'
		                );
		                $kendo    = new \App\CustomKendoGrid($options);

		                // Fields
		                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
						$PatientTypeIconField->type('string');
						$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
						$PatientTypeField->type('string');
						$ApprovalDateField = new \Kendo\Data\DataSourceSchemaModelField('approvalDate');
						$ApprovalDateField->type('date');
						$ApprovalHourField = new \Kendo\Data\DataSourceSchemaModelField('approvalDate');
						$ApprovalHourField->type('date');
						$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
						$RequestNumberField->type('string');
						$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
						$OrderNumberField->type('string');
						$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
						$ProcedureField->type('string');
						$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
						$PatientFirstNameField->type('string');
						$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
						$PatientLastNameField->type('string');
						$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
						$PatientIDField->type('string');
						$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
						$BlockedStatusField->type('integer');
						$BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
						$BlockingUserId->type('integer');
						$BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
						$BlockingUserName->type('string');
						
		                // Add Fields
		                $kendo->addFields(array(
							$PatientTypeIconField,
							$ApprovalDateField,
							$ApprovalHourField,
							$RequestNumberField,
							$OrderNumberField,
							$ProcedureField,
							$PatientFirstNameField,
							$PatientLastNameField,
							$PatientIDField,
							$BlockedStatusField,
							$BlockingUserId,
							$BlockingUserName,
		                ));

		                // Create Schema
		                $kendo->createSchema(true, true);

		                // Create Data Source
		                $kendo->createDataSource();
		                
		                // Create Grid
		                $kendo->createGrid(null, 'grid3');

		                // Columns
		                $PatientTypeIcon = new \Kendo\UI\GridColumn();
						$PatientTypeIcon->field('patientTypeIcon')
								->attributes(['class' => 'font-awesome-td'])
								->filterable(false)
								->encoded(false)
								->title(ucfirst(trans('labels.patient-type-icon')));
						$PatientType = new \Kendo\UI\GridColumn();
						$PatientType->field('patientType')
								->title(ucfirst(trans('labels.technician-patient-type')));
						$ApprovalDate = new \Kendo\UI\GridColumn();
						$ApprovalDate->field('approvalDate')
								->format('{0: dd/MM/yyyy}')
								->title(ucfirst(trans('labels.approval-date')));
						$ApprovalHour = new \Kendo\UI\GridColumn();
						$ApprovalHour->field('approvalDate')
								->filterable(false)
								->format('{0: h:mm tt}')
								->title(ucfirst(trans('labels.approval-hour')));
						$RequestNumber = new \Kendo\UI\GridColumn();
						$RequestNumber->field('serviceRequestID')
								->title(ucfirst(trans('labels.service-request-id')));
						$OrderNumber = new \Kendo\UI\GridColumn();
						$OrderNumber->field('orderID')
								->title(ucfirst(trans('labels.order-id')));
						$Procedure = new \Kendo\UI\GridColumn();
						$Procedure->field('procedureDescription')
								->title(ucfirst(trans('labels.procedure')));
						$PatientFirstName = new \Kendo\UI\GridColumn();
						$PatientFirstName->field('patientFirstName')
								->title(ucfirst(trans('labels.patient.first-name')));
						$PatientLastName = new \Kendo\UI\GridColumn();
						$PatientLastName->field('patientLastName')
								->title(ucfirst(trans('labels.patient.last-name')));
						$actions = new \Kendo\UI\GridColumn();
						$actions->title(trans('labels.actions'));
						
						$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');
						
						$editAction = ($canEdit ? "\"<a href='" . url('radiologist/addendum') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');
						
						$actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $editAction . ";
                            }"));

		                // Excel
		                $kendo->setExcel('titles.addendums-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

		                // PDF
		                $kendo->setPDF('titles.addendums-list', 'radiologist/6/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template5');

		                // Filter
		                $kendo->setFilter();

		                // Pager
		                $kendo->setPager();

		                // Column Menu
		                $kendo->setColumnMenu();

		                $kendo->addcolumns(array(
							$PatientTypeIcon,
							$PatientType,
							$ApprovalDate,
							$ApprovalHour,
							$RequestNumber,
							$OrderNumber,
							$Procedure,
							$PatientLastName,
							$PatientFirstName,
							$actions,
		                ));

		                // Auto Bind (Not Data Start)
		                $kendo->autoBind(false);

		                $kendo->generate(true, true, 'row', 550);
						
						?>
						
						{!! $kendo->render() !!}
					
					</div>
					
					<script>
						$("#grid3").kendoGrid({
					        autobind: false
					    });
					</script>

					<div id="orders-dictated" class="tab-pane fade {{ $status == 4 ? 'in active' : '' }}">
						<div class="title row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h1>{{ trans('labels.orders-dictated') }}</h1>
							</div>
						</div>
						
						<?php

						// Grid
		                $options = (object) array(
		                    'url' => '/radiologist/4'
		                );
		                $kendo    = new \App\CustomKendoGrid($options);

		                // Fields
		                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
						$PatientTypeIconField->type('string');
						$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
						$PatientTypeField->type('string');
						$DictationDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceDictationDate');
						$DictationDateField->type('date');
						$DictationHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceDictationDate');
						$DictationHourField->type('date');
						// $AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
						// $AdmissionDateField->type('date');
						$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
						$RequestNumberField->type('string');
						$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
						$OrderNumberField->type('string');
						$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
						$ProcedureField->type('string');
						$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
						$PatientFirstNameField->type('string');
						$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
						$PatientLastNameField->type('string');
						$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
						$PatientIDField->type('string');
						$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
						$BlockedStatusField->type('integer');
						$BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
						$BlockingUserId->type('integer');
						$BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
						$BlockingUserName->type('integer');
						$RadiologistUserId = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserID');
						$RadiologistUserId->type('integer');
						$RadiologistUserName = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserName');
						$RadiologistUserName->type('integer');
						
		                // Add Fields
		                $kendo->addFields(array(
							$PatientTypeIconField,
							// $AdmissionDateField,
							$DictationDateField,
							$DictationHourField,
							$RequestNumberField,
							$OrderNumberField,
							$ProcedureField,
							$PatientLastNameField,
							$PatientFirstNameField,
							$PatientIDField,
							$BlockedStatusField,
							$BlockingUserId,
							$BlockingUserName,
							$RadiologistUserId,
							$RadiologistUserName,
		                ));

		                // Create Schema
		                $kendo->createSchema(true, true);

		                // Create Data Source
		                $kendo->createDataSource(1, 10, array(
							array('field' => 'radiologistUserID', 'value' => Auth::user()->id, 'operator' => 'eq'),
							array('field' => 'blockedStatus', 'value' => 0, 'operator' => 'eq'),
		                ));
		                
		                // Create Grid
		                $kendo->createGrid(null, 'grid4');

		                // Columns
		                $PatientTypeIcon = new \Kendo\UI\GridColumn();
						$PatientTypeIcon->field('patientTypeIcon')
								->attributes(['class' => 'font-awesome-td'])
								->filterable(false)
								->encoded(false)
								->title(ucfirst(trans('labels.patient-type-icon')));
						$PatientType = new \Kendo\UI\GridColumn();
						$PatientType->field('patientType')
								->title(ucfirst(trans('labels.technician-patient-type')));
						// $AdmissionDate = new \Kendo\UI\GridColumn();
						// $AdmissionDate->field('serviceIssueDate')
						// 		->format('{0: dd/MM/yyyy}')
						// 		->title(ucfirst(trans('labels.admission-date')));
						$DictationDate = new \Kendo\UI\GridColumn();
						$DictationDate->field('serviceDictationDate')
								->format('{0: dd/MM/yyyy}')
								->title(ucfirst(trans('labels.dictation-date')));
						$DictationHour = new \Kendo\UI\GridColumn();
						$DictationHour->field('serviceDictationDate')
								->filterable(false)
								->format('{0: h:mm tt}')
								->title(ucfirst(trans('labels.dictation-hour')));
						$RequestNumber = new \Kendo\UI\GridColumn();
						$RequestNumber->field('serviceRequestID')
								->title(ucfirst(trans('labels.service-request-id')));
						$OrderNumber = new \Kendo\UI\GridColumn();
						$OrderNumber->field('orderID')
								->title(ucfirst(trans('labels.order-id')));
						$Procedure = new \Kendo\UI\GridColumn();
						$Procedure->field('procedureDescription')
								->title(ucfirst(trans('labels.procedure')));
						$PatientFirstName = new \Kendo\UI\GridColumn();
						$PatientFirstName->field('patientFirstName')
								->title(ucfirst(trans('labels.patient.first-name')));
						$PatientLastName = new \Kendo\UI\GridColumn();
						$PatientLastName->field('patientLastName')
								->title(ucfirst(trans('labels.patient.last-name')));
						$PatientIDField = new \Kendo\UI\GridColumn();
						$PatientIDField->field('patientID')
								->title(ucfirst(trans('labels.id-number')));
						$actions = new \Kendo\UI\GridColumn();
						$actions->title(trans('labels.actions'));

						$slash = "<span class='slash'>|</span>";

						$canRevert = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'revert');

						$revertAction = ($canRevert ? "\"" . '' . "\" + \"<div><a href='javascript:;' class='revert-order' attr-id=\" + row.orderID + \" data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.revert-order') . "'><i class='fa fa-hand-paper-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\"" : '');

						$actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $revertAction . ";
                            }"));
						
		                // Excel
		                $kendo->setExcel('titles.dictated-orders-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

		                // PDF
		                $kendo->setPDF('titles.dictated-orders-list', 'radiologist/4/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template6');

		                // Filter
		                $kendo->setFilter();

		                // Pager
		                $kendo->setPager();

		                // Column Menu
		                $kendo->setColumnMenu();

		                $kendo->addcolumns(array(
							$PatientTypeIcon,
							$PatientType,
							$DictationDate,
							$DictationHour,
							// $AdmissionDate,
							$RequestNumber,
							$OrderNumber,
							$Procedure,
							$PatientLastName,
							$PatientFirstName,
							$PatientIDField,
							$actions,
		                ));

		                $kendo->generate(true, true, 'row', 550);

						?>

						{!! $kendo->render() !!}
					
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script type="x/kendo-template" id="page-template1">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.orders-to-dictate-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<script type="x/kendo-template" id="page-template2">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.my-orders-to-dictate-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<script type="x/kendo-template" id="page-template3">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.orders-to-approve-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<script type="x/kendo-template" id="page-template4">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.my-orders-to-approve-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<script type="x/kendo-template" id="page-template5">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.addendums-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<script type="x/kendo-template" id="page-template6">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #</div>
				{{ trans('titles.dictated-orders-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<style type="text/css">
		/* Page Template for the exported PDF */
		.page-template {
			font-family: "Open Sans", "Arial", sans-serif;
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
		}
		.page-template .header {
			position: absolute;
			top: 30px;
			left: 30px;
			right: 30px;
			border-bottom: 1px solid #888;
			color: #888;
		}
		.page-template .footer {
			position: absolute;
			bottom: 30px;
			left: 30px;
			right: 30px;
			border-top: 1px solid #888;
			text-align: center;
			color: #888;
		}
		.page-template .watermark {
			font-weight: bold;
			font-size: 400%;
			text-align: center;
			margin-top: 30%;
			color: #aaaaaa;
			opacity: 0.1;
			transform: rotate(-35deg) scale(1.7, 1.5);
		}
	</style>


@endsection
