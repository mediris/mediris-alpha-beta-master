@extends('layouts.app')
@section('title', ucfirst(trans('labels.title-poll-page')))
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12" style="margin-bottom:20px;">
            <div class="col-xs-12">
                @if(Session::has('message'))
                     <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
                <!-- Polls -->
                <div class="panel  elastic sombra" id="dates-notification-panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('labels.title-poll-page') }}</h3>
                    </div>
                    <form method="POST" action="{{ route('polls.send') }}">
                    <div class="panel-body">
                        <label for="date_from">{{ ucfirst(trans('labels.startDatePoll')) }}</label>
                        <input type="date" name="{{ 'date_from' }}" id="{{  'date_from' }}" class="form-control" value="{{ $first_day_date }}"/>
                        <label for="endDate">{{ ucfirst(trans('labels.endDatePoll')) }}</label>
                        <input type="date" name="date_to" id="date_to" class="form-control" value="{{ $today_date }}" >

                        <label for="age_from">{{ ucfirst(trans('labels.age_from')) }}</label>
                        <input type="number" name="age_from" id="age_from" class="form-control" value="0"/>
                        <label for="age_to">{{ ucfirst(trans('labels.age_to')) }}</label>
                        <input type="number" name="age_to" id="age_to" class="form-control" value="100"/>
                        <div>
                            <label class="title-label" for="sex_id">{{ ucfirst(trans('labels.gender')) }}</label>
                        </div>
                        @include('includes.select',[
                            'idname' => 'sex',
                            'data' => $sexes,
                            'keys' => ['id','name'],
                        ])
                         <div>
                            <label class="title-label" for="modalities">{{ ucfirst(trans('labels.modalities')) }}</label>
                        </div>
                       <select name="{{ 'modalities' }}[]" id="{{ 'modalities' }}" class="form-control" multiple>
                            @foreach($modalities as $value)
                                @if($value->description != null)
                                        <option value="{{ $value->id }}">{{ ucfirst(trans('labels.'.$value->description)) }}</option>

                                @elseif($value->name != null)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @elseif($value->first_name != null)
                                    <option value="{{ $value->id }}">{{ $value->first_name . ' ' . $value->last_name }}</option>
                                @endif
                            @endforeach
                        </select>
                      {{--   <div>
                            <label class="title-label" for="procedure">{{ ucfirst(trans('labels.procedure')) }}</label>
                        </div>
                       <div>
                            <select name="{{ 'procedure' }}[]" id="{{ 'procedure' }}" class="" multiple="multiple">
                                @foreach($procedures as $value)
                                    <option value="{{ $value->id }}" selected>{{ ucfirst(trans($value->description)) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                                @include('includes.procedures-select',[
                                    'idname' => 'procedure',
                                    'procedures' => $procedures
                                ])
                            </div>--}}
                    </div>
                    <div class="modal-footer">
                        <input id='btn-send-poll' class="btn btn-form" type='submit' name='button' data-style="expand-left" value='{{ ucfirst(trans('labels.button-poll-page')) }}'>
                    </div>
                </form>
                </div>
            </div>
    </div>
</div>

<script>
    jQuery( document ).ready(function() {
    loadDatePicker();
});
</script>
<style>
   /* button.dropdown-toggle{
        display: none;
    }*/
</style>
@endsection
