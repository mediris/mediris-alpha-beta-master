@extends('layouts.app')

@section('title', ucfirst(trans('titles.search')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.search')),
									'elem_type' => 'button',
									'elem_name' => '',
									'second_elem_name' => ucfirst(trans('labels.search')),
									'second_elem_id' => 'search-order',
									'elem_id' => '',
									'form_id' => '',
									'route' => '',
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => [
										'enable' => true,
										'id-1' => 'clear-search-1',
										'id-2' => 'clear-search-2',
										'class' => ''
									],
									'num' => ['1','2'],
								])
	
	<div class="container-fluid search index">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="panel sombra">
					<div class="panel-heading">
						<h4 class="panel-title">{{ trans('titles.filters') }}</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<label for="birad">{{ trans('labels.category-bi-rad') }}</label>
										</div>
										<select name="birad" id="birad" class="form-control">
											<option value="999" selected>{{ ucfirst(trans('labels.select')) }}</option>
											@if(isset($birads))
												@foreach($birads as $birad)
													<option value="{{ $birad->id }}">{{ $birad->description }}</option>
												@endforeach
											@endif
										</select>
									</div>

									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<label for="biopsy-results">{{ trans('labels.biopsy-results') }}</label>
										</div>
										<select name="biopsy-results" id="biopsy-results" class="form-control">
											<option value="999"
											        selected>{{ ucfirst(trans('labels.select')) }}</option>
											<option value="1">{{ trans('labels.positive') }}</option>
											<option value="2">{{ trans('labels.negative') }}</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div>
											<label for="category-panel"></label>
										</div>
										<div class="panel panel-input-like" id="category-panel">
											<div class="panel-heading" role="tab">
												<a role="button" data-toggle="collapse" href="#search-teaching-file"
												   aria-expanded="false" aria-controls="#search-teaching-file" class="collapsed">
													<h4 class="panel-title">
														{{ ucfirst(trans('labels.teaching-file')) }}
													</h4>
												</a>
											</div>
											<div id="search-teaching-file" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body">
													<div class="row">
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
															<div>
																<label for="category">{{ ucfirst(trans('labels.categories')) }}</label>
															</div>
															<select name="category" id="category" class="form-control">
																<option value="999" selected>{{ ucfirst(trans('labels.select')) }}</option>
																@if(isset($categories))
																	@foreach($categories as $category)
																		<option value="{{ $category->id }}">{{ $category->description }}</option>
																	@endforeach
																@endif
															</select>
														</div>
														<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
															<div>
																<label for="subcategory">{{ ucfirst(trans('labels.subcategories')) }}</label>
															</div>
															<select name="sub-category" id="sub-category" class="form-control">
																<option value="999" selected>{{ ucfirst(trans('labels.select')) }}</option>
																@if(isset($subcategories))
																	@foreach($subcategories as $subcategory)
																		<option value="{{ $subcategory->id }}">{{ $subcategory->description }}</option>
																	@endforeach
																@endif
															</select>
														</div>
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<div>
																<label for="teaching-file-text">{{ ucfirst(trans('labels.teaching-file-text')) }}</label>
															</div>
															<div>
																<input type="text" name="teaching-file-text" id="teaching-file-text" class="form-control" value="" title="{{ trans('labels.text') }}">
															</div>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div>
									<label for="report">{{ ucfirst(trans('labels.text-in-report')) }}</label>
								</div>
								<div id="search-report" >
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div>
												<input type="text" name="reportText" id="reportText" class="form-control" value="" title="{{ trans('labels.text') }}">
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="row" id="pat-info">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="panel-sombra">
					<div class="panel-heading">
						<h4 class="panel-title">{{ ucfirst(trans('titles.column-change')) }}</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<label for="search-filter2">{{ trans('labels.dates') }}</label>
										</div>
										<select name="search-filter2" id="search-filter2" class="form-control changeableFilter">
											<option value="2" selected>{{ trans('labels.admission-date') }}</option>
											<option value="3">{{ trans('labels.study-date') }}</option>
											<option value="4">{{ trans('labels.dictation-date') }}</option>
											<option value="5">{{ trans('labels.transcription-date') }}</option>
											<option value="6">{{ trans('labels.approval-date') }}</option>
											<option value="7">{{ trans('labels.suspension-date') }}</option>
										</select>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" >
										<div>
											<label for="search-filter1">{{ trans('labels.patient-information') }}</label>
										</div>
										<select name="search-filter1" id="search-filter1" class="form-control changeableFilter">
											<option value="11" selected>{{ trans('labels.patient-id') }}</option>
											{{--<option value="">{{ trans('labels.history-id') }}</option>--}}
											<option value="12">{{ trans('labels.service-request-id') }}</option>
											<option value="13">{{ trans('labels.cellphone-number') }}</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1>{{ ucfirst(trans('titles.orders-list')) }}</h1>
						</div>
					</div>
				</div>
				
				<?php

				// Grid
                $options = (object) array(
                    'url' => 'search'
                );
                $kendo    = new \App\CustomKendoGrid($options);

                // Fields
                $patientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
				$patientTypeIconField->type('string');
				$patientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
				$patientTypeField->type('string');
				$orderStatusField = new \Kendo\Data\DataSourceSchemaModelField('orderStatus');
				$orderStatusField->type('string');
				$orderIDField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
				$orderIDField->type('string');
				$orderStatusIDField = new \Kendo\Data\DataSourceSchemaModelField('orderStatusID');
				$orderStatusIDField->type('string');
				$orderIdentificationIDField = new \Kendo\Data\DataSourceSchemaModelField('orderIdentificationID');
				$orderIdentificationIDField->type('string');
				$patientNameField = new \Kendo\Data\DataSourceSchemaModelField('patientName');
				$patientNameField->type('string');
				$patientIdentificationIDField = new \Kendo\Data\DataSourceSchemaModelField('patientIdentificationID');
				$patientIdentificationIDField->type('string');
				$patientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
				$patientIDField->type('string');
				$procedureDescriptionField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
				$procedureDescriptionField->type('string');
				$referringField = new \Kendo\Data\DataSourceSchemaModelField('referring');
				$referringField->type('string');
				$approveUserNameField = new \Kendo\Data\DataSourceSchemaModelField('approveUserName');
				$approveUserNameField->type('string');
				$reportTextField = new \Kendo\Data\DataSourceSchemaModelField('reportText');
				$reportTextField->type('string');
				// $addendumTextField = new \Kendo\Data\DataSourceSchemaModelField('addendumText');
				// $addendumTextField->type('string');
				$radiologistUserNameField = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserName');
				$radiologistUserNameField->type('string');
				$radiologistUserIDField = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserID');
				$radiologistUserIDField->type('string');
				$admissionDateField = new \Kendo\Data\DataSourceSchemaModelField('admissionDate');
				$admissionDateField->type('date');
				$technicianEndDateField = new \Kendo\Data\DataSourceSchemaModelField('technicianEndDate');
				$technicianEndDateField->type('date');
				$dictationDateField = new \Kendo\Data\DataSourceSchemaModelField('dictationDate');
				$dictationDateField->type('date');
				$transcriptionDateField = new \Kendo\Data\DataSourceSchemaModelField('transcriptionDate');
				$transcriptionDateField->type('date');
				$approvalDateField = new \Kendo\Data\DataSourceSchemaModelField('approvalDate');
				$approvalDateField->type('date');
				$suspensionDateField = new \Kendo\Data\DataSourceSchemaModelField('suspensionDate');
				$suspensionDateField->type('date');
				$biradIDField = new \Kendo\Data\DataSourceSchemaModelField('biradID');
				$biradIDField->type('string');
				$serviceRequestIDField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
				$serviceRequestIDField->type('string');
				$patientSexField = new \Kendo\Data\DataSourceSchemaModelField('patientSex');
				$patientSexField->type('string');
				$cellPhoneField = new \Kendo\Data\DataSourceSchemaModelField('cellPhone');
				$cellPhoneField->type('string');
				$modalitiesField = new \Kendo\Data\DataSourceSchemaModelField('modalities');
				$modalitiesField->type('string');
				$teachingFileTextField = new \Kendo\Data\DataSourceSchemaModelField('teachingFileText');
				$teachingFileTextField->type('string');
				$categoryIDField = new \Kendo\Data\DataSourceSchemaModelField('categoryID');
				$categoryIDField->type('string');
				$subCategoryIDField = new \Kendo\Data\DataSourceSchemaModelField('subCategoryID');
				$subCategoryIDField->type('string');
				$biopsyResultField = new \Kendo\Data\DataSourceSchemaModelField('biopsyResult');
				$biopsyResultField->type('number');
				
                // Add Fields
                $kendo->addFields(array(
					$patientTypeIconField,
					$patientTypeField,
					$orderStatusField,
					$orderIDField,
					$orderStatusIDField,
					$orderIdentificationIDField,
					$patientNameField,
					$patientIdentificationIDField,
					$patientIDField,
					$procedureDescriptionField,
					$referringField,
					$approveUserNameField,
					$reportTextField,
					// $addendumTextField,
					$radiologistUserNameField,
					$radiologistUserIDField,
					$admissionDateField,
					$technicianEndDateField,
					$dictationDateField,
					$transcriptionDateField,
					$approvalDateField,
					$suspensionDateField,
					$serviceRequestIDField,
					$patientSexField,
					$cellPhoneField,
					$modalitiesField,
					$biradIDField,
					$teachingFileTextField,
					$categoryIDField,
					$subCategoryIDField,
					$biopsyResultField,
                ));

                // Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid('search');

                // Columns
                $patientTypeIcon = new \Kendo\UI\GridColumn();
				$patientTypeIcon->field('patientTypeIcon')
						->attributes(['class' => 'font-awesome-td'])
						->filterable(false)
						->encoded(false)
						->width(50)
						->title(ucfirst(trans('labels.patient-type-icon')));
				$patientType = new \Kendo\UI\GridColumn();
				$patientType->field('patientType')
						->width(150)
						->title(ucfirst(trans('labels.patient-type')));
				$admissionDate = new \Kendo\UI\GridColumn();
				$admissionDate->field('admissionDate')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->format('{0: dd/MM/yyyy}')
						->width(150)
						->title(ucfirst(trans('labels.admission-date')));
				$technicianEndDate = new \Kendo\UI\GridColumn();
				$technicianEndDate->field('technicianEndDate')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->format('{0: dd/MM/yyyy}')
						->width(150)
						->title(ucfirst(trans('labels.study-date')));
				$dictationDate = new \Kendo\UI\GridColumn();
				$dictationDate->field('dictationDate')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->format('{0: dd/MM/yyyy}')
						->width(150)
						->title(ucfirst(trans('labels.dictation-date')));
				$transcriptionDate = new \Kendo\UI\GridColumn();
				$transcriptionDate->field('transcriptionDate')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->format('{0: dd/MM/yyyy}')
						->width(150)
						->title(ucfirst(trans('labels.transcription-date')));
				$approvalDate = new \Kendo\UI\GridColumn();
				$approvalDate->field('approvalDate')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->format('{0: dd/MM/yyyy}')
						->width(150)
						->title(ucfirst(trans('labels.approval-date')));
				$suspensionDate = new \Kendo\UI\GridColumn();
				$suspensionDate->field('suspensionDate')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->format('{0: dd/MM/yyyy}')
						->width(150)
						->title(ucfirst(trans('labels.suspension-date')));
				$orderStatusFilterable = new \Kendo\UI\GridColumnFilterable();
            	$orderStatusFilterable->ui(new \Kendo\JavaScriptFunction('orderStatusFilter'));
				$orderStatus = new \Kendo\UI\GridColumn();
				$orderStatus->field('orderStatus')
						->filterable($orderStatusFilterable)
						->width(150)
						->title(ucfirst(trans('labels.request-status')));
				$patientName = new \Kendo\UI\GridColumn();
				$patientName->field('patientName')
						->width(175)
						->title(ucfirst(trans('labels.patient.first-name')));
				$patientSexFilterable = new \Kendo\UI\GridColumnFilterable();
            	$patientSexFilterable->ui(new \Kendo\JavaScriptFunction('patientSexFilter'));
				$patientSex = new \Kendo\UI\GridColumn();
				$patientSex->field('patientSex')
						->filterable($patientSexFilterable)
						->width(150)
						->title(ucfirst(trans('labels.sex')));
				$patientIdentificationID = new \Kendo\UI\GridColumn();
				$patientIdentificationID->field('patientIdentificationID')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->width(150)
						->title(ucfirst(trans('labels.patient-id')));
				$cellPhone = new \Kendo\UI\GridColumn();
				$cellPhone->field('cellPhone')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->width(150)
						->title(ucfirst(trans('labels.cellphone-number')));
				$serviceRequestID = new \Kendo\UI\GridColumn();
				$serviceRequestID->field('serviceRequestID')
						->attributes(['class' => 'changeable'])
						->headerAttributes(['class' => 'changeable'])
						->hidden(true)
						->width(150)
						->title(ucfirst(trans('labels.service-request-id')));
				$procedureDescription = new \Kendo\UI\GridColumn();
				$procedureDescription->field('procedureDescription')
						->width(200)
						->title(ucfirst(trans('labels.procedure')));
				$referring = new \Kendo\UI\GridColumn();
				$referring->field('referring')
						->width(150)
						->title(ucfirst(trans('labels.referring')));
				$orderID = new \Kendo\UI\GridColumn();
				$orderID->field('orderID')
						->width(150)
						->title(ucfirst(trans('labels.order')));
				$approveUserName = new \Kendo\UI\GridColumn();
				$approveUserName->field('approveUserName')
						->title(ucfirst(trans('labels.approved')));
				$biradID = new \Kendo\UI\GridColumn();
				$biradID->field('biradID')
						->hidden(true)
						->title(ucfirst(trans('labels.birad')));
				$modalities = new \Kendo\UI\GridColumn();
				$modalities->field('modalities')
						->hidden(false)
						->width(150)
						->title(ucfirst(trans('labels.modalities')));
				$teachingFileText = new \Kendo\UI\GridColumn();
				$teachingFileText->field('teachingFileText')
						->hidden(false);
				$categoryID = new \Kendo\UI\GridColumn();
				$categoryID->field('categoryID')
						->hidden(false);
				$subCategoryID = new \Kendo\UI\GridColumn();
				$subCategoryID->field('subCategoryID')
						->hidden(false);
				$biopsyResult = new \Kendo\UI\GridColumn();
				$biopsyResult->field('biopsyResult')
						->hidden(false);
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->title(trans('labels.acc'))
						->width(75);
				
				$slash = "<span class='slash'>|</span>";
				
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'search', 'edit');
				
				//::::::::::::::::::::: ACTION BUTTONS ::::::::::::::::::::::::://
				
				//				$approveOrder = ($canEdit ? "\"<div class='order-action approve-order'><a href='" . url('preadmission/approve') .
				//						"/\" + row.orderID + \"" .
				//						"' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.approve-order') .
				//						"'><i class='fa fa-check-square-o' aria-hidden='true'></i></a></div>\" + " : '');
				
				$orderDetail = ($canEdit ? "\"<div class='order-action view-order-detail'><a href='" . url('search/edit') .
						"/\" + row.orderID + \"" .
						"' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') .
						"'><i class='fa fa-eye' aria-hidden='true'></i></a></div>\"" : '');
				
				//				$ableToBeBlockedButtons	= "\"<div class='actions'>\" + " . $print . $rewriteOrder . $suspendOrder . $orderDetail . " + \"</div>\"";
				$actions = "\"<div class='actions'>\" + " . $orderDetail . "+ \"</div>\"";
				
				$actionButtons = "\"<div class='action-buttons'>\" + " . $actions . " + \"</div>\"";
				
				$Actions->template(new Kendo\JavaScriptFunction("
						function (row) {
							return  " . $actionButtons . ";
						}"));

				$excel = new \Kendo\UI\GridExcel();
				$excel->fileName(trans('titles.orders-list') . '.xlsx')
						->filterable(true);

                // Excel
                $kendo->setExcel('titles.orders-list', '
                    function(e) {
		                if (!exportFlag) {
				            e.sender.hideColumn(0);
				            e.preventDefault();
				            exportFlag = true;
				            setTimeout(function () {
				              e.sender.saveAsExcel();
				            });
				          } else {
				            e.sender.showColumn(0);
				            exportFlag = false;
				          }
				    }', '.xlsx');

                // PDF
                $kendo->setPDF('titles.orders-list', 'search/1', '
                    function (e) {
						e.sender.hideColumn(0);
	                    e.sender.hideColumn(8);
						e.promise.done(function() {
							e.sender.showColumn(0);
	                        e.sender.showColumn(8);
						});
					}');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$patientTypeIcon,
					$patientType,
					$admissionDate,
					$technicianEndDate,
					$dictationDate,
					$transcriptionDate,
					$approvalDate,
					$suspensionDate,
					$orderStatus,
					$patientName,
					$patientSex,
					$patientIdentificationID,
					$serviceRequestID,
					$cellPhone,
					$procedureDescription,
					$referring,
					$orderID,
					$biradID,
					$modalities,
					$Actions,
                ));

                $kendo->generate(true, true, null, 550);

                //dd((new \Lang), \Lang::get('Por aprobar'));

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>

	<script>
		
		function orderStatusFilter(element) {
			element.kendoComboBox({
                dataSource: {
                    transport: {
                        read: {
                            url: "search",
                            type: "POST"
                        }
                    },
                    schema: {
                        data: "data"
                    }
                },
                dataTextField: "orderStatus",
                dataValueField: "orderStatus",
                optionLabel: "--Selecciona un Valor--"
            });
		}

		function patientSexFilter(element) {
			element.kendoComboBox({
                dataSource: {
                    transport: {
                        read: {
                            url: "search",
                            type: "POST"
                        }
                    },
                    schema: {
                        data: "data"
                    }
                },
                dataTextField: "patientSex",
                dataValueField: "patientSex",
                optionLabel: "--Selecciona un Valor--"
            });
		}

	</script>
	
	<script type="x/kendo-template" id="page-template">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
					#
				</div>
				{{ trans('titles.search-orders-list') }}
			</div>
			<div class="watermark">IDACA</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<style type="text/css">
		/* Page Template for the exported PDF */
		.page-template {
			font-family: "Open Sans", "Arial", sans-serif;
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
		}
		
		.page-template .header {
			position: absolute;
			top: 30px;
			left: 30px;
			right: 30px;
			border-bottom: 1px solid #888;
			color: #888;
		}
		
		.page-template .footer {
			position: absolute;
			bottom: 30px;
			left: 30px;
			right: 30px;
			border-top: 1px solid #888;
			text-align: center;
			color: #888;
		}
		
		.page-template .watermark {
			font-weight: bold;
			font-size: 400%;
			text-align: center;
			margin-top: 30%;
			color: #aaaaaa;
			opacity: 0.1;
			transform: rotate(-35deg) scale(1.7, 1.5);
		}
	</style>

@endsection
