
<div class="container-fluid">
    <form method="post" action="{{ route('steps.edit', [$step]) }}" id="StepEditForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.edit')).' '.trans('titles.step') }}</h4>
            </div>
        </div>

        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('includes.general-checkbox', [
                    'id'        =>'active-chk',
                    'name'      =>'active',
                    'label'     =>'labels.is-active',
                    'condition' => $step->active
                ])
                @if ($errors->has('active'))
                <span class="text-danger">
                    <strong>{{ $errors->first('active') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
                </div>
                <div>
                    <input type="text" name="description" id="description" class="input-field form-control user btn-style" value="{{ $step->description }}"/>
                    @if ($errors->has('description'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }} *</label>
                </div>
                <div>
                    <input type="text" class="form-control" name="administrative_ID" id="administrative_ID" value="{{ $step->administrative_ID }}">
                    @if ($errors->has('administrative_ID'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                <div>
                    <label for='indications'>{{ ucfirst(trans('labels.indications')) }} *</label>
                </div>
                <div>
                    <textarea class="form-control" name="indications" id="indications">{{ $step->indications }}</textarea>
                    @if ($errors->has('indications'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('indications') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12">
                <div>
                    <label class="control-label" for="equipment">{{ ucfirst(trans('labels.equipment')) }}</label>
                </div>
                <div>
                    <select class="form-control" name="equipment[]" id="equipment" multiple="multiple">
                        @foreach($equipment as $equipment)
                        <option value="{{ $equipment->id }}" {{ $step->hasEquipment($equipment->id) ? 'selected' : '' }}>{{ $equipment->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('equipment'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('equipment') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <label class="control-label" for="consumables">{{ ucfirst(trans('messages.consumables')) }}</label>
                </div>
                <div>
                    <select class="form-control" name="consumables[]" id="consumables" multiple="multiple">
                        @foreach($consumables as $consumable)
                        <option value="{{ $consumable->id }}" {{ $step->hasConsumable($consumable->id) ? 'selected' : '' }}>{{ $consumable->description }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('consumables'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('consumables') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>
    </form>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\StepEditRequest', '#StepEditForm'); !!}
</div>
<script>
        //ColorBoxSelectsInit();
        ColorBoxmultiSelectsInit();
    </script>

