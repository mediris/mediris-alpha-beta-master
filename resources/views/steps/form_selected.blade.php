<div class="row" id="{{ $random }}">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 step-container" position="{{ $position + 1 }}">
		<div class="step panel sombra">
			
			<input type="hidden" id="step-id" name="steps[{{ $random }}][id]" value="{{ $step->id }}">
			
			<div class="panel-heading bg-blue" role="tab" id="heading_{{ $random }}">
				<a role="button" data-toggle="collapse" data-parent="#steps" href="#collapse_{{ $random }}"
				   aria-expanded="false" aria-controls="collapse_{{ $random }}" class="collapsed">
					<h4 class="panel-title">
						{{ ucfirst(trans('titles.step')) }} <span class="step-position">{{ $position + 1 }}</span>
					</h4>
				</a>
				
				<a type="button" class="close white requested-procedure-close" id-attr="{{ $random }}">×</a>
			
			</div>
			<div id="collapse_{{ $random }}" class="panel-collapse collapse" role="tabpanel"
			     aria-labelledby="heading_{{ $random }}">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div>
								<label for='steps[{{ $random }}][administrative_ID]'>{{ ucfirst(trans('labels.administrative-id')) }}
									*</label>
							</div>
							<input type="text" class="form-control" name="steps[{{ $random }}][administrative_ID]"
							       id="administrative_ID" value="{{ $step->administrative_ID }}">
						
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div>
								<label for='steps[{{ $random }}][description]'>{{ ucfirst(trans('labels.description')) }}
									*</label>
							</div>
							<input type="text" name="steps[{{ $random }}][description]" id="description"
							       class="input-field form-control user btn-style" value="{{ $step->description }}"/>
						
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div>
								<label for="steps[{{ $random }}][equipment]">{{ ucfirst(trans('labels.equipment')) }}</label>
							</div>
							<select class="form-control" name="steps[{{ $random }}][equipment][]" id="equipment"
							        multiple>
								@foreach($equipment as $equipment)
									<option value="{{ $equipment->id }}" {{ $step->hasEquipment($equipment->id) ? 'selected' : '' }}>{{ $equipment->name }}</option>
								@endforeach
							</select>
						
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div>
								<label class="control-label"
								       for="consumables">{{ ucfirst(trans('messages.consumables')) }}</label>
							</div>
							<select class="form-control" name="steps[{{ $random }}][consumables][]" id="consumables"
							        multiple>
								@foreach($consumables as $consumable)
									<option value="{{ $consumable->id }}" {{ $step->hasConsumable($consumable->id) ? 'selected' : '' }}>{{ $consumable->description }}</option>
								@endforeach
							</select>
						
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div>
								<label for='steps[{{ $random }}][indications]'>{{ ucfirst(trans('labels.indications')) }}
									*</label>
							</div>
							<textarea class="form-control" name="steps[{{ $random }}][indications]"
							          id="indications">{{ $step->indications }}</textarea>
						</div>
					</div>
					<div class="row">
						<div class="hide">
							<input type="number" name="steps[{{ $random }}][order]"
							       class="input-field form-control user btn-style step-order"
							       value="{{ $position + 1 }}" readonly>
						
						</div>
					</div>
				</div>
			</div>
			<script>
				//deleteRequestedProcedure();
				
				//$('#' + "{{ $random }}" + ' select').not('select[multiple]').selectpicker();
				
				
				// $('#' + "{{ $random }}" + ' select[multiple]').kendoMultiSelect({
				// 	autoClose: false,
				// 	noDataTemplate: ''
				// });
			
			</script>
		</div>
	</div>
</div>