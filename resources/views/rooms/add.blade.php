@extends('layouts.app')

@section('title',ucfirst(trans('titles.add')).' '.trans('titles.room'))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.add')).' '.trans('titles.room'),
									'elem_type' => 'button',
									'elem_name' => ucfirst(trans('labels.save')),
									'form_id' => '#RoomAddForm',
									'route' => '',
									'fancybox' => '',
									'routeBack' => route('rooms')
								])
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif

				@if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
			</div>
		</div>
		
		<form action="{{ route('rooms.add') }}" method="post" id="RoomAddForm">
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra x4">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-hospital-o"
							                           aria-hidden="true"></i> {{ ucfirst(trans('titles.room')) }}</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									@include('includes.general-checkbox', [
									    'id'        =>'active-chk',
									    'name'      =>'active',
									    'label'     =>'labels.is-active',
									    'condition' => 0
									])
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="name" id="name"
										       value="{{ old('name') }}">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }}
											*</label>
									</div>
									<div>
										<input type="text" class="form-control" name="administrative_ID"
										       id="administrative_ID" value="{{ old('administrative_ID') }}">
									</div>
								</div>
							
							</div>
							
							<div class="row">
								
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label class="control-label"
										       for="division_id">{{ ucfirst(trans('labels.division')) }} *</label>
									</div>
									<div>
										@include('includes.select', [
                                            'idname' => 'division_id',
                                            'data' 	 => $divisions,
                                            'keys' 	 => ['id', 'name'],
                                            'value'  => old('division_id' )
                                        ])
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
									</div>
									<div>
										<textarea class="form-control" name="description" id="description"
										          rows="5">{{ old('description') }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="panel sombra x4">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-file-text"
							                           aria-hidden="true"></i> {{ ucfirst(trans('titles.procedures')) }}
							</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="modality_id_reception">{{ ucfirst(trans('labels.modality')) }}</label>
									</div>
									<div>
										@include('includes.select', [
                                            'idname' => 'modality_id_reception',
                                            'data' => $modalities,
                                            'keys' => ['id', 'name'] 
                                        ])
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="procedure_id">{{ ucfirst(trans('labels.procedures')) }}</label>
									</div>
									<div>
										@include('includes.procedures-select',[

                                                    'idname' => 'procedure_id',
                                                    'procedures' => $procedures
                                                    
                                                ])
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="default_duration">{{ ucfirst(trans('labels.default-duration')) }}</label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="default_duration"
										       id="default_duration" value="1">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="inline-button">
										<a type="button" href="javascript:;" name="edit-photo" id="addProcedureRoom"
										   class="btn btn-form" title="Edit avatar">
											<i class="fa fa-plus" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.add')) }}
										</a>
										
										<a href="javascript:;" class="btn btn-form" id="addAllProcedureRoom">
											<i class="fa fa-plus" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.select-all')) }}
										</a>

										<a href="javascript:;" class="btn btn-form btn-danger" id="removeProcedureRooms">
											<i class="fa fa-minus" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.remove-all')) }}
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-calendar" aria-hidden="true"></i> Bloques</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='start_hour'>Hora de inicio *</label>
									</div>
									<div>
										<input type="text" class="hour-field" name="start_hour" id="start_hour"
										       value="7:00 AM">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='end_hour'>Hora de fin *</label>
									</div>
									<div>
										<input type="text" class="hour-field" name="end_hour" id="end_hour"
										       value="5:00 PM">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="block_size">Tama&ntilde;o del bloque *</label>
									</div>
									<div>
										@include('includes.select', [
                                            'idname' => 'block_size',
                                            'data' => $blockSizes,
                                            'keys' => [0, 1],
                                        ])
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for='quota'>Cupos por Bloque *</label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="quota" id="quota" value="1">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for='equipment_number'>N&uacute;mero de Equipos *</label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="equipment_number" id="equipment_number" value="1">
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-calendar-times-o" aria-hidden="true"></i> Bloques
								inhabilitados</h3>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div>
									<label for="block_size">Bloques</label>
								</div>
								<div id="lock_blocks">
								
								</div>
							
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row">
						<div id="procedures">
						
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		{!! JsValidator::formRequest('App\Http\Requests\RoomAddRequest', '#RoomAddForm'); !!}
	</div>

@endsection