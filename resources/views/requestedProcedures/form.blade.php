<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 requestedProcedure" id="{{ $position }}" >
    <div class="panel sombra">
        <div class="panel-heading bg-blue">
            <a type="button" id="close-procedure" class="close white requested-procedure-close" id-attr="{{ $position }}">×</a>
            <h3 class="panel-title">{{ $procedure->description }} - {{ $procedure->modality->name }}</h3>
        </div>
        <div class="panel-body">

            @if(isset($requestedProcedure))
                <input type="hidden" name="requestedProcedures[{{ $position }}][id]" value="{{ $requestedProcedure->id }}">
            @endif

            <input type="hidden" name="requestedProcedures[{{ $position }}][procedure_id]" value="{{ $procedure->id }}">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="check-awesome">
                        <div class="ckeckbox-left">    
                            <input type="checkbox" name="requestedProcedures[{{ $position }}][urgent]" id="urgent-{{ $position }}" class="form-control" value="1">  
                            <label for="urgent-{{ $position }}">
                                <div class="check-container">
                                    <span class="check"></span>
                                </div>
                                <span class="box"></span>
                            </label> 
                        </div>
                        <div class="ckeckbox-right">
                            <span class="message-checkbox">
                            {{ ucfirst(trans('labels.urgent')) }}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div>
                        <label for='comments'>{{ ucfirst(trans('labels.comments')) }}</label>
                    </div>
                    <div>
                        <textarea class="form-control" name="requestedProcedures[{{ $position }}][comments]" id="comments">{{ isset($requestedProcedure) ? $requestedProcedure->comments : old('comments') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      deleteRequestedProcedure();    
    </script>
</div>
