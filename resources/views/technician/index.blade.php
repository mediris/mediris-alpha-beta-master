@extends('layouts.app')

@section('title', ucfirst(trans('labels.technician')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('labels.technician')),
									'elem_type' => '',
									'elem_name' => '',
									'form_id' => '',
									'route' => '',
									'fancybox' => '',
									'routeBack' => '',
									'clearFilters' => true
								])

	<!-- Modal -->
	<div class="modal fade" id="suspendModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content" id="cboxContent">
				<div class="modal-header">
					<button type="button" id="cboxClose" data-dismiss="modal"><span>x</span></button>
					<h4 class="modal-title">{{ ucfirst(trans('labels.suspension-reason')) }}</h4>
				</div>

				<form id="suspendForm" method="post" action="{{ route('technician.suspend') }}">
					<div class="modal-body">
						<div>
							@include('includes.select', [
	                                                    'idname' => 'suspensionReason',
	                                                    'data' => $suspensionReasons,
	                                                    'keys' => ['id', 'description'] 
	                                                ])
						</div>
						<input type="hidden" id="orderId" name="orderId" value="-1">
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</div>
					<div class="modal-footer">
						<button type="submit" id="btn-add-edit-save" class="btn btn-form">{{ ucfirst(trans('labels.save')) }}</button>
						@include('includes.select', [
	                                                    'idname'	=> 'user-change-sel',
	                                                    'value' 	=> \Auth::user()->id,
	                                                    'data' 		=> \App\User::all(),
	                                                    'keys' 		=> ['id', 'username'],
	                                                    'class'		=> 'dropup in-footer'
	                                                ])
					</div>
				</form>

			</div>
		</div>
	</div>

	<!-- Modal login -->
	@include('includes.modal-login')
	
	<div class="container-fluid technician index">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.orders')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array(
                    'url' => 'technician'
                );
                $kendo    = new \App\CustomKendoGrid($options);

                // Fields
                $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
				$PatientTypeIconField->type('string');
				$PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
				$PatientTypeField->type('string');
				$AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
				$AdmissionDateField->type('date');
				$AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
				$AdmissionHourField->type('date');
				$RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
				$RequestNumberField->type('string');
				$OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
				$OrderNumberField->type('string');
				$ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
				$ProcedureField->type('string');
				$PatientIDNumber = new \Kendo\Data\DataSourceSchemaModelField('patientIdentificationID');
				$PatientIDNumber->type('number');
				$PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
				$PatientFirstNameField->type('string');
				$PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
				$PatientLastNameField->type('string');
				$PatientIdentificationIDField = new \Kendo\Data\DataSourceSchemaModelField('patientIdentificationID');
				$PatientIdentificationIDField->type('string');
				$PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
				$PatientIDField->type('string');
				$TechnicianUserIDField = new \Kendo\Data\DataSourceSchemaModelField('technicianUserID');
				$TechnicianUserIDField->type('number');
				$BlockingUserNameField = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
				$BlockingUserNameField->type('string');
				$BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
				$BlockedStatusField->type('integer');
				$BlockingUserIDField = new \Kendo\Data\DataSourceSchemaModelField('blockingUserID');
				$BlockingUserIDField->type('number');

                // Add Fields
                $kendo->addFields(array(
					$PatientTypeIconField,
					$PatientTypeField,
					$AdmissionDateField,
					$AdmissionHourField,
					$RequestNumberField,
					$OrderNumberField,
					$ProcedureField,
					$PatientFirstNameField,
					$PatientLastNameField,
					$PatientIDNumber,
					$PatientIdentificationIDField,
					$PatientIDField,
					$TechnicianUserIDField,
					$BlockingUserNameField,
					$BlockedStatusField,
					$BlockingUserIDField,
                ));

                // Create Schema
                $kendo->createSchema(true, true);

                // Create Data Source
                $kendo->createDataSource();
                
                // Create Grid
                $kendo->createGrid('technician');

                // Columns
                $PatientTypeIcon = new \Kendo\UI\GridColumn();
				$PatientTypeIcon->field('patientTypeIcon')
						->attributes(['class' => 'font-awesome-td'])
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.patient-type-icon')));
				$PatientType = new \Kendo\UI\GridColumn();
				$PatientType->field('patientType')
						->title(ucfirst(trans('labels.technician-patient-type')));
				$Modality = new \Kendo\UI\GridColumn();
				$Modality->field('modality')
						->title(ucfirst(trans('labels.modality')));
				$AdmissionDate = new \Kendo\UI\GridColumn();
				$AdmissionDate->field('serviceIssueDate')
						->format('{0: dd/MM/yyyy}')
						->title(ucfirst(trans('labels.admission-date')));
				$AdmissionHour = new \Kendo\UI\GridColumn();
				$AdmissionHour->field('serviceIssueDate')
						->filterable(false)
						->format('{0: h:mm tt}')
						->title(ucfirst(trans('labels.admission-hour')));
				$RequestNumberFilterable = new \Kendo\UI\GridColumnFilterable();
                $RequestNumberFilterable->ui(new \Kendo\JavaScriptFunction('requestNumberFilter'));
				$RequestNumber = new \Kendo\UI\GridColumn();
				$RequestNumber->field('serviceRequestID')
						->title(ucfirst(trans('labels.service-request-id')))
						->format('{0:n0}');
				$OrderNumber = new \Kendo\UI\GridColumn();
				$OrderNumber->field('orderID')
						->title(ucfirst(trans('labels.order-id')));
				$Procedure = new \Kendo\UI\GridColumn();
				$Procedure->field('procedureDescription')
						->title(ucfirst(trans('labels.procedure')));
				$PatientIDNumber = new \Kendo\UI\GridColumn();
				$PatientIDNumber->field('patientIdentificationID')
						->title(ucfirst( trans('labels.id-number')));
				$PatientFirstName = new \Kendo\UI\GridColumn();
				$PatientFirstName->field('patientFirstName')
						->title(ucfirst(trans('labels.patient.first-name')));
				$PatientLastName = new \Kendo\UI\GridColumn();
				$PatientLastName->field('patientLastName')
						->title(ucfirst(trans('labels.patient.last-name')));
				
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->title(trans('labels.actions'))
				->width(220);
					
				$slash = "<span class='slash'>|</span>";
					
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'technician', 'edit');
				$canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'technician', 'lock');
				$canPrint = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'technician', 'lock');
				$canRewrite = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'technician', 'rewrite');
				$canSuspend = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'technician', 'lock');
				
				//::::::::::::::::::::: ACTION BUTTONS ::::::::::::::::::::::::://
				$lockedOrNot = "<i class='fa \" + (row.blockedStatus == 0 ? \"fa-unlock\" : \"fa-lock blue\") + \"' aria-hidden='true'></i>";
				$lockedToolTip = "\" + (row.blockedStatus == 0 ? \"" . trans('labels.block-order') . " \" : \"" .
								trans('labels.order-blocked-by') . " \" + row.blockingUserName) + \"";
					
				$lock = ($canLock ? "<div class='order-action block-order'><a class='change-order-status' href='" . url('technician/lock/') .
						"/\" + row.patientID + \"/\" + row.serviceRequestID + \"" .
						"' data-toggle='tooltip' data-placement='bottom' title='" . $lockedToolTip .
						"' status-attr='lock'>" . $lockedOrNot .
						"</a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
					
				$print = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action print-order-label'><a href='" . url('technician/printer') .
						"/\" + row.orderID + \"/\" + row.patientID + \"" .
						"' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.print-label') .
						"'><i class='fa fa-print' aria-hidden='true'></i></a></div>\" + " : '');
					
				$rewriteOrder = ($canRewrite ? "\"" . $slash . "\" + \"<div class='order-action'><a class='rewrite-order' attr-id='\" + row.orderID + \"' href='javascript:;' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.rewrite-order') .
						"'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
				
				$suspendedOrNot = "<i class='fa \" + (row.orderStatusID == 7 ? \"fa-ban\" : \"fa-hand-paper-o\") + \"' aria-hidden='true'></i>";

				$suspendOrder = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action suspend-order'><a  id='suspend-btn-\" + row.orderID + \"' onclick='modalSuspendOrderReason(\" + row.orderID + \");'  data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.suspend-order') .
						"'><i class='fa fa-hand-paper-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');
					
				$orderDetail = ($canEdit ? "\"" . $slash . "\" + \"<div class='order-action view-order-detail'><a href='" . url('technician/edit') .
						"/\" + row.orderID + \"" .
						"' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') .
						"'><i class='fa fa-eye' aria-hidden='true'></i></a></div>\"" : '');
				

				$ableToBeBlockedButtons	= "\"<div class='actions'>\" + " . $print . $rewriteOrder . $suspendOrder . $orderDetail . "+ \"</div>\"";
					
				$actionButtons = "\"<div class='action-buttons'>" . $lock . $ableToBeBlockedButtons . " + \"</div>\"";
					
				$Actions->template(new Kendo\JavaScriptFunction("
						function (row) {
							//console.log(row);
							return  " . $actionButtons . ";
						}"));

                // Excel
                $kendo->setExcel('titles.technician-orders-list', '
                    function(e) {
		                if (!exportFlag) {
				            e.sender.hideColumn(0);
				            e.preventDefault();
				            exportFlag = true;
				            setTimeout(function () {
				              e.sender.saveAsExcel();
				            });
				          } else {
				            e.sender.showColumn(0);
				            exportFlag = false;
				          }
				    }');

                // PDF
                $kendo->setPDF('titles.technician-orders-list', 'technician/1', '
                    function (e) {
						e.sender.hideColumn(0);
						e.sender.hideColumn(8);
						e.promise.done(function() {
							e.sender.showColumn(0);
							e.sender.showColumn(8);
						});
					}');

                // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
                    $PatientTypeIcon, 
					$PatientType,
					$Modality,
					$AdmissionDate,
					$AdmissionHour,
					$RequestNumber,
					$OrderNumber,
					$Procedure,
					$PatientLastName,
					$PatientFirstName,
					$PatientIDNumber,
					$Actions,
                ));

                $kendo->generate(true, true);
					
				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>

	<script>

        function requestNumberFilter(element) {
            element.kendoComboBox({
                dataSource: {
                    transport: {
                        read: {
                            url: "technician",
                            type: "POST"
                        }
                    },
                    schema: {
                        data: "data"
                    }
                },
                dataTextField: "serviceRequestID",
                dataValueField: "serviceRequestID",
                optionLabel: "--Selecciona un Valor--"
            });
        }

    </script>

    <script>
		$(document).ready(function(){
			// Refresh Kendo Grid each 30s
			// Custom Way kendoGridAutoRefresh('#grid', 30);
			kendoGridAutoRefresh();
		});
	</script>

	<script type="x/kendo-template" id="page-template">
		<div class="page-template">
			<div class="header">
				<div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
					#
				</div>
				{{ trans('titles.technician-orders-list') }}
			</div>
			<div class="watermark">{{ Session::get('institution')->name }}</div>
			<div class="footer">
				{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
			</div>
		</div>
	</script>
	
	<style type="text/css">
		/* Page Template for the exported PDF */
		.page-template {
			font-family: "Open Sans", "Arial", sans-serif;
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
		}
		
		.page-template .header {
			position: absolute;
			top: 30px;
			left: 30px;
			right: 30px;
			border-bottom: 1px solid #888;
			color: #888;
		}
		
		.page-template .footer {
			position: absolute;
			bottom: 30px;
			left: 30px;
			right: 30px;
			border-top: 1px solid #888;
			text-align: center;
			color: #888;
		}
		
		.page-template .watermark {
			font-weight: bold;
			font-size: 400%;
			text-align: center;
			margin-top: 30%;
			color: #aaaaaa;
			opacity: 0.1;
			transform: rotate(-35deg) scale(1.7, 1.5);
		}
	</style>

	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\OrderSuspendRequest', '#suspendForm'); !!}

	<script>
		$(document).ready(function () {
			//SUSPENDER ORDEN
    		callSuspendOrder();
		});
	</script>
@endsection