@extends('layouts.app')

@section('title', ucfirst(trans('titles.alertmessages')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.alertmessages')),
        'elem_type' => 'a',
        'elem_name' => ucfirst(trans('labels.add')),
        'form_id' => '',
        'route' => route('alertMessages.add'),
        'fancybox' => 'add-row cboxElement',
        'routeBack' => '',
        'clearFilters' => true
    ])

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="title">
					<h1>{{ ucfirst(trans('titles.alertmessages')) }}</h1>
				</div>

				<?php

					// Grid
	                $options = (object) array();
	                $kendo    = new \App\CustomKendoGrid($options, false, false);

	                // Fields
	                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
					$IdField->type('string');
					$NameField = new \Kendo\Data\DataSourceSchemaModelField('message');
					$NameField->type('string');
					$TypeField = new \Kendo\Data\DataSourceSchemaModelField('alert_message_type.name');
					$TypeField->type('string');
					$StartDateField = new \Kendo\Data\DataSourceSchemaModelField('start_date');
					$StartDateField->type('date');
					$EndDateField = new \Kendo\Data\DataSourceSchemaModelField('end_date');
					$EndDateField->type('date');

					$kendo->addFields(array(
						$IdField,
						$NameField,
						$StartDateField,
						$EndDateField,
					));

	                // Create Schema
	                $kendo->createSchema(false, false);

	                // Create Data Source
	                // $kendo->createDataSource();

	                $alertMessages = $alertmessages->toArray();

					foreach ($alertMessages as $key => $value) {
						$alertMessages[$key]['actions'] = '<a href=' . route('alertMessages.edit', [$alertMessages[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement" ><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>';
					}

					// Set  Alert Messages
					$kendo->setDataSource($alertMessages);

					// Create Grid
	                $kendo->createGrid('alertmessages');

	                // Columns
	                $Id = new \Kendo\UI\GridColumn();
					$Id->field('id')
							->filterable(false)
							->title('Id');
					$Name = new \Kendo\UI\GridColumn();
					$Name->field('message')
							->title(ucfirst(trans('labels.message')));
					$Type = new \Kendo\UI\GridColumn();
					$Type->field('alert_message_type.name')
							->template("#= (data.alert_message_type) ? data.alert_message_type.name : '' #")
							->title(ucfirst(trans('labels.type')));
					$StartDateFilterable = new \Kendo\UI\GridColumnFilterable();
                	$StartDateFilterable->ui(new \Kendo\JavaScriptFunction('StartDateFilter'));
					$StartDate = new \Kendo\UI\GridColumn();
					$StartDate->field('start_date')
							->filterable($StartDateFilterable)
							->template("#= (data.start_date) ? kendo.toString(kendo.parseDate(start_date, 'yyyy-MM-dd'), 'dd-MM-yyyy') : '' #")
							->title(ucfirst(trans('labels.start')));
					$EndDateFilterable = new \Kendo\UI\GridColumnFilterable();
                	$EndDateFilterable->ui(new \Kendo\JavaScriptFunction('EndDateFilter'));
					$EndDate = new \Kendo\UI\GridColumn();
					$EndDate->field('end_date')
							->filterable($EndDateFilterable)
							->template("#= (data.end_date) ? kendo.toString(kendo.parseDate(end_date, 'yyyy-MM-dd'), 'dd-MM-yyyy') : '' #")
							->title(ucfirst(trans('labels.end')));
					$Actions = new \Kendo\UI\GridColumn();
					$Actions->field('actions')
							->sortable(false)
							->filterable(false)
							->encoded(false)
							->title(ucfirst(trans('labels.actions')));

					 // Filter
	                $kendo->setFilter();

	                // Pager
	                $kendo->setPager();

	                // Column Menu
	                $kendo->setColumnMenu();

	                $kendo->addcolumns(array(
						$Name,
						$Type,
						$StartDate,
						$EndDate,
						$Actions,
	                ));
					
	                $kendo->generate();

					?>
					
					{!! $kendo->render() !!}

			</div>
		</div>
	</div>

	  <script>

        function StartDateFilter(element) {
            element.kendoDatePicker({
                culture: 'es-VE',
                dataInput: true,
                field: "start_date",
                valueField: "start_date",
                optionLabel: "--Selecciona un Valor--",
                format: "{0:dd-MM-yyyy}",
                parseFormats: ["{0:dd-MM-yyyy}"] 
            });
        }

        function EndDateFilter(element) {
            element.kendoDatePicker({
                culture: 'es-VE',
                dataInput: true,
                field: "end_date",
                valueField: "end_date",
                optionLabel: "--Selecciona un Valor--",
                format: "{0:dd-MM-yyyy}",
                parseFormats: ["{0:dd-MM-yyyy}"] 
            });
        }

    </script>

@endsection