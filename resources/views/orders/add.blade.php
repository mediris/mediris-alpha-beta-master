

<div class="container-fluid">

    <form method="post" action="{{ route('orders.add') }}" id="OrderAddForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.add')).' '.trans('titles.order') }}</h4>
            </div>
        </div>

        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('includes.general-checkbox', [
                    'id'        =>'active-chk',
                    'name'      =>'active',
                    'label'     =>'labels.is-active',
                    'condition' => 0
                ])
                
                @if ($errors->has('active'))
                <span class="text-danger">
                    <strong>{{ $errors->first('active') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div>
                    <label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
                </div>

                <div>
                    <input type="text" name="name" id="name" class="input-field form-control user btn-style" value="{{ old('name') }}"/>
                    @if ($errors->has('name'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div>
                    <label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }} *</label>
                </div>

                <div>
                    <input type="text" name="administrative_ID" id="units" class="input-field form-control user btn-style" value="{{ old('administrative_ID') }}"/>
                    @if ($errors->has('administrative_ID'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                    </span>
                    @endif
                </div>

            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                <div>
                    <label for='procedures'>{{ ucfirst(trans('labels.procedures')) }}</label>
                </div>
                <div>
                    <select class="form-control" name="procedures[]" id="procedures" multiple="multiple">
                        @foreach($procedures as $procedure)
                        <option value="{{ $procedure->id }}">{{ $procedure->description }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('procedures'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('procedures') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>

    </form>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\OrderAddRequest', '#OrderAddForm'); !!}

</div>
<script>
        //ColorBoxSelectsInit();
        ColorBoxmultiSelectsInit();
    </script>