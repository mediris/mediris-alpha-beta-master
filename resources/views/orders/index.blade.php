@extends('layouts.app')

@section('title', ucfirst(trans('titles.orders')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.orders')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('orders.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.orders')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('string');
				$NameField = new \Kendo\Data\DataSourceSchemaModelField('name');
				$NameField->type('string');
				$AdministrativeIdField = new \Kendo\Data\DataSourceSchemaModelField('administrative_ID');
				$AdministrativeIdField->type('string');

				$kendo->addFields(array(
					$IdField,
					$NameField,
					$AdministrativeIdField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $orders = $orders->toArray();
				
				$canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'orders', 'show');
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'orders', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'orders', 'active');
				
				foreach ($orders as $key => $value) {
					$title = $orders[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $orders[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$showAction = $canShow ? '<a href=' . route('orders.show', [$orders[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.show') . '><i class="fa fa-eye" aria-hidden="true"></i></a>' : '';
					$editAction = $canEdit ? '<a href=' . route('orders.edit', [$orders[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement" ><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('orders.active', [$orders[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					
					$orders[$key]['actions'] = $editAction . $activeAction;
				}

				// Set  Orders
				$kendo->setDataSource($orders);

				// Create Grid
                $kendo->createGrid('orders');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Name = new \Kendo\UI\GridColumn();
				$Name->field('name')
						->title(ucfirst(trans('labels.name')));
				$AdministrativeId = new \Kendo\UI\GridColumn();
				$AdministrativeId->field('administrative_ID')
						->title(ucfirst(trans('labels.administrative-id')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$Name,
					$AdministrativeId,
					$Actions,
                ));
				
                $kendo->generate();
				
				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>

@endsection