<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Tracking Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to set the titles of the views.
    |
    */

    'home' => 'home',
    'create' => 'create',
    'new' => 'new',
    'delete' => 'delete',
    'last' => 'last',
    'last-sm' => 'last',

    //SINGULAR
    'institution' => 'institution',
    'patient' => 'patient',
    'section' => 'section',
    'action' => 'action',
    'role' => 'role',
    'user' => 'user',
    'configuration' => 'configuration',
    'division' => 'division',
    'room' => 'room',
    'modality' => 'modality',
    'equipment' => 'equipment',
    'patient-type' => 'patient type',
    'printer' => 'printer',
    'source' => 'source',
    'template' => 'template',
    'notificationtemplate' => 'notification template',
    'consumable' => 'consumable',
    'referring' => 'Referring',
    'suspendreasons' => 'Reasons for suspension',
    'order' => 'order',
    'procedure' => 'Procedure',
    'step' => 'step',

    'select' => 'select',
    'add' => 'add',
    'edit' => 'edit',
    'login' => 'login',
    
    'change' => 'change',
    'password' => 'password',
    'availability' => 'Availability',

    //PLURAL
    'institutions' => 'institutions',
    'patients' => 'patients',
    'patients_migration' => 'migration patients',
    'sections' => 'sections',
    'actions' => 'actions',
    'roles' => 'roles',
    'users' => 'users',
    'configurations' => 'configurations',
    'divisions' => 'divisions',
    'rooms' => 'rooms',
    'modalities' => 'modalities',
    'alertmessages' => 'alert messages',
    'patient-types' => 'patient types',
    'printers' => 'printers',
    'sources' => 'sources',
    'templates' => 'templates',
    'notificationtemplates' => 'notification template',
    'consumables' => 'consumables',
    'referrings' => 'referrings',
    'orders' => 'orders',
    'procedures' => 'procedures',
    'steps' => 'steps',
    'reset' => 'reset',
    'forbidden' => 'forbidden',
    '401' => 'unauthorized',
    '402' => 'payment required',
    '403' => 'forbidden',
    '404' => 'not found',
    '405' => 'method not allowed',
    '500' => 'internal server error',
    '502' => 'bad gateway',
    '503' => 'service unavailable',
    'units' => 'units',
    'unit' => 'unit',
    'appointments' => 'appointments',
    'appointment' => 'appointment',
    'new-appointment' => 'New Appointment',
    'edit-appointment' => 'Edit Appointment',
    'docent-file' => 'Docen File',
    'categories' => 'categories',
    'category' => 'category',
    'subcategories' => 'subcategories',
    'subcategory' => 'subcategory',
    'teaching-file' => 'teaching file',
    
    //Reception Section
    'reception' => 'reception',
    'new-request' => 'New Request',
    'edit-request' => 'Edit Request',
    'patient-search' => 'Patient Search',
    'patient-info' => "Patient's Info",
    'request-info' => "Request's Info",
    'related-documents' => 'Request Related Documents',
    'number-request' => 'request number',
    'appointments-list' => "Appointments List",
    "patient-states" => "patient states",
    "patient-state" => "Patient State",

    //Technician Section
    'orders-list' => 'Orders List',
    'order-details' => 'Order Details',
    'detail' => 'Detail',
    'patient-record' => "Patient's Record",
    'appointment-record' => "Appointment's Record",
    'search' => 'search',
    'search-of-appointments' => 'Search',
    'show' => 'show',
    'radiologist' => 'radiologist',
    'technician-orders-list' => 'Orders List (Technician)',
    'print-label' => 'print-label',
    
    //Radiologist Section
    'transcription' => 'Transcription',
    'addendum' => 'Addendum',
    'approve' => 'approve',
    'transcriber' => 'transcriber',
    'orders-to-approve' => 'Orders to approve',
    'orders-to-dictate' => 'Orders to dictate',
    'orders-to-dictate-list' => 'Orders to dictate list',
    'my-orders-to-dictate-list' => 'My orders to dictate list',
    'orders-to-approve-list' => 'Orders to approve list',
    'my-orders-to-approve-list' => 'My orders to approve list',
    'addendums-list' => 'Addendums list',
    'dictated-orders-list' => 'Dictated orders list',

    //Pre-admission Section
    'preadmission-orders-list' => 'Orders List',
    'pre-admission' => 'Pre-Admission',
    'orders-to-transcribe' => 'Orders to transcribe',
    
    'results' => 'results',
    'deliver' => 'deliver',
    'global-statistics' => 'Global Statistics',
    'delivery-information' => 'Delivery Information',
    
    //Search Section
    'filters' => 'Filters',
    'column-change' => 'Column Change',
    'search-orders-list'=>'Orders List',

    // Home Section
    'month-statistics' => 'Monthly statistics',

    //Reports Section
    'reports' => 'Reports',
    'generate' => 'Generate',

    // Results Section
    'finished-orders-list' => 'Finished orders list',

    'statistics' => 'Statistics',
    'personal-statistics' => 'Personal statistics',

    //Migrations Section
    'migrations-history'=>'migration of history',
    'migrations'=>'Migration',
    'requests'=>'Solicitud',
    'scan-files'=>'Scanned document',
    'data-conn'=>'connection data',
    'title-confirm'=>'confirmation'
];