<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    '1' => 'Enero',
    '2' => 'Febrero',
    '3' => 'Marzo',
    '4' => 'Abril',
    '5' => 'Mayo',
    '6' => 'Junio',
    '7' => 'Julio',
    '8' => 'Agosto',
    '9' => 'Septiempre',
    '10' => 'Octubre',
    '11' => 'Noviembre',
    '12' => 'Diciembre',

    // Reports
    'January' => 'Enero',
    'February' => 'Febrero',
    'March' => 'Marzo',
    'April' => 'Abril',
    'May' => 'Mayo',
    'June' => 'Junio',
    'July' => 'Julio',
    'August' => 'Agosto',
    'September' => 'Septiembre',
    'October' => 'Octubre',
    'November' => 'Noviembre',
    'December' => 'Diciembre',
];