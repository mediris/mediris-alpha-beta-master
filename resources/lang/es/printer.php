<?php

return [

    //::::::::::::::: ADDENDUMS PDF TRANSLATIONS :::::::::::::::
    'addendum-order' => 'addendum_orden_',
    'final-order' => 'orden_',
    'report' => 'Informe',
    'order' => 'Orden',
    'order-no' => 'No. Orden'

];