<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    'success-add' => 'Datos agregados correctamente',
    'success-edit' => 'Datos editados correctamente',
    'success-delete' => 'Datos eliminados correctamente',
    'error-add' => 'Datos no agregados',
    'error-edit' => 'Datos no editados',
    'confirm-delete-row' => '¿Está seguro de borrar esta fila y todos los datos relacionados a la misma?',
    '23000' => 'No es posible eliminar o actualizar una fila padre',
    'success-edit-password' => 'La contraseña ha sido cambiada con éxito',
    'error-edit-password' => 'La contraseña actual dada no coincide con los registros',
    'select-valid-step' => 'Por favor seleccione un paso válido',
    'select-valid-procedure' => 'Por favor seleccione un procedimiento válido',
    'forbidden' => 'La operación solicitada está prohibida y no puede ser completada.',
    'block-unavailable' => 'Los bloque solicitados están indisponibles. Por favor intente escoger otros',
    'add-block' => 'No se pueden agregar citas en esta vista.',
    'delete-block' => 'Este bloque no puede ser removido',
    'move-block' => 'Este bloque no puede ser movido',
    'edit-block' => 'Este bloque no puede ser editado',
    'select-procedure' => 'Por favor seleccione un procedimiento válido',
    'insufficient-block' => 'No hay bloques suficientes para el horario seleccionado. Por favor escoja otro.',
    'busy-block' => 'El bloque seleccionado está ocupado. Por favor escoja otro.',
    'appointment-created' => 'La cita ha sido agendada con éxito',
    'appointment-edited' => 'La cita ha sido editada con éxito',
    'all-required' => 'Por favor llene todos los campos requeridos.',
    'wrong-date' => 'No se puede agregar la cita en el bloque seleccionado. Por favor seleccione otro donde la fecha sea mayor que la fecha actual.',
    'appointment-discharged' => 'La cita ha sido cancelada con éxito',
    'reason-to-delete' => 'Por favor introduzca la razón por la cual desea eliminar esta cita.',
    'wrong-reason' => 'Por favor introduzca una razón válida',
    'see-record' => 'Ver su ficha',
    'provide-criteria' => 'Por favor introduzca un criterio de búsqueda válido.',
    'select-valid-template' => 'Por favor seleccione una plantilla válida',
    'confirm-template' => '¿Está seguro de inserter la plantilla seleccionada? Los cambios hechos serán reescritos.',
    'success-transcribe-order' => 'Orden transcrita correctamente',
    'no-data' => 'El reporte esta vacio',
    'success-edit-draft' => 'Borrador guardado correctamente',
    'no-reverse' => 'Este cambio no podrá ser reversado, esta seguro de continuar?',
    'wrong-room-hour' => 'La hora de fin no puede ser antes que la de Inicio',
    
    /*Al que pase por aquí,
    agregue su mensaje en orden alfabético después de este mensaje y tome uno de los mensajes anteriores a esta línea y agréguelo a la lista ordenada.
    Entre todos podemos hacer un mundo mejor.
    */

    'confirm-action'=>'¿Está seguro que desea continuar?',
    'email-sent' => 'El correo electrónico fue enviado',
    'error-test-conn'=>'Prueba de conexión fallida',
    'field' => 'El campo',
    'field-required' => 'es requerido',
    'invalid-test-conn'=>'Datos de conexión inválido',
    'no-email'=>'El paciente no posee email asignado',
    'no-logo-email'=>'La Institución no posee logo de email asignado',
    'no-result-email' => 'La configuración "Envío de reporte" no está activada',
    'room-not-ready' => 'Falta configuración de datos en la sala, comuníquese con el Administrador del sistema',
    'same-procedure' => 'no puede seleccionar el mismo procedimiento mas de una vez',
    'success-test-conn'=>'Prueba de conexión exitosa',
    'user-cancel-action'=>'Acción cancelada por el usuario',


    // kill session - JCH
    'confirm'=>'Confirmación',
    'kill-session'=>'Estos cambios forzarán la desconexión de todos los usuarios'

];